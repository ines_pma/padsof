package graphical.authorPage;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import app.App;
import app.users.RegUser;
import app.users.User;
import graphical.Colors;
import graphical.general.LoggedPanel;
import graphical.rowsDisplays.MultipleAlbumDisplay;
import graphical.rowsDisplays.MultipleSongDisplay;

/**
 * Shows the author's name, whether you can follow them or not and their media.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class AuthorPanel extends JPanel {
	
	public AuthorPanel(RegUser u, LoggedPanel parent) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(100,100,100, 100));
		setBackground(Colors.getColor(4));
		
		JPanel up = new JPanel();
		up.setLayout(new FlowLayout());
		JLabel username = new JLabel(u.getAuthorName()); 
		username.setFont(new Font(username.getFont().getName(), Font.BOLD , 30));
		up.setBackground(Colors.getColor(4));
		up.add(username);
		User actUser = App.getAppInstance().getActualUser();
		
		if (actUser instanceof RegUser && actUser != u) 
			up.add(new FollowUnfollowPanel(u,(RegUser) actUser));
		
		up.setMaximumSize(up.getMinimumSize());
		this.add(up);
		up.setAlignmentX(Component.LEFT_ALIGNMENT);

		MultipleSongDisplay songs = new MultipleSongDisplay(u.getSongs(),parent);
		this.add(songs);
		songs.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		MultipleAlbumDisplay albums = new MultipleAlbumDisplay(u.getAlbums(),parent);
		this.add(albums);
		albums.setAlignmentX(Component.LEFT_ALIGNMENT);
	}
	}
