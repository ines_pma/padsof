package graphical.authorPage;

import javax.swing.JButton;
import javax.swing.border.MatteBorder;

import app.users.RegUser;
import graphical.Colors;
/**
 * Unfollow button
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class UnfollowButton extends JButton{
	/**
	 * @param u user to unfollow
	 * @param actualUser actual user
	 * @param panel panel where the button will appear
	 * 
	 * This button will only appear if the actual user is already following u. When clicked
	 * it will change to follow as it unfollows the user
	 */
	public UnfollowButton(RegUser u, RegUser actualUser, FollowUnfollowPanel panel) {
		this.setText("Unfollow");
		setMaximumSize(getMinimumSize());
		setBackground(Colors.getColor(3));
		setForeground(Colors.getColor(4));
		setBorder(new MatteBorder(5,3,5,3,Colors.getColor(3)));
		
		this.addActionListener(p ->{ actualUser.unfollow(u);
									 panel.changeButton(new FollowButton(u,actualUser,panel));});
	}

}
