package graphical.authorPage;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.border.MatteBorder;

import app.users.RegUser;
import graphical.Colors;

/**
 * Button to follow a user
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class FollowButton extends JButton{
	/**
	 * @param u user you want to follow
	 * @param actualUser user who wants to follow
	 * @param panel panel where this button will apear
	 * When clicked it will change the button to unfollow
	 */
	public FollowButton(RegUser u, RegUser actualUser, FollowUnfollowPanel panel) {
		this.setText("  Follow ");
		int height = (int) this.getMinimumSize().getHeight();
		int width = (int) this.getMinimumSize().getWidth();
		setMinimumSize(new Dimension(width + 200,height));
		setBackground(Colors.getColor(3));
		setForeground(Colors.getColor(4));
		setBorder(new MatteBorder(5,5,5,5,Colors.getColor(3)));
		
		addActionListener(p ->{ actualUser.follow(u);
									 panel.changeButton(new UnfollowButton(u,actualUser,panel));});
	}

}
