package graphical.authorPage;

import javax.swing.BorderFactory;
import javax.swing.JButton;

import app.users.RegUser;
import graphical.general.LoggedPanel;

/**
 * Button which takes you to the author's page
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class AuthorButton extends JButton {
	
	/**
	 * @param user user's page it will take you to
	 * @param parent panel where theuser's page will be placed
	 */
	public AuthorButton(RegUser user, LoggedPanel parent) {
		super(user.getAuthorName());
		this.setBorder(BorderFactory.createEmptyBorder());
		this.setContentAreaFilled(false);
		this.setMaximumSize(this.getMinimumSize());
		this.addActionListener(e -> parent.changeCenter(new AuthorPanel(user,parent)));
	}
}
