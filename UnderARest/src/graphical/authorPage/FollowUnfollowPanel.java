package graphical.authorPage;

import javax.swing.JButton;
import javax.swing.JPanel;

import app.users.RegUser;
import graphical.Colors;
/**
 * panel with follow/unfollow button
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

@SuppressWarnings("serial")

public class FollowUnfollowPanel extends JPanel {
	JButton button;

	/**
	 * @param u user you will follow/unfollow
	 * @param actualUser actual user
	 */
	public FollowUnfollowPanel(RegUser u, RegUser actualUser) {
		setBackground(Colors.getColor(4));
		if(actualUser.getFollowing().contains(u)) 
			button = new UnfollowButton(u,actualUser,this);
		else 
			button = new FollowButton(u,actualUser,this);
		this.add(button);
	}

	/**
	 * @param b button the current button will be replaced with
	 */
	public void changeButton(JButton b) {
		this.removeAll();
		this.add(b);
		button = b;
		repaint();
		revalidate();
	}

}
