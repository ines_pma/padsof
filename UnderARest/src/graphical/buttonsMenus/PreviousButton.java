package graphical.buttonsMenus;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import graphical.general.LoggedPanel;

/**
 * Takes you to the previous panel
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class PreviousButton extends JButton{
	public PreviousButton(LoggedPanel parent){
		try {
			setIcon(new ImageIcon(ImageIO.read(new File("img/previous.png"))));
		} catch (IOException e1) {
			setText("Back");
		}
		setBorder(BorderFactory.createEmptyBorder());
		setContentAreaFilled(false);
		addActionListener(e->parent.previous());
				
	}

}
