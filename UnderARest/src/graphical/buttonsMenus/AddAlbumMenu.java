package graphical.buttonsMenus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;

import app.App;
import app.media.Album;
import app.media.Song;
import app.media.exceptions.DuplicatedSongException;
import app.users.RegUser;
import app.users.exceptions.InvalidTitleException;
import app.users.exceptions.NotYourMusicException;
import graphical.Colors;
import graphical.general.MyPanel;

/**
 * Menu to add a song to an album
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class AddAlbumMenu extends JMenu {
	private ImageIcon img = null;
	/**
	 * It has an item to create a new album and one for each one the current user already has
	 * @param s song that would be added to the album
	 * @param panel panel where the song is printed, it will be refreshed
	 *  if a new album is created.
	 * 
	 */
	public AddAlbumMenu(Song s, MyPanel panel) {
		super("Add to album");
		
		try {
			img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
		} catch (IOException e1) {}
		
		Set<Album> albums = s.getAuthor().getAlbums();
		if(albums != null)
			for(Album a: albums) {
				JMenuItem i = new JMenuItem(a.getName());
				i.setBackground(Colors.getColor(4));
				i.setBorder(new EmptyBorder(0,0,0,0));
				
				i.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						try {
							a.addSong(s);
							panel.refresh();
						} catch (DuplicatedSongException e) {
							JOptionPane.showMessageDialog(null,"This song can't be added to this album.","Error",JOptionPane.PLAIN_MESSAGE,img);
						}
					}
				});	
				this.add(i);
			}
		
		JMenuItem i = new JMenuItem("New album");
		i.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				NewAlbumPanel p = new NewAlbumPanel();
				int result = JOptionPane.showConfirmDialog(null,p,"New album",JOptionPane.OK_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE, img);
				if(result == JOptionPane.OK_OPTION) {
					HashSet<Song> song = new HashSet<>();
					song.add(s);
					panel.refresh();
					try {
						//s.getAuthor().createAlbum(p.getTitle(),Integer.parseInt(p.getYear()),song);
						((RegUser) App.getAppInstance().getActualUser()).createAlbum(p.getTitle(),Integer.parseInt(p.getYear()),song);
					} catch (NumberFormatException e) {
						JOptionPane.showMessageDialog(null,"Incorrect year","Error",JOptionPane.PLAIN_MESSAGE,img);
					} catch (InvalidTitleException e) {
						JOptionPane.showMessageDialog(null,"That name is already in use","Error",JOptionPane.PLAIN_MESSAGE,img);
					} catch (NotYourMusicException e) {
						JOptionPane.showMessageDialog(null,"This song is not yours","Error",JOptionPane.PLAIN_MESSAGE,img);
					} catch (DuplicatedSongException e) {
						JOptionPane.showMessageDialog(null,"This song is already in this album","Error",JOptionPane.PLAIN_MESSAGE,img);
					} 
				}
			}
		});	
		this.add(i);
	}
}
