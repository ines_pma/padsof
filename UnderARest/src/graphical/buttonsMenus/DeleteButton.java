package graphical.buttonsMenus;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import app.media.Compilation;
import app.media.Music;
import graphical.general.MyPanel;

/**
 * Button to delete music from a compilation
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class DeleteButton extends JButton{
	private ImageIcon img = null;
	/**
	 * @param container compilation from where the music will be deleted
	 * @param contained music which will be deleted
	 * @param panel which will be refreshed
	 */
	public DeleteButton(Compilation container, Music contained, MyPanel panel) {
		try {
			this.setIcon(new ImageIcon(ImageIO.read(new File("img/bin.png"))));
		} catch (IOException e1) {
			this.setText("Delete");
		}
		try {
			img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
		} catch (IOException e1) {}
		this.setBorder(BorderFactory.createEmptyBorder());
		this.setContentAreaFilled(false);
		this.addActionListener(e-> {
			int result = JOptionPane.showConfirmDialog(null,"Delete " + contained.getName() + " from " + container.getName()+"?","Delete",JOptionPane.OK_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE,img);
			if(result == 0)
				container.deleteMusic(contained);
			
			panel.refresh();
		});
	}
}
