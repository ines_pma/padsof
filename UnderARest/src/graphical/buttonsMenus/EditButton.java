package graphical.buttonsMenus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import app.media.Song;
import app.media.exceptions.FileTooLongException;
import app.users.exceptions.InvalidTitleException;
import graphical.general.MyPanel;
import graphical.upperBar.UploadPanel;

/**
 * Button to edit a song
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

@SuppressWarnings("serial")
public class EditButton extends JButton{
	private ImageIcon img= null;
	/**
	 * It will open a panel to introduce the data and it will check it is valid. 
	 * 
	 * @param s song you want to edit
	 * @param panel panel which will be refreshed
	 */
	public EditButton(Song s, MyPanel panel) {
		try {
			this.setIcon(new ImageIcon(ImageIO.read(new File("img/edit.png"))));
		} catch (IOException e1) {this.setText("Edit");}
		try {
			img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
		} catch (IOException e1) {}
		
		setBorder(BorderFactory.createEmptyBorder());
		setContentAreaFilled(false);
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UploadPanel up = new UploadPanel(s.getName(),s.getFile());
				int result = JOptionPane.showConfirmDialog(null, up, 
			               "Edit song", JOptionPane.OK_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE, img);
			      if (result == JOptionPane.OK_OPTION) {
				         try {
							s.getAuthor().changeSongData(s,up.getTitle(),up.getPath());
							panel.refresh();
						} catch (IOException e) {
							JOptionPane.showMessageDialog(null, "Not a valid path.");
						} catch (FileTooLongException e) {
							JOptionPane.showMessageDialog(null, "Song too long.");
						} catch (InvalidTitleException e) {
							JOptionPane.showMessageDialog(null, "Not a valid name.");
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, "Something went wrong.");
						}
			      }
			   }
		});
	}

}
