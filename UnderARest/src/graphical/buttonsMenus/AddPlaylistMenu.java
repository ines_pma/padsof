package graphical.buttonsMenus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import app.App;
import app.media.Music;
import app.media.Playlist;
import app.media.exceptions.DuplicatedSongException;
import app.users.RegUser;
import app.users.exceptions.InvalidTitleException;
import graphical.general.MyPanel;

/**
 * Menu to add music to a playlist.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class AddPlaylistMenu extends JMenu{
	private ImageIcon img = null;
	/**
	 * It has an item to create a new playlist and one for each one the current user already has
	 * @param m music to add to the playlist
	 * @param panel panel which will be refreshed if a new playlist is created
	 */
	public AddPlaylistMenu(Music m, MyPanel panel) {
		super("Add to playlist");
		
		Set<Playlist> playlists = ((RegUser)App.getAppInstance().getActualUser()).getPlaylists();
		
		
		try {
			img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
		} catch (IOException e1) {}
		
		
		for(Playlist p: playlists) {
			JMenuItem i = new JMenuItem(p.getName());
			i.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						p.addMusic(m);
						panel.refresh();
					} catch (DuplicatedSongException e) {
						JOptionPane.showMessageDialog(null,"This music can't be added to this playlist because part of it is already in it",
								"Error",JOptionPane.PLAIN_MESSAGE,img);
					}
				}
			});	
			this.add(i);
		}
		
		
		JMenuItem i = new JMenuItem("New playlist");
		i.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String name = (String) JOptionPane.showInputDialog(null,"Playlist name","Create playlist",JOptionPane.QUESTION_MESSAGE,img,null,null);
				try {
					((RegUser)App.getAppInstance().getActualUser()).createPlaylist(name,m);
					panel.refresh();
				} catch (InvalidTitleException e) {
					JOptionPane.showMessageDialog(null,"That name is already in use","Invalid title",JOptionPane.PLAIN_MESSAGE,img);
				} catch (DuplicatedSongException e) {
					JOptionPane.showMessageDialog(null,"Duplicated song", "Duplicated song", JOptionPane.PLAIN_MESSAGE,img);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,"Something hasn't worked", "Error",JOptionPane.PLAIN_MESSAGE,img);
				} 
				finally {
					panel.refresh();
				}
			}
		});	
		this.add(i);
	}
}
