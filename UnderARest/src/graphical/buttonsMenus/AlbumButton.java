package graphical.buttonsMenus;

import javax.swing.BorderFactory;
import javax.swing.JButton;

import app.media.Compilation;
import graphical.general.LoggedPanel;
import graphical.personalPage.CompilationPanel;

/**
 *Button which takes you to the album's page
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class AlbumButton extends JButton{
		/**
		 * @param parent panel where the album page will appeear if clicked
		 * @param c	album the button references
		 */
		public AlbumButton(LoggedPanel parent, Compilation c) {
			setBorder(BorderFactory.createEmptyBorder());
			setContentAreaFilled(false);
			setMaximumSize(this.getMinimumSize());
			setText(c.getName());
			addActionListener(e -> parent.changeCenter(new CompilationPanel(c,parent)));
		}

}
