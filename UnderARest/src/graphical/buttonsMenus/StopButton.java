package graphical.buttonsMenus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import app.media.Music;
/**
 * Botton to stop the music
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class StopButton extends JButton{
	/** 
	 * stops music
	 * @param s music to be stopped
	 */
	public StopButton(Music s){
		try {
			this.setIcon(new ImageIcon(ImageIO.read(new File("img/stop.png"))));
		} catch (IOException e1) {
			this.setText("Stop");
		}
		this.setBorder(BorderFactory.createEmptyBorder());
		this.setContentAreaFilled(false);
		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
			try {
				s.stop();
			} catch (InterruptedException e) {
			}
			}
		});
	}
}
