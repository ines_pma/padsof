package graphical.buttonsMenus;

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import app.App;
import app.media.Compilation;
import app.users.RegUser;
import graphical.Colors;
import graphical.general.MyPanel;

/**
 * Panel which has the buttons a compilation should have (play and compilation menu)
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class CompilationButtons extends JPanel {
	
	/**
	 * It has a play button, and a menu to add it to a playlist or delete it if yours.
	 * @param c compilation the button references
	 * @param panel panel which will be refreshed if a new playlist is created
	 */
	public CompilationButtons(Compilation c, MyPanel panel) {
		setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
		setBackground(Colors.getColor(5));
		PlayButton play = null;
		play = new PlayButton(c);
		add(play);
				
		if(App.getAppInstance().getActualUser() instanceof RegUser) {
			JMenuBar mb = new JMenuBar();
			add(mb);
			JMenu menu = new JMenu();
			mb.add(menu);

			try {	menu.setIcon(new ImageIcon(ImageIO.read(new File("img/menu.png"))));
			} catch (IOException e) {menu.setText("Menu");}

			menu.setMaximumSize(new Dimension(30, 30));
			menu.setBorder(BorderFactory.createEmptyBorder());
			menu.setContentAreaFilled(false);
			this.setBorder(BorderFactory.createEmptyBorder());
			AddPlaylistMenu addplaylist = new AddPlaylistMenu(c,panel);
			menu.add(addplaylist);
			if(c.getAuthor().equals(App.getAppInstance().getActualUser())) {
				JMenuItem delete = new JMenuItem("Delete");
				delete.addActionListener(e->c.delete());
				menu.add(delete);
			}			
		}
		
		setMaximumSize(this.getMinimumSize());
	}
}
