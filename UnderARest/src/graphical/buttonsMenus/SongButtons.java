package graphical.buttonsMenus;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import app.App;
import app.media.Song;
import app.users.UserAux;
import graphical.Colors;
import graphical.general.MyPanel;

/**
 * Panel with the play button and the songMenuButton
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class SongButtons extends JPanel {
		/**
		 * @param s song which the buttons refers to
		 * @param panel panel which will be refreshed
		 */
		public SongButtons(Song s, MyPanel panel) {
			setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
			setBackground(Colors.getColor(5));
			PlayButton play = null;
			play = new PlayButton(s);
			this.add(play);
			
			if(App.getAppInstance().getActualUser() instanceof UserAux) {
				SongMenuButton menu = new SongMenuButton(s,panel);
				this.add(menu);
			}
			setMaximumSize(this.getMinimumSize());
		}
}
