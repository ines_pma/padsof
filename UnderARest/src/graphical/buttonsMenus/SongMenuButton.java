package graphical.buttonsMenus;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import app.App;
import app.media.Song;
import app.users.RegUser;
import app.users.exceptions.ExistingNotificationException;
import graphical.general.MyPanel;

/**
 * Song menu
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class SongMenuButton extends JMenuBar{
	private RegUser user = null;
	private ImageIcon logo = null;
	/**
	 * Depending on the user's relation with the song, the options shown will differ
	 * @param s Song the menu refers to
	 * @param panel panel which will be refreshed.
	 */
	public SongMenuButton(Song s, MyPanel panel) {
		if (App.getAppInstance().getActualUser() instanceof RegUser)
			user = (RegUser) App.getAppInstance().getActualUser();
		
		JMenu menu = new JMenu();
		this.add(menu);
		ImageIcon img;
		try {
			img = new ImageIcon(ImageIO.read(new File("img/menu.png")));
			menu.setIcon(img);
		} catch (IOException e1) {menu.setText("Menu");}
		try {
			logo  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
		} catch (IOException e1) {}
		menu.setMaximumSize(new Dimension(30, 30));
		menu.setBorder(BorderFactory.createEmptyBorder());
		menu.setContentAreaFilled(false);
		this.setBorder(BorderFactory.createEmptyBorder());
		
		if(user != null ) {
			if(!(user.equals(s.getAuthor()))) {
				JMenuItem report = new JMenuItem("Report");
				menu.add(report);
				report.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						String rep = (String) JOptionPane.showInputDialog(null,"Write a brief message for the administrator to process de report.","Report",JOptionPane.QUESTION_MESSAGE,logo,null,null);
						if (rep != null) {
							try {
								user.report(s,rep);
							} catch (ExistingNotificationException e) {
								JOptionPane.showMessageDialog(null, "This report has already been made.","Error",JOptionPane.PLAIN_MESSAGE,logo);		
							}
						}
					}
				});
			}
			AddPlaylistMenu addplaylist = new AddPlaylistMenu(s,panel);
			menu.add(addplaylist);
		}
		
		if(user == null|| user.equals(s.getAuthor())) {
			JMenuItem delete = new JMenuItem("Delete");
			delete.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					int result = JOptionPane.showConfirmDialog(null, "Are you sure you want to permanently delete this song?","Delete",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE, logo);
					if (result == JOptionPane.YES_OPTION) {
						user.getSongs().remove(s);
						s.delete();
						panel.refresh();
					}
				}
			});
			menu.add(delete);
		}

		if(user != null && user.equals(s.getAuthor())) {
			JMenuItem addAlbum = new AddAlbumMenu(s,panel);
			menu.add(addAlbum);
		}
	}

}
