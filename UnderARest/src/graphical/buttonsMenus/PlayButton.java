package graphical.buttonsMenus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import app.media.Music;
import pads.musicPlayer.exceptions.Mp3PlayerException;
/**
 * Button to play the music
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class PlayButton extends JButton{
	private ImageIcon img = null; 
		/** 
		 * Plays music
		 * @param s music to be played
		 */
		public PlayButton(Music s){
			try {
				this.setIcon(new ImageIcon(ImageIO.read(new File("img/play.png"))));
			} catch (IOException e1) {
				this.setText("Play");
			}
			try {
				img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
			} catch (IOException e1) {}
			
			setBorder(BorderFactory.createEmptyBorder());
			setContentAreaFilled(false);
			addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					try {
						s.play();
					} catch (FileNotFoundException | Mp3PlayerException  e) {
						JOptionPane.showMessageDialog(null,"This song can not be played at the moment.","Error",JOptionPane.PLAIN_MESSAGE,img);
					}
				}
			});
		}
}
