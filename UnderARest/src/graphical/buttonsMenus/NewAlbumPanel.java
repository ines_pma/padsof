package graphical.buttonsMenus;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Panel where you can input the information needed to create a new album.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class NewAlbumPanel extends JPanel{
	private JTextField year = new JTextField();
	private JTextField title = new JTextField();
	
	public NewAlbumPanel() {
		setLayout(new GridLayout(2,2));
		add(new JLabel("Title"));
		add(title);
		add(new JLabel("Year"));
		add(year);
	}
	
	/**
	 * @return album title
	 */
	public String getTitle(){
		return title.getText();
	}
	
	/**
	 * @return year the album was created
	 */
	public String getYear() {
		return year.getText();
	}
}
