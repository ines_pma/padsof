package graphical.upperBar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;

import app.users.Admin;
import app.users.RegUser;
import app.users.User;
import graphical.Colors;
import graphical.adminPage.AdminPagePanel;
import graphical.general.LoggedPanel;
import graphical.personalPage.PersonalPagePanel;
/**
 * Button which takes you to your personal page
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class PersonalPageButton extends JButton {
		
	/**
	 * If its anonymous it wont do anything
	 * @param u actual user
	 * @param parent center panel
	 */
	public PersonalPageButton(User u, LoggedPanel parent) {
		super("Guest");
		this.setBorder(BorderFactory.createEmptyBorder());
		this.setContentAreaFilled(false);
		this.setForeground(Colors.getColor(0));
	}
	
	/**
	 * If it's a RegUser it will take you to personalPage
	 * @param u actual user
	 * @param parent center panel
	 */
	public PersonalPageButton(RegUser u, LoggedPanel parent) {
		super(u.getUserName());
		this.setForeground(Colors.getColor(0));
		this.setBorder(BorderFactory.createEmptyBorder());
		this.setContentAreaFilled(false);
		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				parent.changeCenter(new PersonalPagePanel(u,parent));
			}
		});
		
	}
	
	/**
	 * It will take you to adminPage
	 * @param admin 
	 * @param parent center panel
	 */
	public PersonalPageButton(Admin admin, LoggedPanel parent) {
		super("Admin");
		this.setForeground(Colors.getColor(0));
		this.setBorder(BorderFactory.createEmptyBorder());
		this.setContentAreaFilled(false);
		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				parent.changeCenter(new AdminPagePanel(parent));
			}
		});
		
	}
}
