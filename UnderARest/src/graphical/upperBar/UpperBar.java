package graphical.upperBar;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import app.App;
import app.users.Admin;
import app.users.RegUser;
import app.users.User;
import app.users.UserState;
import graphical.Colors;
import graphical.MainWindow;
import graphical.authentication.Register;
import graphical.buttonsMenus.PreviousButton;
import graphical.general.Home;
import graphical.general.LoggedPanel;
import graphical.general.SearchPanel;
import graphical.general.UpgradePanel;
/**
 * Upper bar, remains throughout the program
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class UpperBar extends JPanel{
	private JButton home;
	private JButton title = new JButton("   Under a rest");
	private JButton username;
	private JTextField searchBar = new JTextField("Search",40);
	private LoggedPanel parent;
	private JPanel button = new JPanel();
	private MainWindow frame;
	private UploadButton upload = new UploadButton();

	/**
	 * @param parent center panel
	 * @param frame frame where the app is running
	 */
	public UpperBar(LoggedPanel parent, MainWindow frame) {
		this.parent = parent;
		this.frame = frame;
		setBorder(new EmptyBorder(5,5,5,5));
		BoxLayout layout = new BoxLayout(this, BoxLayout.X_AXIS);
		setLayout(layout);
		title.setBorder(BorderFactory.createEmptyBorder());
		title.setContentAreaFilled(false);
		title.addActionListener(e->parent.changeCenter(new Home(parent)));
		title.setForeground(Colors.getColor(4));
		
		setBackground(Colors.getColor(3));
		
		User u = App.getAppInstance().getActualUser();
		if (u instanceof RegUser) {
			username = new PersonalPageButton((RegUser) u,parent);
			upload = new UploadButton((RegUser) u);
		}else if(u instanceof Admin)
			username = new PersonalPageButton((Admin) u,parent);
		else
			username = new PersonalPageButton(u,parent);
		this.add(new PreviousButton(parent));
		createHome();
		createSearchBar();
		createUpgrade();

		this.add(title);
		this.add(Box.createHorizontalGlue());
		this.add(searchBar);
		JPanel right = new JPanel();
		right.setLayout(new FlowLayout(FlowLayout.RIGHT));
		right.add(button);
		right.add(upload);
		right.add(username);
		right.setBackground(Colors.getColor(3));
		this.add(right);
	}

	private void createHome() {
		try {
			home = new JButton(new ImageIcon(ImageIO.read(new File("img/mini_logo.png"))));
		} catch (IOException e) {}
		this.add(home);
		home.setBorder(BorderFactory.createEmptyBorder());
		home.setContentAreaFilled(false);
		home.addActionListener(e->parent.changeCenter(new Home(parent)));
	}


	private void createUpgrade() {
		if(App.getAppInstance().getActualUser() == App.getAdmin()) {
			button.setVisible(false);
			return;
		}
		
		if(App.getAppInstance().getActualUser().getUserState() == UserState.REGISTERED ||
		   App.getAppInstance().getActualUser().getUserState() == UserState.PREMIUM) {
			JButton upgrade = new JButton("Upgrade");
			upgrade.setBorder(BorderFactory.createEmptyBorder());
			upgrade.setOpaque(false);
			upgrade.setBackground(Colors.getColor(1));
			button.add(upgrade);
			if (App.getAppInstance().getActualUser().getUserState() == UserState.PREMIUM)
				upgrade.setText("Renew");
			upgrade.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					parent.changeCenter(new UpgradePanel());
				}
			});
			return;
		}
		JButton register = new JButton("Register");
		button.add(register);
		register.setBorder(BorderFactory.createEmptyBorder());
		register.setOpaque(false);
		register.setBackground(Colors.getColor(1));
		
		//register.setForeground(Colors.getColor(1));
		register.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				parent.changeCenter(new Register(frame));
			}

		});
		return;
	}

	private void createSearchBar() {
		searchBar.setMaximumSize(new Dimension(100,20));
		searchBar.setBorder(BorderFactory.createEmptyBorder());
		searchBar.setBackground(Colors.getColor(4));
		//when you click on the searchBox all the text is selected
		searchBar.addFocusListener(new FocusAdapter() {
				  public void focusGained(FocusEvent fEvt) {
				    JTextField tField = (JTextField)fEvt.getSource();
				    tField.selectAll();
				  }
				});

		//When the enter key is pressed, the search is made
		searchBar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					parent.changeCenter(new SearchPanel(searchBar.getText(),frame,parent));//cambiar panel central a resultado
					searchBar.setText("Search");
					searchBar.selectAll();
				}
			});
	}
}
