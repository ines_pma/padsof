package graphical.upperBar;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Panel to upload a song
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class UploadPanel extends JPanel{
	private JLabel title = new JLabel("Title");
	private JTextField titleInput = new JTextField();
	private JLabel file = new JLabel("File");
	private JButton openExplorer = new JButton("Choose file");
	private JTextField path = new JTextField(" ");
	private JFileChooser explorer = new JFileChooser();
	
	public UploadPanel(){
		setLayout(new GridLayout(2,3));
		add(title);
		add(titleInput);
		int height = (int) titleInput.getMinimumSize().getHeight();
		int width = (int) titleInput.getMaximumSize().getWidth();
		add(new JLabel(""));
		titleInput.setMaximumSize(new Dimension(width,height+10));
		add(file);
		add(path);
		add(openExplorer);
		
		
		openExplorer.addActionListener(e-> {
			int returnVal = explorer.showOpenDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION)
				path.setText(explorer.getSelectedFile().toPath().toString());
		});
		
	}
	
	/**
	 * Initializes the title and path
	 * @param name
	 * @param file2
	 */
	public UploadPanel(String name, String file2) {
		this();
		titleInput.setText(name);
		path.setText(file2);
	}

	/**
	 * @return song's title
	 */
	public String getTitle() {
		return titleInput.getText();
	}
	
	/**
	 * @return song's path
	 */
	public String getPath() {
		return path.getText();
	}
}
