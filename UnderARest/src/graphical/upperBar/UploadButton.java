package graphical.upperBar;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import app.media.exceptions.FileTooLongException;
import app.users.RegUser;
import app.users.exceptions.ExistingNotificationException;
import app.users.exceptions.InvalidTitleException;
/**
 * Button to upload a song
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class UploadButton extends JButton{
		private ImageIcon img = null;
		/**
		 * It opens a window to introduce the information
		 * @param u actualUser
		 */
		public UploadButton(RegUser u) {
			try {
				this.setIcon(new ImageIcon(ImageIO.read(new File("img/add.png"))));
			} catch (IOException e1) {this.setText("Add");}
			try {
				img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
			} catch (IOException e1) {}
			
			setBorder(BorderFactory.createEmptyBorder());
			setContentAreaFilled(false);
			addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					UploadPanel up = new UploadPanel();
					int result = JOptionPane.showConfirmDialog(null, up, 
				               "Upload song", JOptionPane.OK_CANCEL_OPTION,
				               JOptionPane.QUESTION_MESSAGE, img);
				      if (result == JOptionPane.OK_OPTION) {
					         try {
								u.uploadSong(up.getTitle(),up.getPath());
							} catch (IOException e) {
								JOptionPane.showMessageDialog(null, "Not a valid path.","Error",JOptionPane.PLAIN_MESSAGE,img);
							} catch (FileTooLongException e) {
								JOptionPane.showMessageDialog(null, "Song too long.","Error",JOptionPane.PLAIN_MESSAGE,img);
							} catch (InvalidTitleException e) {
								JOptionPane.showMessageDialog(null, "Not a valid name.","Error",JOptionPane.PLAIN_MESSAGE,img);
							} catch (ExistingNotificationException e) {
								JOptionPane.showMessageDialog(null, "Something went wrong.","Error",JOptionPane.PLAIN_MESSAGE,img);
							}
				      }
				   }
			});
		}

		public UploadButton() {
			setBorder(BorderFactory.createEmptyBorder());
			setContentAreaFilled(false);
		}
}
