package graphical.personalPage;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import app.media.Album;
import app.media.Song;
import app.users.RegUser;
import graphical.Colors;
import graphical.buttonsMenus.AlbumButton;
import graphical.buttonsMenus.EditButton;
import graphical.buttonsMenus.PlayButton;
import graphical.buttonsMenus.SongButtons;
import graphical.general.BlankSpace;
import graphical.general.LoggedPanel;
import graphical.general.MyPanel;

/**
 * Panel to show your music
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

//TODO delete album
//TODO delete playlist

@SuppressWarnings("serial")
public class YourMusicPanel extends JPanel implements MyPanel{
	private RegUser u;
	private JPanel upp = new JPanel();
	private JPanel pvp = new JPanel();
	private JPanel alp = new JPanel();
	private LoggedPanel parent;
	private PersonalPageMainPanel personalPage;
	
	/**
	 * @param u user whose music is displayed
	 */
	public YourMusicPanel(RegUser u, LoggedPanel parent, PersonalPageMainPanel p) {
		personalPage = p;
		this.u = u;
		this.parent = parent;
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBackground(Colors.getColor(4));
		String font;		
		
		JLabel pv = new JLabel("Pending to validate");
		font = pv.getFont().getName();
		pv.setFont(new Font(font, Font.BOLD , 30));
		pv.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.add(pv);
		
		
		pvp.setLayout(new GridLayout(0,3));
		pvp.setAlignmentX(Component.LEFT_ALIGNMENT);
		pvp.setBackground(Colors.getColor(4));
		this.add(pvp);
		
		JLabel up = new JLabel("Uploaded");
		up.setFont(new Font(font, Font.BOLD , 30));
		up.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.add(up);
		
		upp.setLayout(new GridLayout(0,3));
		upp.setAlignmentX(Component.LEFT_ALIGNMENT);
		upp.setBackground(Colors.getColor(4));
		this.add(upp);
		
		JLabel al = new JLabel("Albums");
		al.setFont(new Font(font, Font.BOLD , 30));
		al.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.add(al);
		
		alp.setLayout(new GridLayout(0,3));
		alp.setAlignmentX(Component.LEFT_ALIGNMENT);
		alp.setBackground(Colors.getColor(4));
		this.add(alp);
		refresh();	
	}

	//maybe en vez de pasar this pasar personal page y hacer refresh de playlist
	/**
	 * when a change is made it repaints the panel with the updated information
	 */
	@Override
	public void refresh() {
		upp.removeAll();
		Set<Song> validated = u.getValidatedSongs();
		if (validated.isEmpty()) upp.add(new BlankSpace());
		for (Song s: validated) {
			upp.add(new JLabel(s.getName()));
			upp.add(new JLabel(s.getDurationString()));
			SongButtons sb = new SongButtons(s,personalPage);
			sb.setBackground(Colors.getColor(4));
			upp.add(sb);
		}
		int height = (int) upp.getMinimumSize().getHeight();
		int width = (int) upp.getMaximumSize().getWidth();
		upp.setMaximumSize(new Dimension(width,height+10));
		

		pvp.removeAll();
		Set<Song> pending = u.getPendingSongs();
		if (pending.isEmpty()) pvp.add(new BlankSpace());
		for (Song s: pending) {
			pvp.add(new JLabel(s.getName()));
			pvp.add(new JLabel(s.getSongState().name()));
			JPanel pb = new JPanel();
			pb.add(new PlayButton(s));
			pb.add(new EditButton(s,personalPage));
			pb.setBackground(Colors.getColor(4));
			pvp.add(pb);
		}
		height = (int) pvp.getMinimumSize().getHeight();
		width = (int) pvp.getMaximumSize().getWidth();
		pvp.setMaximumSize(new Dimension(width,height+10));
		Set<Album> albums = u.getAlbums();
		for (Album a: albums) {
			alp.add(new AlbumButton(parent,a));
			alp.add(new JLabel(a.getDurationString()));
			alp.add(new PlayButton(a));
		}
		height = (int) alp.getMinimumSize().getHeight();
		width = (int) alp.getMaximumSize().getWidth();
		alp.setMaximumSize(new Dimension(width,height+10));
		
		revalidate();
		repaint();
	}

}
