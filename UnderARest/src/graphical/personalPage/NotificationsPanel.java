package graphical.personalPage;

import java.awt.Component;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import app.App;
import app.notification.UserNotif;
import app.users.RegUser;
import app.users.UserState;
import graphical.Colors;
/**
 * Show's a user notifications
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class NotificationsPanel extends JPanel{

	/**
	 * @param u user whose notifications are to be shown
	 */
	public NotificationsPanel(RegUser u) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBackground(Colors.getColor(4));
		Set<UserNotif> notif = u.getNotifications();
		JLabel days = new JLabel();
		if(u.getUserState() == UserState.PREMIUM)
			add(days);
		days.setText("Your account will expire: " + ((RegUser)App.getAppInstance().getActualUser()).getExpDate());
		days.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		for(UserNotif n: notif) {
			if(n.getSong() == null) continue;
			JLabel not = new JLabel(n.toString());
			not.setAlignmentX(Component.LEFT_ALIGNMENT);
			add(not);
			
		}
		
	}

}
