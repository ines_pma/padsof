package graphical.personalPage;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import app.media.Compilation;
import app.media.Music;
import graphical.Colors;
import graphical.buttonsMenus.DeleteButton;
import graphical.buttonsMenus.PlayButton;
import graphical.general.LoggedPanel;
import graphical.general.MyPanel;
/**
 * Panel to show the music in a compilation
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class CompilationPanel extends JPanel implements MyPanel{
	private JPanel musicPanel = new JPanel();
	private Set<Music> music;
	private Compilation c;
	
	/**
	 * @param c compilation to show
	 * @param parent panel where it is displayed
	 */
	public CompilationPanel(Compilation c, LoggedPanel parent) {
		this.c = c;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(100,100,100, 100));
		setBackground(Colors.getColor(4));
		JLabel title = new JLabel(c.getName()); 
		title.setFont(new Font(title.getFont().getName(), Font.BOLD , 30));
		title.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(title);
		musicPanel.setLayout(new GridLayout(0,2));
		add(musicPanel);
		refresh();
	}

	/**
	 * Updates the panel after a change
	 */
	@Override
	public void refresh() {
		musicPanel.removeAll();
		music = c.getMusic();
		for (Music m: music) {
			musicPanel.add(new JLabel(m.getName()));
			musicPanel.setBackground(Colors.getColor(4));
			JPanel p = new JPanel();
			p.setBackground(Colors.getColor(4));
			p.add(new PlayButton(m));
			p.add(new DeleteButton(c,m,this));
			int height = (int) p.getMinimumSize().getHeight();
			int width = (int) p.getMaximumSize().getWidth();
			p.setMaximumSize(new Dimension(width,height+10));
			musicPanel.add(p);
			height = (int) musicPanel.getMinimumSize().getHeight();
			width = (int) musicPanel.getMaximumSize().getWidth();
			musicPanel.setMaximumSize(new Dimension(width,height+10));
		}
	}
}
