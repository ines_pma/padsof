package graphical.personalPage;

import java.awt.CardLayout;

import javax.swing.JPanel;

import app.users.RegUser;
import graphical.Colors;
import graphical.general.LoggedPanel;
import graphical.general.MyPanel;

/**
 * This panel contains the user's tabs and menu to change between them.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class PersonalPageMainPanel extends JPanel implements MyPanel{
	private PlaylistsPanel pl;
	private FollowingPanel fl;
	private NotificationsPanel notif;
	private YourMusicPanel ym;
	
	/**
	 * @param c panel where the other tabs are contained
	 * @param u user whose page is seen
	 * @param parent panel where it is displayed
	 */
	public PersonalPageMainPanel(CardLayout c,RegUser u, LoggedPanel parent){
		setBackground(Colors.getColor(4));
		setLayout(c);
				
		notif = new NotificationsPanel(u);
		add(notif,"Notifications");
		pl = new PlaylistsPanel(u, parent);
		add(pl,"Playlists");
		fl = new FollowingPanel(u,parent);
		add(fl,"Following");
		ym = new YourMusicPanel(u,parent,this);
		add(ym,"Your music");
	}
	
	public void refresh() {
		pl.refresh();
		ym.refresh();
	}
}
