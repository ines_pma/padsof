package graphical.personalPage;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import app.users.RegUser;
import graphical.Colors;
import graphical.general.LoggedPanel;

/**
 * This panel contains the user's tabs and changes between them.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class PersonalPagePanel extends JPanel {

	/**
	 * @param u user who is seeing their own page
	 * @param parent center panel of the frame
	 */
	public PersonalPagePanel(RegUser u, LoggedPanel parent) {
		setLayout(new BorderLayout());
		CardLayout c = new CardLayout();
		setBackground(Colors.getColor(4));
		
		PersonalPageMainPanel p = new PersonalPageMainPanel(c,u, parent);
		p.setBorder(new EmptyBorder(20,100,0,100));
		p.setBackground(Colors.getColor(4));
		this.add(p,BorderLayout.CENTER);
		
		PersonalPageTab tabs = new PersonalPageTab(p,c,u);
		tabs.setBackground(Colors.getColor(4));
		this.add(tabs,BorderLayout.NORTH);
				
	}
}
