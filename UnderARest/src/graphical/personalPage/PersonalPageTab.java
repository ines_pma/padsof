package graphical.personalPage;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import app.users.RegUser;
import graphical.Colors;

/**
 * Panel to change between tabs
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class PersonalPageTab extends JPanel{
	
	public PersonalPageTab(PersonalPageMainPanel p, CardLayout c,RegUser u) {
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(100,100,0,100));
		
		JLabel title = new JLabel(u.getUserName()); 
		title.setFont(new Font(title.getFont().getName(), Font.BOLD , 30));
		title.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(title);
	
		JPanel tab = new JPanel();
		tab.setBackground(Colors.getColor(4));
		tab.setLayout(new BoxLayout(tab, BoxLayout.X_AXIS));
		tab.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(tab);
		
		JButton notif = new JButton("Notifications ");
		JButton pl = new JButton(" Playlists ");
		JButton fl = new JButton(" Following ");
		JButton ym = new JButton(" Your music ");
		
		notif.setForeground(Colors.getColor(3));
		notif.setBorder(BorderFactory.createEmptyBorder());
		notif.setContentAreaFilled(false);
		notif.addActionListener(e->{ 
			c.show(p,"Notifications");
			notif.setForeground(Colors.getColor(3));
			pl.setForeground(Color.black);
			fl.setForeground(Color.black);
			ym.setForeground(Color.black);
		});
		notif.setMaximumSize(notif.getMinimumSize());
		tab.add(notif);
		
		tab.add(new JSeparator(SwingConstants.VERTICAL));
		
		
		pl.setBorder(BorderFactory.createEmptyBorder());
		pl.setContentAreaFilled(false);
		pl.addActionListener(e-> {
			c.show(p,"Playlists");
			notif.setForeground(Color.black);
			pl.setForeground(Colors.getColor(3));
			fl.setForeground(Color.black);
			ym.setForeground(Color.black);
		});
		pl.setMaximumSize(pl.getMinimumSize());
		tab.add(pl);
		
		tab.add(new JSeparator(SwingConstants.VERTICAL));
		
		
		fl.setBorder(BorderFactory.createEmptyBorder());
		fl.setContentAreaFilled(false);
		fl.addActionListener(e-> {
			c.show(p,"Following");
			notif.setForeground(Color.black);
			pl.setForeground(Color.black);
			fl.setForeground(Colors.getColor(3));
			ym.setForeground(Color.black);});
		fl.setMaximumSize(fl.getMinimumSize());
		tab.add(fl);
		
		tab.add(new JSeparator(SwingConstants.VERTICAL));
		
		
		ym.setBorder(BorderFactory.createEmptyBorder());
		ym.setContentAreaFilled(false);
		ym.addActionListener(e-> {
			c.show(p,"Your music");
			notif.setForeground(Color.black);
			pl.setForeground(Color.black);
			fl.setForeground(Color.black);
			ym.setForeground(Colors.getColor(3));
			});
		ym.setMaximumSize(ym.getMinimumSize());
		tab.add(ym);
		
		tab.setMaximumSize(tab.getMinimumSize());
		
	}
}
