package graphical.personalPage;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import app.media.Playlist;
import app.users.RegUser;
import graphical.Colors;
import graphical.buttonsMenus.PlayButton;
import graphical.general.LoggedPanel;
import graphical.general.MyPanel;

/**
 * Shows a user's playlist
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class PlaylistsPanel extends JPanel implements MyPanel{
	private JPanel panel;
	private RegUser u;
	private LoggedPanel parent;
	/**
	 * @param u user whose playlist are displayed
	 * @param parent center panel
	 */
	public PlaylistsPanel(RegUser u, LoggedPanel parent) {
		this.u = u;
		this.parent = parent;
		setBackground(Colors.getColor(4));
		panel = new JPanel();
		panel.setBackground(Colors.getColor(4));
		panel.setLayout(new GridLayout(0,3,100,0));		
		add(panel);
		refresh();
		//panel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
	}

	public void refresh() {
		panel.removeAll();
		Set<Playlist> playlists = u.getPlaylists();
		for(Playlist p: playlists) {
			JButton b = new JButton(p.getName());
			b.setBorder(BorderFactory.createEmptyBorder());
			b.setContentAreaFilled(false);
			b.addActionListener(e-> parent.changeCenter(new CompilationPanel(p, parent)));
			
			panel.add(b);
			panel.add(new JLabel(p.getDurationString()));
			panel.add(new PlayButton(p));
		}
		int height = (int) panel.getMinimumSize().getHeight();
		int width = (int) panel.getMaximumSize().getWidth();
		panel.setMaximumSize(new Dimension(width,height+10));
		
	}

}
