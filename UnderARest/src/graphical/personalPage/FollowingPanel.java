package graphical.personalPage;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Set;

import javax.swing.JPanel;

import app.users.RegUser;
import graphical.Colors;
import graphical.authorPage.AuthorButton;
import graphical.authorPage.FollowUnfollowPanel;
import graphical.general.LoggedPanel;
/**
 * Shows who you are following
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class FollowingPanel extends JPanel {

	/**
	 * @param user user you're going to show their followees
	 * @param parent panel where it is displayed
	 */
	public FollowingPanel(RegUser user, LoggedPanel parent) {
		setBackground(Colors.getColor(4));
		JPanel panel = new JPanel();
		panel.setBackground(Colors.getColor(4));
		panel.setLayout(new GridLayout(0,2,100,0));
		Set<RegUser> users = user.getFollowing();
		
		for(RegUser u: users) {			
			panel.add(new AuthorButton(u, parent));
			panel.add(new FollowUnfollowPanel(u,user));
		}
		int height = (int) panel.getMinimumSize().getHeight();
		int width = (int) panel.getMaximumSize().getWidth();
		panel.setMaximumSize(new Dimension(width,height+10));
		add(panel);
		
	}

}
