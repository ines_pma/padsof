package graphical;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import app.App;
import graphical.authentication.Login;

/**
 * Frame where the app runs
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class MainWindow extends JFrame{
	private Login login;
	private Container contenedor;
	
	public MainWindow(){
		super("UnderARest");
		
		contenedor = this.getContentPane();
		login = new Login(this);
		contenedor.add(login);
		login.setVisible(true);
		setVisible(true);
		
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);	
		setMinimumSize(new Dimension(900, 900));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() { 
			public void windowClosing(WindowEvent e) {
				App.turnOffApp();
				dispose();
				System.exit(1);
			}
		});
	}
		
	 /**
	  * changes the panel displayed
	 * @param newPanel panel to display
	 */
	public void createChangePanel(JPanel newPanel) {
         contenedor.removeAll();
         contenedor.add(newPanel);
         validate();
         setVisible(true);
      }
	
	@SuppressWarnings("unused")
	public static void main(String args[]) {
		App app = App.getAppInstance();
		MainWindow m = new MainWindow();
	}
}