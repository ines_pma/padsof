package graphical;

import java.awt.Color;
/**
 * Colors used in the app
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
public class Colors {
	/**
	 * @param num
	 * @return the specified color
	 */
	public static Color getColor(int num) {
		if(num == 0) return new Color(60,46,62);
		else if(num == 1) return new Color(253,173,134);
		else if(num == 4) return new Color(237,235,230);
		else if(num == 3) return new Color(138,202,199);
		else if(num == 2) return new Color(240,255,255);
		return Color.WHITE;
	}
}
