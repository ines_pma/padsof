package graphical.adminPage;

import java.awt.CardLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import graphical.general.LoggedPanel;

@SuppressWarnings("serial")
/**
 * This panel contains the admin's tabs and changes between them.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
public class AdminPageMainPanel extends JPanel{

	public AdminPageMainPanel(CardLayout c, LoggedPanel parent) {
		
		setBorder(new EmptyBorder(20,100,100, 100));
		setLayout(c);
				
		AdminNotificationsPanel notif = new AdminNotificationsPanel();
		add(notif,"Notifications");
		
		ConfigurationPanel config = new ConfigurationPanel();
		add(config,"Configuration");
	}

}
