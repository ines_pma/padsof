package graphical.adminPage;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Set;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import app.App;
import app.notification.AdminNotif;
import app.notification.ReportNotif;
import app.notification.ValidationNotif;
import graphical.buttonsMenus.PlayButton;
import graphical.general.MyPanel;

/**
 * Panel that shows the administrator's notifications
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

@SuppressWarnings("serial")
public class AdminNotificationsPanel extends JPanel implements MyPanel{
	private Set<AdminNotif> notif;
	
	
	public AdminNotificationsPanel() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		refresh();
	}

	/**
	 *	Loads the actual Admin's notifications and displays them with their own buttons.
	 *
	 */
	@Override
	public void refresh() {
		removeAll();
		notif = App.getAdmin().getNotifications();
		for(AdminNotif n: notif) {
			JPanel p= new JPanel();
			if(n.isReportNotif()) {
				ReportNotif rn = (ReportNotif) n;
				JPanel north = new JPanel();
				north.setLayout(new BoxLayout(north, BoxLayout.X_AXIS));
				p.setLayout(new BorderLayout());
				p.add(north, BorderLayout.NORTH);
				JLabel text = new JLabel("Report: "+ rn.getReporter().getUserName() + " has reported "
						+ rn.getSong().getName() + " by " + rn.getSong().getAuthor().getUserName());
				north.add(text);
				text.setAlignmentX(Component.LEFT_ALIGNMENT);
				north.add(Box.createHorizontalGlue());
				JCheckBox plagiarism = new JCheckBox("Plagiarism");
				plagiarism.setAlignmentX(Component.RIGHT_ALIGNMENT);
				north.add(plagiarism);
				JButton save = new JButton ("Save");
				north.add(save);
				save.setAlignmentX(Component.RIGHT_ALIGNMENT);
				save.addActionListener(e->{
					if(plagiarism.isSelected()) {
						App.getAdmin().accept(rn);
					}
					else {
						App.getAdmin().decline(rn);
					}
					refresh();
				});
				PlayButton play = new PlayButton(rn.getSong());
				north.add(play);
				play.setAlignmentX(Component.RIGHT_ALIGNMENT);
						
				p.add(new JLabel(rn.getComment()),BorderLayout.CENTER);
			}
			else if(n.isValidationNotif()) {
				ValidationNotif vn = (ValidationNotif) n;
				p.setLayout(new BoxLayout(p,BoxLayout.X_AXIS));
				JLabel notifText = new JLabel("Pending to validate: " + vn.getSong().getAuthor().getUserName() 
						+ " has uploaded " + vn.getSong().getName());
				notifText.setAlignmentX(Component.LEFT_ALIGNMENT);
				p.add(notifText);
				p.add(Box.createHorizontalGlue());
				JCheckBox explicit = new JCheckBox("Explicit");
				explicit.setAlignmentX(Component.RIGHT_ALIGNMENT);
				JCheckBox accept = new JCheckBox("Accept");
				accept.setAlignmentX(Component.RIGHT_ALIGNMENT);
				JButton save = new JButton ("Save");
				save.setAlignmentX(Component.RIGHT_ALIGNMENT);
				save.addActionListener(e->{
					if(accept.isSelected())
						App.getAdmin().accept(vn,explicit.isSelected());					
					else
						App.getAdmin().decline(vn);
					refresh();
				});
				PlayButton play = new PlayButton(vn.getSong());
				play.setAlignmentX(Component.RIGHT_ALIGNMENT);
				p.add(explicit);
				p.add(accept);
				p.add(save);
				p.add(play);				
			}
			
			int height = (int) p.getMinimumSize().getHeight();
			int width = (int) p.getMaximumSize().getWidth();
			p.setMaximumSize(new Dimension(width,height+10));
			add(p);
		}
	}

}
