package graphical.adminPage;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import graphical.general.LoggedPanel;


/**
 * Loads the admin page and contains the menu to change from notifications to configuration  
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

@SuppressWarnings("serial")
public class AdminPagePanel extends JPanel {

	public AdminPagePanel(LoggedPanel parent) {
		setLayout(new BorderLayout());
		CardLayout c = new CardLayout();
		
		AdminPageMainPanel p = new AdminPageMainPanel(c, parent);
		p.setBorder(new EmptyBorder(100,100,0,100));
		add(p,BorderLayout.CENTER);
		
		AdminPageTab tabs = new AdminPageTab(p,c);
		add(tabs,BorderLayout.NORTH);
					
	}
}
