package graphical.adminPage;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import graphical.Colors;

/**
 * Panel to change between tabs
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class AdminPageTab extends JPanel{

	AdminPageTab(AdminPageMainPanel p, CardLayout c) {
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(100,100,0,100));
		
		JLabel title = new JLabel("Admin"); 
		title.setFont(new Font(title.getFont().getName(), Font.BOLD , 30));
		title.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(title);
		
		JPanel tab = new JPanel();
		tab.setLayout(new BoxLayout(tab,BoxLayout.X_AXIS));
		tab.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(tab);
		
		JButton notif = new JButton("Notifications ");
		JButton config = new JButton(" Configuration");
		
		notif.setForeground(Colors.getColor(3));
		notif.setBorder(BorderFactory.createEmptyBorder());
		notif.setContentAreaFilled(false);
		notif.addActionListener(e->{ 
			c.show(p,"Notifications");
			notif.setForeground(Colors.getColor(3));
			config.setForeground(Color.black);
		});
		notif.setMaximumSize(notif.getMinimumSize());
		tab.add(notif);
		
		tab.add(new JSeparator(SwingConstants.VERTICAL));
		
		config.setBorder(BorderFactory.createEmptyBorder());
		config.setContentAreaFilled(false);
		config.addActionListener(e->{ 
			c.show(p,"Configuration");
			notif.setForeground(Color.black);
			config.setForeground(Colors.getColor(3));
		});
		config.setMaximumSize(config.getMinimumSize());
		tab.add(config);
		tab.setMaximumSize(tab.getMinimumSize());
		
	}

}
