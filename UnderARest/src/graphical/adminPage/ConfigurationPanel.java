package graphical.adminPage;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import app.App;
import app.general.Settings;
import app.general.exceptions.InvalidValueException;

/**
 * Panel to control the settings, allows to save new ones, if the input is valid.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

@SuppressWarnings("serial")
public class ConfigurationPanel extends JPanel{
	private Settings settings = App.getSettings();
	private ImageIcon img = null;
	
	public ConfigurationPanel() {
		try {
			img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
		} catch (IOException e1) {}
		
		JPanel p = new JPanel();
		add(p);
		p.setLayout(new GridLayout(0,3));
		p.add(new JLabel("Price($) per month"));
		p.add(new JLabel());
		JTextField pricef = new JTextField(""+ settings.getPremiumPrice());
		p.add(pricef);
		
		p.add(new JLabel("Unregistered user number of playbacks"));
		p.add(new JLabel());
		JTextField upf = new JTextField(""+settings.getNumSongAnon());
		p.add(upf);
				
		p.add(new JLabel("Registered user number of playbacks"));
		p.add(new JLabel());
		JTextField rpf = new JTextField(""+ settings.getNumSongAnon());
		p.add(rpf);
		
		p.add(new JLabel("Minimum age to be considered an adult"));
		p.add(new JLabel());
		JTextField maf = new JTextField(""+ settings.getUnderage());
		p.add(maf);
		
		p.add(new JLabel("Number of reproductions to be upgraded"));
		p.add(new JLabel());
		JTextField nrf = new JTextField(""+ settings.getRepUpgrade());
		p.add(nrf);
		
		p.add(new JLabel("Number of blocked days"));
		p.add(new JLabel());
		JTextField bdf = new JTextField(""+ settings.getBlockedDays());
		p.add(bdf);
		
		JButton save = new JButton("Save");
		p.add(new JLabel());
		p.add(new JLabel());
		p.add(save);
		save.addActionListener(e->{
			try {
				double price = Double.parseDouble(pricef.getText());
				settings.setPremiumPrice(price);
			}catch(NumberFormatException|InvalidValueException ex) {
				JOptionPane.showMessageDialog(null,"Price not correctly formated","Error",JOptionPane.PLAIN_MESSAGE,img);
			}
			try {
				int up = Integer.parseInt(upf.getText());
				settings.setNumSongAnon(up);
			}catch(NumberFormatException|InvalidValueException ex) {
				JOptionPane.showMessageDialog(null,"Unregistered number of playbacks not correctly formated","Error",JOptionPane.PLAIN_MESSAGE,img);
			}
			try {
				int rp = Integer.parseInt(rpf.getText());
				settings.setNumSongReg(rp);
			}catch(NumberFormatException|InvalidValueException ex) {
				JOptionPane.showMessageDialog(null,"Registered number of playbacks not correctly formated","Error",JOptionPane.PLAIN_MESSAGE,img);
			}
			try {
				int ma = Integer.parseInt(maf.getText());
				settings.setUnderage(ma);
			}catch(NumberFormatException|InvalidValueException ex) {
				JOptionPane.showMessageDialog(null,"Minimum age not correctly formated","Error",JOptionPane.PLAIN_MESSAGE,img);
			}
			try {
				int nr = Integer.parseInt(nrf.getText());
				settings.setRepUpgrade(nr);
			}catch(NumberFormatException|InvalidValueException ex) {
				JOptionPane.showMessageDialog(null,"Number of reproductions not correctly formated","Error",JOptionPane.PLAIN_MESSAGE,img);
			}
			
			try {
				int bd = Integer.parseInt(bdf.getText());
				settings.setBlockedDays(bd);
			}catch(NumberFormatException|InvalidValueException ex) {
				JOptionPane.showMessageDialog(null,"Blocked days not correctly formated","Error",JOptionPane.PLAIN_MESSAGE,img);
			}
			finally {
				pricef.setText(""+ settings.getPremiumPrice());
				upf.setText("" + settings.getNumSongAnon());
				rpf.setText("" + settings.getNumSongAnon());
				maf.setText("" + settings.getUnderage());
				nrf.setText("" + settings.getRepUpgrade());
				bdf.setText("" + settings.getBlockedDays());
			}
		});
		
		int height = (int) p.getMinimumSize().getHeight();
		int width = (int) p.getMaximumSize().getWidth();
		p.setMaximumSize(new Dimension(width,height+10));
	}

}
