package graphical.rowsDisplays;

import java.util.Collection;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import app.users.RegUser;
import graphical.Colors;
import graphical.general.LoggedPanel;
/**
 * Displays multiple users
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class MultipleUserDisplay extends JPanel {

	/**
	 * @param users users to display
	 * @param lp central panel
	 */
	public MultipleUserDisplay(Collection<RegUser> users, LoggedPanel lp) {
		setBackground(Colors.getColor(4));
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		
		int cont = 0;
		for(RegUser u: users) {
			add(Box.createVerticalStrut(5));
			this.add(new UserRow(u,lp));
			add(Box.createVerticalStrut(5));
			cont ++;
			if(cont == 10)
				break;
		}
	}
}
