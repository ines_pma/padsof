package graphical.rowsDisplays;

import java.util.Collection;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import app.App;
import app.media.Album;
import graphical.Colors;
import graphical.general.LoggedPanel;
import graphical.general.MyPanel;

/**
 * Shows multiple albums
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class MultipleAlbumDisplay extends JPanel implements MyPanel{
	private LoggedPanel lp;
	private Collection<Album> albums;
	
	/**
	 * @param albums albums to display
	 * @param lp central panel
	 */
	public MultipleAlbumDisplay(Collection<Album> albums, LoggedPanel lp) {
		this.lp = lp;
		this.albums = albums;
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBackground(Colors.getColor(4));
		if (albums == null)
			return;
		refresh();
	}

	/**
	 * updates the panel
	 */
	@Override
	public void refresh() {
		removeAll();
		int cont = 0;
		for(Album a: albums) {
			if (!(a.getExplicit()) || !(App.getAppInstance().getActualUser().isUnderage())) {
				add(Box.createVerticalStrut(5));
				this.add(new AlbumRow(a,lp,this));
				add(Box.createVerticalStrut(5));
				cont ++;
				if(cont == 100)
					break;
			}
		}
		
	}
}
