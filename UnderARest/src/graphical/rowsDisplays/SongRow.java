package graphical.rowsDisplays;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import app.media.Song;
import graphical.Colors;
import graphical.authorPage.AuthorButton;
import graphical.buttonsMenus.SongButtons;
import graphical.general.LoggedPanel;
import graphical.general.MyPanel;
/**
 * Displays one song's information
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class SongRow extends JPanel{
	
	/**
	 * @param s song to display
	 * @param lp central panel
	 * @param panel panel to refresh
	 */
	public SongRow(Song s, LoggedPanel lp, MyPanel panel) {
		GridLayout gl = new GridLayout(1,0);
		setLayout(gl);
		setBackground(Colors.getColor(5));
		add(new JLabel(s.getName()));
		add(new AuthorButton(s.getAuthor(),lp));
		add(new JLabel(""+ s.getDurationString()));
		if(s.getExplicit())
				this.add(new JLabel("Explicit"));
		else
			this.add(new JLabel());
		this.add(new SongButtons(s, panel), panel);
		
		int height = (int) this.getMinimumSize().getHeight();
		int width = (int) this.getMaximumSize().getWidth();
		this.setMaximumSize(new Dimension(width,height + 5));
	}

}
