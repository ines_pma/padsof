package graphical.rowsDisplays;

import java.util.Collection;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import app.App;
import app.media.Song;
import app.media.SongState;
import graphical.Colors;
import graphical.general.LoggedPanel;
import graphical.general.MyPanel;
/**
 * Displays multiple songs
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class MultipleSongDisplay extends JPanel implements MyPanel{
	private Collection<Song> songs;
	private LoggedPanel parent;
	
	/**
	 * @param songs songs to display
	 * @param parent	central panel
	 */
	public MultipleSongDisplay(Collection<Song> songs, LoggedPanel parent) {
		this.songs = songs;
		this.parent = parent;
		BoxLayout bl = new BoxLayout(this,BoxLayout.Y_AXIS);
		setLayout(bl);
		setBackground(Colors.getColor(4));
		if (songs == null)
				return;
		refresh();
		}

	/**
	 * updates the panel
	 */
	@Override
	public void refresh() {
		removeAll();
		int cont = 0;
		for(Song s: songs) {
			if (s.getSongState() == SongState.VALIDATED && (!(s.getExplicit()) || !(App.getAppInstance().getActualUser().isUnderage()))) {
				add(Box.createVerticalStrut(5));
				add(new SongRow(s,parent,this));
				add(Box.createVerticalStrut(5));
				cont ++;
				if(cont == 100)
					break;
			}
		}
		revalidate();
		repaint();
		
	}
}
