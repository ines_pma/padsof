package graphical.rowsDisplays;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import app.media.Album;
import graphical.Colors;
import graphical.authorPage.AuthorButton;
import graphical.buttonsMenus.AlbumButton;
import graphical.buttonsMenus.CompilationButtons;
import graphical.general.LoggedPanel;
import graphical.general.MyPanel;
/**
 * Row with album's information and buttons
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class AlbumRow extends JPanel {

	/**
	 * @param a album to shows it's information
	 * @param lp central panel
	 * @param panel panel which will be refreshed if there's any change
	 */
	public AlbumRow(Album a, LoggedPanel lp,MyPanel panel) {
		setLayout(new GridLayout(1,0));
		setBackground(Colors.getColor(5));
		
		AlbumButton album = new AlbumButton(lp,a);
		add(album);
		add(new AuthorButton(a.getAuthor(),lp));
		add(new JLabel(""+ a.getYear()));
		if(a.getExplicit())
				add(new JLabel("Explicit"));
		else
			add(new JLabel());
		add(new CompilationButtons(a,panel));
		
		int height = (int) this.getMinimumSize().getHeight();
		int width = (int) this.getMaximumSize().getWidth();
		this.setMaximumSize(new Dimension(width,height+5));		
	}

}
