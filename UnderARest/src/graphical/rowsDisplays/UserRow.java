package graphical.rowsDisplays;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import app.users.RegUser;
import graphical.Colors;
import graphical.authorPage.AuthorButton;
import graphical.general.LoggedPanel;
/**
 * Displays one user's information
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class UserRow extends JPanel {
	
	/**
	 * @param u user to show their information
	 * @param lp central panel
	 */
	public UserRow(RegUser u, LoggedPanel lp) {
		
		setLayout(new GridLayout(1,0));
		setBackground(Colors.getColor(5));
		AuthorButton a = new AuthorButton(u,lp);
		a.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.add(a);
		this.add(new JLabel(u.getUserName()));
		
		int height = (int) this.getMinimumSize().getHeight();
		int width = (int) this.getMaximumSize().getWidth();
		this.setMaximumSize(new Dimension(width,height+15));
	}
}
