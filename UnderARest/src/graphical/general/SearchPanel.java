package graphical.general;

import java.awt.Component;
import java.awt.Font;
import java.util.Collection;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import app.general.Search;
import app.media.Album;
import app.media.Song;
import app.users.RegUser;
import graphical.Colors;
import graphical.MainWindow;
import graphical.rowsDisplays.MultipleAlbumDisplay;
import graphical.rowsDisplays.MultipleSongDisplay;
import graphical.rowsDisplays.MultipleUserDisplay;

/**
 * Shows the results of a search
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class SearchPanel extends JPanel {
	
	/**
	 * @param text text we want to look for
	 * @param frame frame where the app is running 
	 * @param lp panel where the search will be displayed
	 */
	public SearchPanel(String text, MainWindow frame, LoggedPanel lp) {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBorder(new EmptyBorder(100,100,100, 100));
		setBackground(Colors.getColor(4));
		JLabel search = new JLabel("Search: " + text);
		search.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.add(search);
		
		String font = search.getFont().getName();
		
		JLabel username = new JLabel("Users: ");
		username.setFont(new Font(font, Font.BOLD , 30));
		username.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.add(username);
		
		Collection<RegUser> users = Search.searchUser(text);
		if(users.isEmpty())
			this.add(new BlankSpace());
		else {
			MultipleUserDisplay mud = new MultipleUserDisplay(users, lp); 
			this.add(mud);
			mud.setAlignmentX(Component.LEFT_ALIGNMENT);
		}
		
		JLabel album = new JLabel("Album: ");
		album.setFont(new Font(font, Font.BOLD , 30));
		album.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.add(album);
		Collection<Album> albums = Search.searchAlbum(text);
		if (albums.isEmpty())
			this.add(new BlankSpace());
		else {	
			MultipleAlbumDisplay mad = new MultipleAlbumDisplay(albums, lp);
			this.add(mad);
			mad.setAlignmentX(Component.LEFT_ALIGNMENT);
		}
		
		JLabel song = new JLabel("Songs: ");
		song.setFont(new Font(font, Font.BOLD , 30));
		song.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.add(song);
		Collection<Song> songs = Search.searchSong(text);
		if (songs.isEmpty())
			this.add(new BlankSpace());
		else {
			MultipleSongDisplay msd = new MultipleSongDisplay(songs,lp);
			this.add(msd);
			msd.setAlignmentX(Component.LEFT_ALIGNMENT);
		}
	}
	
}
