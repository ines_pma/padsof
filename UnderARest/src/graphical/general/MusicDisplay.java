package graphical.general;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import app.App;
import app.media.Music;
import app.media.Song;
import app.media.SongState;
import graphical.Colors;
import graphical.buttonsMenus.PlayButton;
import graphical.buttonsMenus.StopButton;

/**
 * Shows the music which is playing now
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class MusicDisplay extends JPanel{
	private Music m;
	private Song s;
	private JPanel north = new JPanel();
	private JPanel center = new JPanel();
	private JPanel south = new JPanel();
	private PlayButton play;
	private StopButton stop;
	private JLabel st ;
	private JLabel au ;
	private JLabel title ;
	private JLabel author;
	private JLabel[] songLabels = { new JLabel(), new JLabel(), new JLabel(), new JLabel(), new JLabel()};
	
	public MusicDisplay() {
		setLayout(new BorderLayout());
		setBackground(Colors.getColor(3));
		Collection<Song> songs2 = App.getSongs().values();
		for (Song s1: songs2) {
			if(s1.getSongState().equals(SongState.VALIDATED)){
				App.getAppInstance().setCurrentSong(s1);
				App.getAppInstance().setCurrentMusic(s1);
				break;
			}
		}
		
		north.setLayout(new BoxLayout(north, BoxLayout.Y_AXIS));
		north.setBorder(BorderFactory.createEmptyBorder(130, 30, 20, 100));
		north.setBackground(Colors.getColor(4));
		this.add(north,BorderLayout.NORTH);
		
		center.setLayout(new BoxLayout(center, BoxLayout.Y_AXIS));
		center.setBorder(BorderFactory.createEmptyBorder(20, 30, 20, 100));
		center.setBackground(Colors.getColor(4));
		this.add(center, BorderLayout.CENTER);
		
		
		south.setBorder(BorderFactory.createEmptyBorder(100, 100, 100, 100));
		south.setBackground(Colors.getColor(4));
		m = App.getAppInstance().getCurrentMusic();
		play = new PlayButton(m);
		stop = new StopButton(m);
		south.add(play);
		south.add(stop);
		
		this.add(south,BorderLayout.SOUTH);		
		s = App.getAppInstance().getCurrentSong();
		if(s != null) {
			st = new JLabel(s.getName());
			north.add(st);
			au = new JLabel("By: " + s.getAuthor().getAuthorName());
			north.add(au);
		}
		
		if (m != null) {
			title = new JLabel(m.getName());
			center.add(title);
			author = new JLabel("By: " + m.getAuthor().getAuthorName());
			center.add(author);
			Set<Song> songs = m.getSongs();
			int cont = 0;
			for (Song song: songs) {
				cont++;
				center.add(songLabels[cont]);
				songLabels[cont].setText(song.getName());
				if(cont == 5)
					break;
			}
		}
		
		
		//it keeps checking which song is actually playing
	    javax.swing.Timer timer = new javax.swing.Timer(100, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				s = App.getAppInstance().getCurrentSong();
				m = App.getAppInstance().getCurrentMusic();
				
				st.setText(s.getName());
				au.setText("By: " + s.getAuthor().getAuthorName());
				
				title.setText(m.getName());
				author.setText("By: " + m.getAuthor().getAuthorName());
				
				Set<Song> songs = m.getSongs();
				int cont = 0;
				for (Song song: songs) {
					cont++;
					songLabels[cont].setText(song.getName());
					if(cont == 5)
						break;
				}
				for(int i = 4; i > cont; i--) {
					songLabels[i].setText("");
				}
				
				south.remove(play);
				PlayButton p = new PlayButton(m);
				south.add(p,BorderLayout.SOUTH);
				play = p;
				
				south.remove(stop);
				StopButton s = new StopButton(m);
				south.add(s,BorderLayout.SOUTH);
				stop = s;
				
				MusicDisplay.this.repaint();
				MusicDisplay.this.revalidate();
			}
	    });
	    timer.setRepeats(true);
	    timer.setDelay(100);
	    timer.start();
	}

}
