package graphical.general;

/**
 * Interface for panels which can be refreshed
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
public interface MyPanel {
	public void refresh();
}
