package graphical.general;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JPanel;

import graphical.Colors;
/**
 * Blank space to add to different panels
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class BlankSpace extends JPanel {
	public BlankSpace() {
		setMaximumSize(new Dimension((int) this.getMaximumSize().getWidth(),100));
		setMinimumSize(this.getMaximumSize());
		setAlignmentX(Component.LEFT_ALIGNMENT);
		setBackground(Colors.getColor(4));
	}
}
