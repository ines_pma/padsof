package graphical.general;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

import app.App;
import app.users.RegUser;
import app.users.UserState;
import es.uam.eps.padsof.telecard.OrderRejectedException;
import graphical.Colors;
/**
 * Panel to upgrade your account
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class UpgradePanel extends JPanel implements MyPanel {
	private ImageIcon img = null;
	private JLabel days = new JLabel();
	
	public UpgradePanel() {
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		setBorder(new EmptyBorder(100,100,100, 100));
		setBackground(Colors.getColor(4));
		
		try {
			img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
		} catch (IOException e1) {}
		
		JLabel title = new JLabel("Upgrade your account"); 
		title.setFont(new Font(title.getFont().getName(), Font.BOLD , 30));
		title.setAlignmentX(Component.LEFT_ALIGNMENT);
		add(title);
		
		String[] labels = {" ", "Anonymous", "Regular", "Premium"};
		String[][] data = { {"Upload Songs", "0", "Unlimited", "Unlimited"},
							{"Playbacks", "20 songs per session", "1000 per month", "Unlimited"},
							{"Report", "No", "Yes","Yes"},
							{"Explicit Music", "No", "Only adults", "Only adults"},
							{"Price", "Free", "Free", App.getSettings().getPremiumPrice() + "$ per month"}};
		
		JTable table = new JTable(data,labels);
		table.setAlignmentX(Component.LEFT_ALIGNMENT);
		table.setEnabled(false);
		//table.setBackground(Colors.getColor(4));
		JScrollPane sp= new JScrollPane(table);
		sp.setBackground(Colors.getColor(4));
		add(sp);
		sp.setBorder(BorderFactory.createMatteBorder(130, 0, 100, 0,Colors.getColor(4)));
		sp.setMaximumSize(new Dimension( (int)sp.getMaximumSize().getWidth(), 330));
		JButton upgrade = new JButton("Upgrade");
		upgrade.setBackground(Colors.getColor(3));
		upgrade.setForeground(Colors.getColor(4));
		upgrade.setBorder(new MatteBorder(5,5,5,5,Colors.getColor(3)));
		add(upgrade);
		add(new BlankSpace());
		if(App.getAppInstance().getActualUser().getUserState() == UserState.PREMIUM )
			days.setText("Your account will expire: " + ((RegUser)App.getAppInstance().getActualUser()).getExpDate());
		add(days);
	
		upgrade.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String card = JOptionPane.showInputDialog("Please write your card number.");
				if (card != null) {
					RegUser u = (RegUser) (App.getAppInstance().getActualUser());
					try {
						u.upgradePaying(card);
						JOptionPane.showMessageDialog(null,"Your account has been upgraded","Upgrade",JOptionPane.PLAIN_MESSAGE,img);
						refresh();
					} catch (OrderRejectedException e) {
						JOptionPane.showMessageDialog(null,"Incorrect card number.","Error",JOptionPane.PLAIN_MESSAGE,img);	
					}
				}
			}
		});
		
	}

	/**
	 * Updates the number of days left until your account expires;
	 */
	@Override
	public void refresh() {
		if(App.getAppInstance().getActualUser().getUserState() == UserState.PREMIUM ) {
			days.setText("Your account will expire: " + ((RegUser)App.getAppInstance().getActualUser()).getExpDate());
			revalidate();
			repaint();
		}
	}
}
