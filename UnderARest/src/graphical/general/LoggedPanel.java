package graphical.general;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import graphical.Colors;
import graphical.MainWindow;
import graphical.upperBar.UpperBar;

/**
 * Panel which appears once you've logged in.
 * It has three main parts:
 * - upper bar which remains the same
 * - center which will change as you move through the app
 * - music display which will show what you are listening to
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class LoggedPanel extends JPanel{
	private JPanel center;
	private JPanel previous;
	@SuppressWarnings("unused")
	private MainWindow frame;
	private MusicDisplay md;
	private JScrollPane scroll;
	
	/**
	 * @param frame frame where the app is running
	 */
	public LoggedPanel(MainWindow frame) {
		BorderLayout layout = new BorderLayout();
		setLayout(layout);
		setBorder(BorderFactory.createEmptyBorder());
		UpperBar ub = new UpperBar(this,frame);
		this.add(ub,BorderLayout.NORTH);
		
		center = new Home(this);
		previous = center;
		scroll = new JScrollPane(center);
		add(scroll,BorderLayout.CENTER);
		scroll.setWheelScrollingEnabled(true);
		scroll.setBorder(new EmptyBorder(0,0,0,0));
		
		JScrollBar h = scroll.getHorizontalScrollBar();
		h.setPreferredSize(new Dimension(10, 10));
		h.setUnitIncrement(100);
		h.setBorder(new EmptyBorder(0,0,0,0));
        for(Component c : h.getComponents()) {
        	c.setBackground(Colors.getColor(3));
    		c.setForeground(Colors.getColor(3));
        }
        
        JScrollBar v = scroll.getVerticalScrollBar();
        v.setPreferredSize(new Dimension(10, 10));
		v.setUnitIncrement(100);
		v.setBorder(new EmptyBorder(0,0,0,0));
        for(Component c : v.getComponents()) {
        	c.setBackground(Colors.getColor(3));
    		c.setForeground(Colors.getColor(3));
        }
		
		md = new MusicDisplay();
		md.setMinimumSize(new Dimension(300,1000));
		md.setMaximumSize(new Dimension(300,1000));
		md.setPreferredSize(new Dimension(300,1000));
		this.add(md,BorderLayout.EAST);
		md.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
	}
	
	
	
	/**
	 * @param panel changes the center panel
	 */
	public void changeCenter(JPanel panel) {
		previous = center;
		center = panel;
		scroll.setViewportView(panel);
	}

	public void previous() {
		changeCenter(previous);
	}
	
}
