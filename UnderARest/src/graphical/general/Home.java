package graphical.general;

import java.awt.Component;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import app.App;
import graphical.Colors;
import graphical.rowsDisplays.MultipleSongDisplay;

/**
 * Home page
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
@SuppressWarnings("serial")
public class Home extends JPanel {
	/**
	 * Shows several songs
	 * @param lp panel which will change once logged in
	 */
	public Home(LoggedPanel lp) {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBorder(new EmptyBorder(100,100,50,50));
		setBackground(Colors.getColor(4));
		
		JLabel title = new JLabel("Home"); 
		title.setFont(new Font(title.getFont().getName(), Font.BOLD , 30));
		title.setAlignmentX(Component.LEFT_ALIGNMENT);
		this.add(title);
		
		MultipleSongDisplay table = new MultipleSongDisplay(App.getSongs().values(),lp);
		this.add(table);
		table.setAlignmentX(Component.LEFT_ALIGNMENT);
	
	}
}
