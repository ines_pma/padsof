package graphical.authentication;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;

import app.general.Authentication;
import app.general.exceptions.BlockedUserException;
import app.general.exceptions.InvalidCredentialsException;
import graphical.Colors;
import graphical.MainWindow;
import graphical.general.LoggedPanel;

/**
 * Panel to proceed with the login. 
 * It will validate the data. It has three buttons to login, 
 * enter as a guest and register which will take you to the corresponding page.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

@SuppressWarnings("serial")
public class LoginForm extends JPanel {
	private JLabel userLabel = new JLabel("Username:");
	private JLabel passwordLabel = new JLabel("Password");
	private JTextField userField = new JTextField(20);
	private JTextField passwordField = new JPasswordField(20);
	private JButton loginButton = new JButton("Log in");
	private JButton signUpButton = new JButton("Sign up");
	private JButton enterGuestButton = new JButton("Enter as a guest");
	private MainWindow frame;
	private ImageIcon img = null;
	
	public LoginForm(MainWindow frame) {
		this.frame = frame;
		setBackground(Colors.getColor(4));
		this.setLayout(new GridLayout(0,1));
		this.add(userLabel); 
		this.add(userField);
		this.add(passwordLabel);
		this.add(passwordField);
		passwordField.addActionListener(new ControllerLogin());
		this.add(new JLabel(""));
		JPanel pb = new JPanel();
		pb.setLayout(new GridLayout(1,2));
		JPanel p1 = new JPanel();
		p1.setBackground(Colors.getColor(4));
		p1.add(loginButton);
		pb.add(p1);
		p1 = new JPanel();
		p1.setBackground(Colors.getColor(4));
		p1.add(signUpButton);
		pb.add(p1);
		p1 = new JPanel();
		p1.setBackground(Colors.getColor(4));
		p1.add(enterGuestButton);
		pb.add(p1);
		this.add(pb);
		
		try {
			img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
		} catch (IOException e1) {}
		
		Dimension d = enterGuestButton.getMinimumSize();
		
		loginButton.addActionListener(new ControllerLogin());
		loginButton.setPreferredSize(d);
		loginButton.setBackground(Colors.getColor(3));
		loginButton.setForeground(Colors.getColor(4));
		loginButton.setBorder( new MatteBorder(5, 5, 5, 5, Colors.getColor(3)));
	
		signUpButton.addActionListener(new ControllerSignUp());
		signUpButton.setPreferredSize(d);
		signUpButton.setBackground(Colors.getColor(3));
		signUpButton.setForeground(Colors.getColor(4));
		signUpButton.setBorder( new MatteBorder(5, 5, 5, 5, Colors.getColor(3)));
		
		enterGuestButton.addActionListener(new ControllerEnterGuest());
		enterGuestButton.setPreferredSize(d);
		enterGuestButton.setBackground(Colors.getColor(3));
		enterGuestButton.setForeground(Colors.getColor(4));
		enterGuestButton.setBorder( new MatteBorder(5, 5, 5, 5, Colors.getColor(3)));
		
	}
		
	public class ControllerLogin implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				Authentication.login(userField.getText().trim(),passwordField.getText().trim());
				frame.createChangePanel(new LoggedPanel(frame));
				
			} catch (InvalidCredentialsException e) {
				JOptionPane.showMessageDialog(null, "Wrong username or password","Wrong credentials",JOptionPane.PLAIN_MESSAGE,img);
                // reset username and password
                userField.setText("");
                passwordField.setText("");
			} catch (BlockedUserException e) {
				JOptionPane.showMessageDialog(null, "You are blocked","Blocked",JOptionPane.PLAIN_MESSAGE,img);
                userField.setText("");
                passwordField.setText("");
			}
		}
		
	}
		
	public class ControllerSignUp implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			frame.createChangePanel(new Register(frame));
		}
	}
	
	public class ControllerEnterGuest implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			Authentication.enterAsGuest();
			frame.createChangePanel(new LoggedPanel(frame));
			
		}
	}
		
}
