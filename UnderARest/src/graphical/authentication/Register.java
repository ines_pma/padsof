package graphical.authentication;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import app.App;
import app.exceptions.NameAlreadyExistsException;
import app.general.Authentication;
import app.users.exceptions.NotBornException;
import graphical.Colors;
import graphical.MainWindow;

/**
 * The Register JPanel is the one showed when trying to sign up.
 * It contains fields for a username, authorname, password and 
 * birthdate. If the data introduced isn't valid, a message with
 * the error appears. If it is valid, the registration occurs 
 * immediately.
 * 
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 * 
 */

@SuppressWarnings("serial")
public class Register extends JPanel {

	private JLabel username;
	private JTextField usertext;
	private JLabel authorname;
	private JTextField authortext;
	private JLabel password;
	private JTextField pw;
	private JLabel pwconfirm;
	private JTextField pwconf;
	private JLabel birth;
	private UtilDateModel model;
	private JDatePanelImpl datePanel;
	private JDatePickerImpl datePicker;
	private JCheckBox conditions;
	private JButton register;
	private ImageIcon logo;
	private JLabel logoLabel;
	private JLabel title;
	private MainWindow frame;
	private ImageIcon img = null;

	public Register(MainWindow frame) {
		username = new JLabel("Username");
		usertext = new JTextField(20);
		password = new JLabel("Password");
		pw = new JPasswordField(20);
		authorname = new JLabel("Authorname");
		authortext = new JTextField(20);
		pwconfirm = new JLabel("Password confirmation");
		pwconf = new JPasswordField(20);
		birth = new JLabel("Birth date");
		Properties p = new Properties();
		p.put("text.today", "today");
		p.put("text.month", "month");
		p.put("text.year", "year");
		model = new UtilDateModel();
		datePanel = new JDatePanelImpl(model, p);
		datePicker = new JDatePickerImpl(datePanel, new LabelDateFormatter());
		conditions = new JCheckBox("Accept the conditions");
		register = new JButton("Create your account");
		
		this.frame = frame;
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBackground(Colors.getColor(4));
		try {
			logo = new ImageIcon(ImageIO.read(new File("img/logo.png")));
			logoLabel = new JLabel(logo);
		} catch (IOException e) {}
		try {
			img  = new ImageIcon(ImageIO.read(new File("img/mini_logo.png")));
		} catch (IOException e1) {}
		this.add(Box.createRigidArea(new Dimension(100, 40)));
		title = new JLabel ("Under a rest");
		title.setFont(new Font(title.getFont().getName(), Font.BOLD , 50));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(title);
		this.add(Box.createRigidArea(new Dimension(100, 20)));
		this.add(logoLabel);
		this.add(Box.createRigidArea(new Dimension(100, 20)));
		logoLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

		JPanel pn = new JPanel();
		pn.setBackground(Colors.getColor(4));
		pn.setLayout(new GridLayout(0,1));
		pn.add(username);
		pn.add(usertext);
		pn.add(authorname);
		pn.add(authortext);
		pn.add(password);
		pn.add(pw);
		pn.add(pwconfirm);
		pn.add(pwconf);
		pn.add(birth);
		pn.add(datePicker);
		pn.add(conditions);
		JPanel p1 = new JPanel();
		p1.setBackground(Colors.getColor(4));
		p1.add(register);
		pn.add(p1);
		this.add(pn);
		pn.setAlignmentX(Component.CENTER_ALIGNMENT);
		int height = (int) pn.getMinimumSize().getHeight();
		pn.setMaximumSize(new Dimension(600,height));
		
		this.add(pn);
		datePicker.setBackground(Colors.getColor(4));
		conditions.setBackground(Colors.getColor(4));
		
		register.addActionListener(new ControllerRegister());
		register.setMaximumSize(register.getMinimumSize());
		register.setBackground(Colors.getColor(3));
		register.setForeground(Colors.getColor(4));
		register.setBorder( new MatteBorder(5, 5, 5, 5, Colors.getColor(3)));
	}

	/**
	 * ControllerRegister is an ActionListener which checks if the data is valid for 
	 * registration when the Register button is pressed.
	 * 
	 * @author Ines Pastor
	 * @author Carmen Diez
	 * @author Ignacio Echave-Sustaeta
	 *
	 * 
	 */
	
	private class ControllerRegister implements ActionListener {

		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// Checks the basic conditions for the data to be valid.
			
			if(usertext.getText().trim().equals("") || authortext.getText().trim().equals("") || pw.getText().trim().equals("") || datePicker.getJFormattedTextField().equals("")) {
				JOptionPane.showMessageDialog(null, "Fill all the blank spaces.","Error",JOptionPane.PLAIN_MESSAGE,img);
			} else if(!conditions.isSelected()) {
				JOptionPane.showMessageDialog(null, "You have to accept the conditions.","Error",JOptionPane.PLAIN_MESSAGE,img);
			} else if(!pw.getText().trim().equals(pwconf.getText().trim())) {
				JOptionPane.showMessageDialog(null, "The passwords don't match.","Error",JOptionPane.PLAIN_MESSAGE,img);
				cleanFields();
			} else {
				LocalDate date = LocalDate.parse(datePicker.getJFormattedTextField().getText(), DateTimeFormatter.ofPattern("d/MM/yyyy"));

				// Tries to register with the given data and, in case the data is invalid, informs about the mistake.
				
				try {
					Authentication.register(usertext.getText().trim(), pw.getText().trim(), authortext.getText().trim(), date);
					if(App.getAppInstance().getActualUser() != null)
						Authentication.logout();
					JOptionPane.showMessageDialog(null,"User created succesfully.","Error",JOptionPane.PLAIN_MESSAGE,img);
					frame.createChangePanel(new Login(frame));
				} catch(NameAlreadyExistsException e){
					cleanFields();
					JOptionPane.showMessageDialog(null, "This username in already in use.","Error",JOptionPane.PLAIN_MESSAGE,img);
				} catch(NotBornException n) {
					JOptionPane.showMessageDialog(null, "Not a valid birth date.","Error",JOptionPane.PLAIN_MESSAGE,img);
					cleanFields();
				}
			}			
		}
		
		/**
		 * Cleans the text fields when the data can't be confirmed.
		 */

		private void cleanFields() {
			usertext.setText("");
			authortext.setText("");
			pw.setText("");
			pwconf.setText("");
			datePicker.getJFormattedTextField().setText("");
		}
	}
}
