package graphical.authentication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JFormattedTextField.AbstractFormatter;

/**
 * LabelDateFormatter is an implementation of a Formatter which we have used
 * for our JDatePicker. It uses the patern dd/MM/yyyy for the dates.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 * 
 */

@SuppressWarnings("serial")
public class LabelDateFormatter  extends AbstractFormatter {
    private String datePatern = "dd/MM/yyyy";

    private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePatern);

    @Override
    public Object stringToValue(String text) throws ParseException {
        return dateFormatter.parseObject(text);
    }

    @Override
    public String valueToString(Object value) throws ParseException {
        if (value != null) {
            Calendar cal = (Calendar) value;
            return dateFormatter.format(cal.getTime());
        }

        return "";
    }
}