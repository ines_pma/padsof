package graphical.authentication;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import graphical.Colors;
import graphical.MainWindow;

/**
 * Panel to login. Contains a logginForm which will take and validate all the input
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

@SuppressWarnings("serial")
public class Login extends JPanel{
	private JLabel logoLabel;
	private JLabel title;
	private BufferedImage logo;
	@SuppressWarnings("unused")
	private MainWindow frame;
	
	
	public Login(MainWindow frame) {
		this.frame = frame;
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBackground(Colors.getColor(4));
		try {
			logo = ImageIO.read(new File("img/logo.png"));
			logoLabel = new JLabel(new ImageIcon(logo));
		} catch (IOException e) {}
		
		this.add(Box.createRigidArea(new Dimension(100, 40)));
		title = new JLabel ("Under a rest");
		title.setFont(new Font(title.getFont().getName(), Font.BOLD , 50));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(title);
		add(Box.createRigidArea(new Dimension(100, 20)));
		add(logoLabel);
		add(Box.createRigidArea(new Dimension(100, 20)));
		logoLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		LoginForm lf = new LoginForm(frame);
		int height = (int) lf.getMinimumSize().getHeight();
		lf.setMaximumSize(new Dimension(600,height));
		this.add(lf);
		lf.setAlignmentX(Component.CENTER_ALIGNMENT);	
	}



	

}
