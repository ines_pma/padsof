package app.media;

import java.util.HashSet;
import java.util.Set;

import app.App;
import app.media.exceptions.DuplicatedSongException;
import app.users.RegUser;

/**
 * Albums are a type of compilation which have songs and a publication year
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 */
public class Album extends Compilation {
	private static final long serialVersionUID = 1L;
	private int year;

	/**
	 * It will create an empty album with the name and year specified.
	 * 
	 * @param name
	 * @param year
	 */
	public Album(String name, int year) {
		super(name);
		this.year = year;
		duration = 0.0;
	}

	/**
	 * It will create an album with the name and year specified which contains that
	 * list of songs
	 * 
	 * @param name
	 * @param year
	 * @param songs Set of songs which the album will contain
	 * @throws DuplicatedSongException
	 */
	public Album(String name, int year, Set<Song> songs) throws DuplicatedSongException {
		this(name, year);
		for (Song s : songs)
			addSong(s);
	}

	/**
	 * @return year
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @see app.media.Music#getAuthor()
	 */
	@Override
	public RegUser getAuthor() {
		return (music.iterator().next()).getAuthor();
	}

	/**
	 * It adds a song to the album
	 * 
	 * @param s
	 * @throws DuplicatedSongException if it already contains that song
	 */
	public void addSong(Song s) throws DuplicatedSongException {
		containedMusic(s);
		music.add(s);
		s.contained.add(this);
		duration += s.getDuration();
		if (s.getExplicit())
			setExplicit(true);
	}

	/**
	 * @see app.media.Compilation#getSongs()
	 */
	@Override
	public Set<Song> getSongs() {
		Set<Song> songs = new HashSet<>();
		for (Music m : music)
			songs.add((Song) m);
		return songs;
	}

	/**
	 * @see app.media.Music#delete()
	 */
	@Override
	public void delete() {
		Set<Compilation> aux = new HashSet<>();
		aux.addAll(contained);
		for (Compilation c : aux) {
			c.deleteMusic(this);
		}
		Set<Music> aux2 = new HashSet<>();
		aux2.addAll(music);
		for (Music m : aux2) {
			m.deleteContained(this);
		}
		App.deleteAlbum(this);
	}


}
