package app.media;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import app.App;
import app.media.exceptions.DeleteException;
import app.media.exceptions.DuplicatedSongException;
import app.media.exceptions.FileTooLongException;
import app.users.RegUser;
import pads.musicPlayer.Mp3Player;

/**
 * Song class has the song's duration, the file it refers to, its author and
 * state
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

public class Song extends Music implements Serializable {

	private static final long serialVersionUID = 1L;

	private String file;
	private LocalDate timeValid;
	private RegUser author;
	private SongState songState;

	/**
	 * The name must have been validated before calling this function
	 * 
	 * @param name
	 * @param file
	 * @param author
	 * @throws FileNotFoundException
	 */
	public Song(String name, String file, RegUser author) throws FileNotFoundException, FileTooLongException {
		super(name);
		this.timeValid = LocalDate.now();
		this.duration = Mp3Player.getDuration(file);
		if (duration > 30 * 60)
			throw new FileTooLongException();
		this.file = file;
		this.setPlaying(false);
		this.author = author;
		songState = SongState.PENDING_TO_VALIDATE;
	}

	@Override
	public String toString() {
		return name;
	}

	/**
	 * @param time
	 */
	public void setTimeValid(LocalDate time) {
		this.timeValid = time;
	}

	/**
	 * @return SongState
	 */
	public SongState getSongState() {
		return songState;
	}

	/**
	 * @return timeValid
	 */
	public LocalDate getTimeValid() {
		return timeValid;
	}

	/**
	 * @param songState
	 */
	public void setSongState(SongState songState) {
		this.songState = songState;
	}

	/**
	 * @see app.media.Music#getAuthor()
	 */
	@Override
	public RegUser getAuthor() {
		return author;
	}

	/**
	 * @return file path
	 */
	public String getFile() {
		return file;
	}

	/**
	 * Changes user data. Must validate the data before calling this method
	 * 
	 * @param name
	 * @param file
	 * @throws FileTooLongException 
	 * @throws IOException 
	 */
	public void change(String name, String file) throws FileTooLongException, IOException {
		this.duration = Mp3Player.getDuration(file);
		if (duration > 30 * 60)
			throw new FileTooLongException();
		this.file = "mp3/" + name + this.getAuthor().getUserName() + ".mp3";
		this.copyFileTo(file, this.file);
		this.name = name;
		songState = SongState.PENDING_TO_VALIDATE;
	}

	private void copyFileTo(String fromPath, String toPath) throws IOException {
		Path from = Paths.get((new File(fromPath)).getAbsolutePath());
		Path to = Paths.get((new File(toPath)).getAbsolutePath());
		Files.copy(from,to, StandardCopyOption.REPLACE_EXISTING);
	}

	/**
	 * If the songs data hasn't been changed, the song is deleted
	 * 
	 * @throws DuplicatedSongException
	 */
	public void checkDateValidation() throws DeleteException {
		LocalDate act = LocalDate.now().minusDays(3);
		if (songState == SongState.WITH_ERRORS) {
			if (this.timeValid.isBefore(act)) {
				throw new DeleteException();
			}
		}
	}

	/**
	 * @see app.media.Music#delete()
	 */
	@Override
	public void delete() {
		Set<Compilation> aux = new HashSet<>();
		aux.addAll(contained);
		for (Compilation c : aux) {
			c.deleteMusic(this);
		}
		App.deleteSong(this);
	}

	/**
	 * @throws DuplicatedSongException
	 * @see app.media.Music#containsMusic(app.media.Music)
	 */
	@Override
	public void containsMusic(Music m) throws DuplicatedSongException {
		if (this == m)
			throw new DuplicatedSongException(this.getName());
	}

	/**
	 * @see app.media.Music#getSongs()
	 */
	@Override
	public Set<Song> getSongs() {
		Set<Song> s = new HashSet<Song>();
		s.add(this);
		return s;
	}

	/**
	 * Function used when loading the App, to retrieve the real strong reference to
	 * the author instance.
	 */
	public void loadAuthor(RegUser r) {
		this.author = r;
	}
	
}
