package app.media;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.WeakHashMap;

import app.App;
import app.media.exceptions.DuplicatedSongException;
import app.users.RegUser;
import pads.musicPlayer.exceptions.Mp3PlayerException;

/**
 * Music is an abstract class with a name, a set of compiltions where they are
 * contained their playing duration and if they are explicit or not.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
public abstract class Music implements Serializable {

	private static final long serialVersionUID = 1L;
	protected long id;
	private static long total = 0;

	protected String name;
	protected transient Set<Compilation> contained = Collections.newSetFromMap(new WeakHashMap<>());
	protected Set<Long> containedid = new HashSet<Long>();
	private Boolean playing;
	protected double duration;
	protected Boolean explicit;

	/**
	 * Creates music with a given name
	 * 
	 * @param name
	 */
	public Music(String name) {
		this.name = name;
		setPlaying(false);
		this.id = total;
		explicit = false;
		total++;
		App.setTotalMusic(total);
	}

	/**
	 * @return song name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return true if its explicit
	 */
	public Boolean getExplicit() {
		return explicit;
	}

	/**
	 * @param explicit
	 */
	public void setExplicit(Boolean explicit) {
		this.explicit = explicit;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return playing
	 */
	public Boolean getPlaying() {
		return playing;
	}

	/**
	 * @param playing
	 */
	public void setPlaying(Boolean playing) {
		this.playing = playing;
	}

	public double getDuration() {
		return duration;
	}

	/**
	 * It will play the music
	 * 
	 * @throws Mp3PlayerException
	 * @throws FileNotFoundException
	 */
	public void play() throws FileNotFoundException, Mp3PlayerException {
		App.newThread(this);
	}

	/**
	 * It will stop the music playing
	 * 
	 * @throws InterruptedException
	 */
	public void stop() throws InterruptedException {
		if (playing) {
			App.threadStop();
			playing = false;
		}
	}

	/**
	 * @return Music's author
	 */
	public abstract RegUser getAuthor();

	/**
	 * @param m music you want to check if its contained
	 * @throws DuplicatedSongException if it already contains the song (or is the
	 *                                 same)
	 */
	public abstract void containsMusic(Music m) throws DuplicatedSongException;

	/**
	 * It will check if any of the compilations in which it is contained contains m
	 * 
	 * @param m
	 * @throws DuplicatedSongException if any of the compilations in which it is
	 *                                 contained contains m (or is the same)
	 */
	public void containedMusic(Music m) throws DuplicatedSongException {
		containsMusic(m);
		if (this.contained.isEmpty())
			return;
		for (Compilation c : contained) {
			c.containedMusic(m);
		}
	}

	/**
	 * It deletes the music from the compilation
	 * 
	 * @param c compilation you want to remove the music from
	 */
	public void deleteContained(Compilation c) {
		contained.remove(c);
	}

	/**
	 * Returns the id of the object.
	 * 
	 * @return long which is the id of the Music
	 */
	public long getId() {
		return id;
	}

	/**
	 * Returns the containedid
	 * 
	 * @return Set<Long> of ids contained.
	 */
	public Set<Long> getContainedId() {
		if (contained == null) {
			contained = Collections.newSetFromMap(new WeakHashMap<>());
		} else {
			for (Compilation c : contained) {
				containedid.add(Long.valueOf(c.getId()));
			}
		}

		return containedid;
	}

	/**
	 * Changes the total of music created.
	 * 
	 * @param newtotal new total of music
	 */
	public static void updateTotal(long newtotal) {
		total = newtotal;
	}

	/**
	 * Adds a compilation to be contained in (when loading the app)
	 * 
	 * @param c Compilation to be loaded
	 */
	public void loadContained(Compilation c) {
		contained.add(c);
		return;
	}

	/**
	 * @return Set of songs it contains
	 */
	public abstract Set<Song> getSongs();

	/**
	 * Deletes the music
	 */
	public abstract void delete();
	
	public String getDurationString() {
		int min = (int) (duration/60);
		int seg = (int)duration % 60;
		return(""+ min +":"+ seg );
	}
}
