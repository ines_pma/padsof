package app.media;

/**
 * Enumeration song class
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 */
public enum SongState {
	PENDING_TO_VALIDATE, WITH_ERRORS, VALIDATED, REPORTED
}
