package app.media;

import java.util.HashSet;
import java.util.Set;

import app.media.exceptions.DuplicatedSongException;
import app.users.RegUser;

/**
 * Compilation of songs, albums and other playlists. This are private and only
 * registered users can use them
 *
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

public class Playlist extends Compilation {
	private RegUser author;

	private static final long serialVersionUID = 1L;

	/**
	 * @param name
	 * @param author
	 */
	public Playlist(String name, RegUser author) {
		super(name);
		this.author = author;
		duration = 0.0;
	}

	/**
	 * @see app.media.Music#delete()
	 */
	@Override
	public void delete() {
		Set<Compilation> aux = new HashSet<>();
		aux.addAll(contained);
		for (Compilation c : aux) {
			c.deleteMusic(this);
		}
		Set<Music> aux2 = new HashSet<>();
		aux2.addAll(music);
		for (Music m : aux2) {
			m.deleteContained(this);
		}
		author.removePlaylist(this);
	}

	/**
	 * @param m music you want to add
	 * @throws DuplicatedSongException
	 */
	public void addMusic(Music m) throws DuplicatedSongException {
		containsMusic(m);
		containedMusic(m);
		music.add(m);
		this.getMusicId().add(m.getId());
		m.contained.add(this);
		duration += m.getDuration();
		if (m.getExplicit())
			setExplicit(true);
	}

	/**
	 * @see app.media.Music#getAuthor()
	 *
	 */
	@Override
	public RegUser getAuthor() {
		return author;
	}

	/**
	 * @see app.media.Compilation#getSongs()
	 */
	@Override
	public Set<Song> getSongs() {
		Set<Song> songs = new HashSet<>();
		for (Music m : music) {
			songs.addAll(m.getSongs());
		}
		return songs;
	}

	/**
	 * Function used when loading the App, to retrieve the real strong reference to
	 * the author instance.
	 */
	public void loadAuthor(RegUser r) {
		this.author = r;
	}
	/*
	public boolean equals(Object o) {
		if(o == null) return false;
		if((o instanceof Playlist)) return false;
		Playlist s = (Playlist) o;
		return (name == s.getName() &&	author == s.getAuthor());
	}

	public int hashCode() {
		int result = 11;
		result = result*31 + name.hashCode();
		result = result*31 + author.hashCode();
		return result;
	}
*/
}
