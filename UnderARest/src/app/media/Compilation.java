package app.media;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.WeakHashMap;

import app.media.exceptions.DuplicatedSongException;

/**
 * Compilations are a type of music which can be made of other music.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
public abstract class Compilation extends Music {

	private static final long serialVersionUID = 1L;
	protected transient Set<Music> music = Collections.newSetFromMap(new WeakHashMap<>());
	protected Set<Long> musicid = new HashSet<Long>();

	/**
	 * Creates an empty compilation with the given name
	 * 
	 * @param name
	 */
	public Compilation(String name) {
		super(name);
	}

	/**
	 * @see app.media.Music#getSongs()
	 */
	@Override
	public abstract Set<Song> getSongs();

	/**
	 * @see app.media.Music#containsMusic(app.media.Music)
	 */
	@Override
	public void containsMusic(Music m) throws DuplicatedSongException {
		if (m == this)
			throw new DuplicatedSongException(m.getName());
		for (Music mus : music) {
			m.containsMusic(mus);
			mus.containsMusic(m);
		}
	}

	/**
	 * Deletes the music m from the compilation
	 * 
	 * @param m
	 */
	public void deleteMusic(Music m) {
		if (music.contains(m)) {
			music.remove(m);
			m.deleteContained(this);
			duration -= m.getDuration();
			if (music.isEmpty()) {
				delete();
			}
			if (getExplicit()) {
				setExplicit(false);
				for (Song s : getSongs()) {
					if (s.getExplicit())
						setExplicit(true);
				}
			}
		}
	}

	/**
	 * Adds Music directly (to user when loading the App)
	 * 
	 * @param m music to be added
	 */
	public void loadMusic(Music m) {
		music.add(m);
		return;
	}

	/**
	 * Used to store the data when closing the app
	 * 
	 * @return musicid
	 */
	public Set<Long> getMusicId() {
		if (music == null) {
			music = Collections.newSetFromMap(new WeakHashMap<>());
		} else {
			for (Music m : music) {
				musicid.add(Long.valueOf(m.getId()));
			}
		}

		return musicid;
	}
	
	
	public Set<Music> getMusic(){
		return music;
	}
}
