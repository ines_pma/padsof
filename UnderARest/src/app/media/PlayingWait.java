package app.media;

import java.io.FileNotFoundException;
import java.util.Set;

import app.App;
import pads.musicPlayer.Mp3Player;
import pads.musicPlayer.exceptions.Mp3PlayerException;

/**
 * Thread used to know which song is being played
 *
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
public class PlayingWait implements Runnable {
	private Music m;

	public PlayingWait(Music m) {
		this.m = m;
	}

	/**
	 * It will play all the songs in a compilation and check which one is playing
	 *
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		boolean run = true;
		m.setPlaying(true);
		App.getAppInstance().setCurrentMusic(m);
		Set<Song> songs = m.getSongs();
		for (Song s : songs) {
			if (App.getAppInstance().getActualUser() != s.getAuthor()) {
				run = App.getAppInstance().getActualUser().increasePlays();
				s.getAuthor().increaseListened();
			}
			if (run) {
				try {
					Mp3Player player = App.newPlayer();
					s.setPlaying(true);
					App.getAppInstance().setCurrentSong(s);
					player.add(s.getFile());
					player.play();
					Thread.sleep((int) (s.getDuration() * 1000));
					s.setPlaying(false);
				} catch (Mp3PlayerException | InterruptedException | FileNotFoundException e) {
					s.setPlaying(false);
					Mp3Player player = App.getPlayer();
					if (player != null)
						player.stop();
					break;
				}
			}
		}
		m.setPlaying(false);
	}

}
