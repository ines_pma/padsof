package app.media.exceptions;

@SuppressWarnings("serial")
public class FileTooLongException extends Exception {
	@Override
	public String toString() {
		return "This song exceeds the maximum duration. It must be under 30 min.";
	}

}
