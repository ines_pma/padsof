package app.media.exceptions;

@SuppressWarnings("serial")
public class DuplicatedSongException extends Exception {
	String music;

	public DuplicatedSongException(String music) {
		this.music = music;
	}

	@Override
	public String toString() {
		return "This media or part of it is already contained in " + music;
	}

}
