package app.media.exceptions;

@SuppressWarnings("serial")
public class EmptyException extends Exception {
	@Override
	public String toString() {
		return "This compilation is now empty and will be deleted";
	}

}
