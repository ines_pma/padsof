package app.users;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import app.App;
import app.media.Song;
import app.media.SongState;
import app.notification.AdminNotif;
import app.notification.NotValidatedNotif;
import app.notification.ReportNotif;
import app.notification.ReportedNotif;
import app.notification.UploadNotif;
import app.notification.UserNotif;
import app.notification.ValidatedNotif;
import app.notification.ValidationNotif;
import app.users.exceptions.ExistingNotificationException;

/**
 * The Admin is the user in charge of managing the application settings and
 * uploads.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 * 
 */

public class Admin extends UserAux {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3096026197476464609L;
	private Set<AdminNotif> notifications = new HashSet<>();

	private static Admin admin = null;

	private Admin(String name, String password) {
		super(false, 0, UserState.ADMIN, name, password);
	}

	public static Admin getAdminInstance() {
		if (admin == null)
			admin = new Admin("admin", "admin");
		return admin;
	}

	// SETTERS + GETTERS

	/**
	 * @return notifications
	 */
	public Set<AdminNotif> getNotifications() {
		return Collections.unmodifiableSet(this.notifications);
	}

	public Set<AdminNotif> getNotificationsTest() {
		return this.notifications;
	}

	/**
	 * @param notif
	 * @throws ExistingNotificationException
	 */
	public void addNotification(AdminNotif notif) throws ExistingNotificationException {
		for (AdminNotif n : this.notifications) {
			if (n.isReportNotif() == notif.isReportNotif())
				if (n.getSong() == notif.getSong())
					throw new ExistingNotificationException();
		}
		this.notifications.add(notif);
	}

	public void removeNotification(AdminNotif notif) {
		this.notifications.remove(notif);
	}

	// FUNCTIONALITIES

	/**
	 * Accepts a song and decides if it is explicit.
	 * 
	 * @param notif
	 * @param explicit
	 */
	public void accept(ValidationNotif notif, Boolean explicit) {
		Song s = notif.getSong();
		RegUser author = s.getAuthor();
		s.setSongState(SongState.VALIDATED);
		s.setExplicit(explicit);
		author.addNotification(new ValidatedNotif(s));
		for (RegUser follower : author.getFollowers())
			follower.addNotification(new UploadNotif(s));
		this.notifications.remove(notif);
	}

	/**
	 * Declines a song upload.
	 * 
	 * @param notif
	 */
	public void decline(ValidationNotif notif) {
		notif.getSong().setSongState(SongState.WITH_ERRORS);
		notif.getSong().setTimeValid(LocalDate.now());
		notif.getSong().getAuthor().addNotification(new NotValidatedNotif(notif.getSong()));
		this.notifications.remove(notif);
	}

	/**
	 * Accepts a report notification.
	 * 
	 * @param notif
	 */
	public void accept(ReportNotif notif) {
		notif.getSong().getAuthor().setUserState(UserState.BLOCKED_FOREVER);
		notif.getSong().getAuthor().updateExpDate();
		notif.getSong().delete();
		App.deleteSong(notif.getSong());
		System.gc();
		this.notifications.remove(notif);
	}

	/**
	 * Declines a report.
	 * 
	 * @param notif
	 */
	public void decline(ReportNotif notif) {
		UserNotif toRemove = null;
		notif.getReporter().setUserState(UserState.BLOCKED);
		notif.getReporter().updateExpDate();
		notif.getSong().setSongState(SongState.VALIDATED);

		Set<UserNotif> sn = notif.getSong().getAuthor().getNotificationsTest();
		for (UserNotif n : sn) {
			if (n.isReportedNotif() && ((ReportedNotif) n).getSong() == notif.getSong()) {
				toRemove = n;
				break;
			}
		}
		sn.remove(toRemove);

		this.notifications.remove(notif);
	}
}
