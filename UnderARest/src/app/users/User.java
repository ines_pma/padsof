package app.users;

import java.util.HashSet;
import java.util.Set;

import app.App;
import app.media.Music;
import app.media.Playlist;
import app.media.exceptions.DuplicatedSongException;
import app.users.exceptions.InvalidTitleException;

/**
 * The User class is used to represent the anonymous users, and has subclasses
 * which represents the rest of user types.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 * 
 */

public class User implements java.io.Serializable {

	private static final long serialVersionUID = 8479644414379512842L;
	protected int plays;
	protected boolean underage;
	protected UserState userState;

	public User() { // ANONYMOUS
		this.plays = 0;
		this.underage = true;
		this.userState = UserState.ANONYMOUS;
	}

	// OTHERS
	public User(boolean underage, int plays, UserState userState) {
		this.plays = plays;
		this.underage = underage;
		this.userState = userState;
	}

	public int getPlays() {
		return plays;
	}

	public void setPlays(int plays) {
		this.plays = plays;
	}

	public boolean isUnderage() {
		return underage;
	}

	public void setUnderage(boolean underage) {
		this.underage = underage;
	}

	public UserState getUserState() {
		return userState;
	}

	public void setUserState(UserState userState) {
		this.userState = userState;
		if (userState == UserState.REGISTERED) {
			this.plays = App.getSettings().getNumSongReg();
		}
	}

	/**
	 * Increases the plays of the user
	 * 
	 * @return boolean which says if the user is above the limit or not.
	 */

	public boolean increasePlays() {
		int limit;
		if (userState == UserState.ANONYMOUS)
			limit = App.getSettings().getNumSongAnon();
		else if (userState == UserState.REGISTERED)
			limit = App.getSettings().getNumSongReg();
		else
			limit = Integer.MAX_VALUE;
		if (plays < limit) {
			plays++;
			return true;
		}
		return false;
	}

	/**
	 * @return guest 
	 */
	public String getAuthorName() {
		return "Guest";
	}
	
	/**
	 * @return empty playlists' set
	 */
	public Set<Playlist> getPlaylists() {
		return new HashSet<Playlist>();
	}

	/**
	 * Anonymous users can't create playlists, so for them this function
	 * throws an exception.
	 * 
	 * @param name
	 * @param m
	 * @throws Exception
	 * @throws InvalidTitleException
	 * @throws DuplicatedSongException
	 */
	public void createPlaylist(String name, Music m) throws Exception, InvalidTitleException, DuplicatedSongException  {
		throw new Exception();
		
	}	
}

