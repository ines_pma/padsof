package app.users;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.WeakHashMap;

import app.App;
import app.general.Settings;
import app.media.Album;
import app.media.Music;
import app.media.Playlist;
import app.media.Song;
import app.media.SongState;
import app.media.exceptions.DeleteException;
import app.media.exceptions.DuplicatedSongException;
import app.media.exceptions.FileTooLongException;
import app.notification.PremiumExpiredNotif;
import app.notification.PremiumPromotedNotif;
import app.notification.ReportNotif;
import app.notification.ReportedNotif;
import app.notification.UserNotif;
import app.notification.ValidationNotif;
import app.users.exceptions.ExistingNotificationException;
import app.users.exceptions.InvalidTitleException;
import app.users.exceptions.NotBornException;
import app.users.exceptions.NotChangeableSongException;
import app.users.exceptions.NotYourMusicException;
import es.uam.eps.padsof.telecard.InvalidCardNumberException;
import es.uam.eps.padsof.telecard.OrderRejectedException;

/**
 * The RegUser class represents any registered user of the application.
 *
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 *
 */

public class RegUser extends UserAux implements java.io.Serializable {
	private static final long serialVersionUID = -8197824078496234469L;
	private static long total = 0;
	private long id;

	private int listened;
	private LocalDate birthdate;
	private LocalDate expDate;
	private String authorName;

	private Set<RegUser> following = new HashSet<>();
	private Set<RegUser> followers = new HashSet<>();

	private Set<UserNotif> notifications = new HashSet<>();

	private transient Set<Song> songs = Collections.newSetFromMap(new WeakHashMap<>());
	private Set<Long> songsid = new HashSet<Long>();
	private transient Set<Album> albums = Collections.newSetFromMap(new WeakHashMap<>());
	private Set<Long> albumsid = new HashSet<Long>();
	private Set<Playlist> playlists = new HashSet<>();

	public RegUser(String userName, String passwd, String authorName, LocalDate birthdate) throws NotBornException {
		super(true, 0, UserState.REGISTERED, userName, passwd);
		if (!birthdate.isBefore(LocalDate.now()))
			throw new NotBornException();
		this.listened = 0;
		this.expDate = LocalDate.now();
		this.birthdate = birthdate;
		adultifier();
		this.authorName = authorName;
		this.id = total;
		total++;
		App.setTotalUsers(total);
	}

	/**
	 * @return listened times this author has been listened to
	 */
	public int getListened() {
		return listened;
	}

	public void increaseListened() {
		update();
		listened++;
	}

	/**
	 * @return birthdate
	 */
	public LocalDate getBirthdate() {
		return birthdate;
	}

	/**
	 * @return expDate
	 */
	public LocalDate getExpDate() {
		return expDate;
	}

	/**
	 * @param expDate
	 */
	public void setExpDate(LocalDate expDate) {
		this.expDate = expDate;
	}

	/**
	 * @param listened
	 */
	public void setListened(int listened) {
		this.listened = listened;
	}

	/**
	 * @return the authorName
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * @return the notifications
	 */
	public Set<UserNotif> getNotifications() {
		return Collections.unmodifiableSet(this.notifications);
	}

	public Set<UserNotif> getNotificationsTest() {
		return this.notifications;
	}

	/**
	 * @param notif
	 */
	public void addNotification(UserNotif notif) {
		this.notifications.add(notif);
	}

	/**
	 * @return following
	 */
	public Set<RegUser> getFollowing() {
		return Collections.unmodifiableSet(this.following);
	}

	/**
	 * @return followers
	 */
	public Set<RegUser> getFollowers() {
		return Collections.unmodifiableSet(this.followers);
	}

	/**
	 * @param follower
	 */
	public void addFollower(RegUser follower) {
		this.followers.add(follower);
	}

	/**
	 * @param follower
	 */
	public void removeFollower(RegUser follower) {
		this.followers.remove(follower);
	}

	/**
	 * @return author's songs
	 */
	public Set<Song> getSongs() {
		//return Collections.unmodifiableSet(this.songs);
		return songs;
	}

	/**
	 * @return albums
	 */
	public Set<Album> getAlbums() {
		//return Collections.unmodifiableSet(this.albums);
		return albums;
	}

	/**
	 * @return playlists
	 */
	public Set<Playlist> getPlaylists() {
		return playlists;
	}

	// ATTRIBUTES MODIFIERS

	/**
	 * It checks if a user is underage and sets this attribute.
	 */
	public void adultifier() {
		int minAge = App.getSettings().getUnderage();
		if (birthdate.isBefore(LocalDate.now().minusYears(minAge)))
			setUnderage(false);
		else
			setUnderage(true);
	}

	/**
	 * Updates the expDate depending on the userState.
	 */
	public void updateExpDate() {
		UserState state = this.getUserState();
		if (state == UserState.BLOCKED_FOREVER) {
			this.expDate = LocalDate.MAX;
		} else {
			if (state == UserState.BLOCKED)
				this.expDate = LocalDate.now().plusDays(App.getSettings().getBlockedDays());
			else {
				if(expDate.isBefore(LocalDate.now()) && getUserState() == UserState.PREMIUM)
					this.expDate = LocalDate.now().plusDays(30);
				else
					expDate = expDate.plusDays(30);
				this.listened = 0;
				this.setPlays(0);
			}
		}
	}

	public void update() {
		UserState userState = this.getUserState();
		if (getExpDate().isBefore(LocalDate.now())) {
			if (userState == UserState.BLOCKED)
				setUserState(UserState.REGISTERED);
			else if (getListened() > Settings.getSettingsInstance().getRepUpgrade()) {
				setUserState(UserState.PREMIUM);
				this.addNotification(new PremiumPromotedNotif());
			} else {
				setUserState(UserState.REGISTERED);
				if (userState == UserState.PREMIUM)
					this.addNotification(new PremiumExpiredNotif());
			}
			updateExpDate();
		}
	}

	public void upgradePaying(String card) throws InvalidCardNumberException, OrderRejectedException {
		App.getBank().pay(card, "Premium", App.getSettings().getPremiumPrice());
		setUserState(UserState.PREMIUM);
		updateExpDate();
	}

	/* FUNCTIONALITIES */

	/**
	 * @param followed
	 */
	public void follow(RegUser followed) {
		following.add(followed);
		followed.addFollower(this);
	}

	/**
	 * Reports a song with a certain comment.
	 *
	 * @param s
	 * @param comment
	 * @throws ExistingNotificationException
	 */
	public void report(Song s, String comment) throws ExistingNotificationException {
		App.getAdmin().addNotification(new ReportNotif(s, this, comment));
		s.setSongState(SongState.REPORTED);
		s.getAuthor().addNotification(new ReportedNotif(s));
	}

	/**
	 * Creates a playlist from a given set of music.
	 *
	 * @param name
	 * @param m
	 * @throws InvalidTitleException
	 * @throws DuplicatedSongException
	 */
	public void createPlaylist(String name, Music m) throws InvalidTitleException, DuplicatedSongException {
		for (Playlist p : playlists)
			if ((p.getName()).equals(name))
				throw new InvalidTitleException();
		Playlist p = new Playlist(name, this);
		p.addMusic(m);
		playlists.add(p);
	}

	public void removePlaylist(Playlist p) {
		playlists.remove(p);
	}

	/**
	 * Creates an album from a given set of songs.
	 *
	 * @param name
	 * @param year
	 * @param songs
	 * @throws InvalidTitleException
	 * @throws NotYourMusicException
	 * @throws DuplicatedSongException
	 */
	public void createAlbum(String name, int year, Set<Song> songs)
			throws InvalidTitleException, NotYourMusicException, DuplicatedSongException {
		if (albums == null)
			albums = Collections.newSetFromMap(new WeakHashMap<>());
		
		for (Album a : albums)
			if ((a.getName()).equals(name))
				throw new InvalidTitleException();
		/*if (!this.songs.containsAll(songs))
			throw new NotYourMusicException();*/
		Album album = new Album(name, year, songs);
		albums.add(album);
		App.addAlbum(album);
	}

	public void addToAlbum(Song s, Album a) throws DuplicatedSongException {
		if (a.getAuthor() == this)
			a.addSong(s);
	}

	/**
	 * Uploads a song and, if everything is correct, a notification is sent to the
	 * admin to validate the song
	 *
	 * @param sname
	 * @param path
	 * @throws FileNotFoundException
	 * @throws FileTooLongException
	 * @throws InvalidTitleException
	 * @throws ExistingNotificationException
	 */
	public Song uploadSong(String sname, String path)
			throws IOException, FileTooLongException, InvalidTitleException, ExistingNotificationException {
		if (!validateSongTitle(sname)) {
			throw new InvalidTitleException();
		}
		String newPath = "mp3/" + sname + this.userName + ".mp3";
		this.copyFileTo(path, newPath);
		Song s = new Song(sname, newPath, this);
		App.getAdmin().addNotification(new ValidationNotif(s));
		songs.add(s);
		App.addSong(s);
		return s;
	}

	private void copyFileTo(String fromPath, String toPath) throws IOException {
		Path from = Paths.get((new File(fromPath)).getAbsolutePath());
		Path to = Paths.get((new File(toPath)).getAbsolutePath());
		Files.copy(from,to, StandardCopyOption.REPLACE_EXISTING);
	}

	public void addToSongs(Song s) {
		songs.add(s);
	}

	/**
	 * Changes
	 *
	 * @param s
	 * @param sname
	 * @param path
	 * @throws InvalidTitleException
	 * @throws ExistingNotificationException
	 * @throws FileTooLongException
	 * @throws IOException 
	 */
	public void changeSongData(Song s, String sname, String path) throws Exception {
		SongState songState = s.getSongState();
		if (!(songState == SongState.WITH_ERRORS || songState == SongState.PENDING_TO_VALIDATE))
			throw new NotChangeableSongException();
		String oldName = s.getName();
		s.setName("temporal name");
		if (!validateSongTitle(sname)) {
			s.setName(oldName);
			throw new InvalidTitleException();
		}
		if (s.getAuthor() == this) {
			s.change(sname, path);
		}
		if (songState == SongState.WITH_ERRORS) {
			for (UserNotif n : this.notifications) {
				if (n.isNotValidatedNotif() && n.getSong().equals(s)) {
					this.notifications.remove(n);
					break;
				}
			}
			App.getAdmin().addNotification(new ValidationNotif(s));
		}
	}

	/**
	 * Sees if a song title is not already taken by him and therefore valid.
	 *
	 * @param sname
	 * @return true if it is not taken, else false
	 */
	public boolean validateSongTitle(String sname) {
		for (Song s : songs)
			if ((s.getName()).equals(sname))
				return false;
		return true;
	}

	// RESTORE METHODS
	public long getId() {
		return this.id;
	}

	public static void updateTotal(long newtotal) {
		total = newtotal;
		return;
	}

	public void loadFollow(RegUser r) {
		following.add(r);
		return;
	}

	public void loadSong(Song s) {
		songs.add(s);
		return;
	}

	public void loadAlbum(Album a) {
		albums.add(a);
		return;
	}

	/**
	 * Gets the songsid field, after loading it.
	 *
	 * @return songsid
	 */

	public Set<Long> getSongsId() {
		if (songs == null) {
			songs = Collections.newSetFromMap(new WeakHashMap<>());
		} else {
			for (Song s : songs) {
				songsid.add(Long.valueOf(s.getId()));
			}
		}

		return songsid;
	}

	/**
	 * Gets the albumsid field, after loading it.
	 *
	 * @return albumsid
	 */

	public Set<Long> getAlbumsId() {
		if (albums == null) {
			albums = Collections.newSetFromMap(new WeakHashMap<>());
		} else {
			for (Album a : albums) {
				albumsid.add(Long.valueOf(a.getId()));
			}
		}

		return albumsid;
	}

	/**
	 * Checks if the songs of a user must be completely deleted.
	 */

	public void reviewSongs() {
		for (Song s : songs) {
			try {
				s.checkDateValidation();
			} catch (DeleteException e) {
				s.delete();
				for (UserNotif un : this.notifications) {
					if (un.isNotValidatedNotif() && un.getSong().equals(s)) {
						this.notifications.remove(un);
						break;
					}
				}
			}
		}
	}

	public boolean isRegUser() {
		return true;
	}

	/**
	 * Unfollows a user
	 * @param user
	 */
	public void unfollow(RegUser u) {
		this.following.remove(u);
		u.removeFollower(this);
	}

	/**
	 * @return set with the pending to validate songs
	 */
	public Set<Song> getPendingSongs() {
		Set<Song> aux = new HashSet<>();
		for (Song s: songs) {
			if(s.getSongState()== SongState.PENDING_TO_VALIDATE || s.getSongState() == SongState.WITH_ERRORS)
				aux.add(s);
		}
		return aux;
	}

	/**
	 * @return set with validated songs
	 */
	public Set<Song> getValidatedSongs() {
		Set<Song> aux = new HashSet<>();
		for (Song s: songs) {
			if(s.getSongState()== SongState.VALIDATED)
				aux.add(s);
		}
		return aux;
	}

}
