
package app.users;

/**
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 */

public enum UserState {
	ANONYMOUS, ADMIN, REGISTERED, PREMIUM, BLOCKED, BLOCKED_FOREVER;
}
