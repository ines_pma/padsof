package app.users;

/**
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 *
 */

public abstract class UserAux extends User implements java.io.Serializable {

	private static final long serialVersionUID = -5871632865282967344L;
	protected String userName;
	protected String passwd;

	/**
	 * @param underage
	 * @param plays
	 * @param userState
	 * @param userName
	 * @param passwd
	 */
	public UserAux(boolean underage, int plays, UserState userState, String userName, String passwd) {
		super(underage, plays, userState);
		if (userName == null)
			throw new NullPointerException();
		this.userName = userName;
		this.passwd = passwd;
	}

	/**
	 * @return userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return password
	 */
	public String getPasswd() {
		return passwd;
	}
}
