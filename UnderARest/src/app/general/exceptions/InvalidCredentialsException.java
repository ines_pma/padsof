package app.general.exceptions;

@SuppressWarnings("serial")
public class InvalidCredentialsException extends Exception {
	@Override
	public String toString() {
		return "Invalid username/password";
	}
}
