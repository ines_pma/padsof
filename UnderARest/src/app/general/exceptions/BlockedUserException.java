package app.general.exceptions;

import java.time.LocalDate;

@SuppressWarnings("serial")
public class BlockedUserException extends Exception {
	private LocalDate date;

	public BlockedUserException(LocalDate date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "This user is blocked until " + date;
	}

}
