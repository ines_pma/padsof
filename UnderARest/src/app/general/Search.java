package app.general;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import app.App;
import app.media.Album;
import app.media.Song;
import app.media.SongState;
import app.users.RegUser;

/**
 * Contains the method needed to look for songs, albums and users
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */

public class Search {

	/**
	 * Searches for the validated songs that match a given string. If the actual
	 * user is anonymous or under 18, it won't show explicit content.
	 * 
	 * @param search String you want to look for
	 * @return Set of songs which match the search string
	 */
	public static Set<Song> searchSong(String search) {
		Boolean underage = App.getAppInstance().getActualUser().isUnderage();
		Collection<Song> songs = App.getSongs().values();
		Set<Song> result = new HashSet<>();
		for (Song s : songs) {
			if (s.getName().toLowerCase().contains(search.toLowerCase())) {
				if (!underage || !s.getExplicit())
					if (s.getSongState() == SongState.VALIDATED)
						result.add(s);
			}
		}
		return result;
	}

	/**
	 * Searches for the albums that match a given string. If the actual user is
	 * anonymous or under 18, it won't show albums which contain explicit content.
	 * 
	 * @param search String you want to look for
	 * @return Set of albums which match the search string
	 */
	public static Set<Album> searchAlbum(String search) {
		Boolean underage = App.getAppInstance().getActualUser().isUnderage();
		Collection<Album> albums = App.getAlbums().values();
		Set<Album> result = new HashSet<>();
		for (Album a : albums) {
			if (a.getName().toLowerCase().contains(search.toLowerCase())) {
				if (!underage || !a.getExplicit())
					result.add(a);
			}
		}
		return result;
	}

	/**
	 * Searches for the users that match a given string. Users may match by username
	 * or author name
	 * 
	 * @param search String you want to look for
	 * @return Set of users which match the search string
	 */
	public static Set<RegUser> searchUser(String search) {
		Collection<RegUser> users = App.getUsers().values();
		Set<RegUser> result = new HashSet<>();
		for (RegUser u : users) {
			if (u.getUserName().toLowerCase().contains(search.toLowerCase())
					|| u.getAuthorName().toLowerCase().contains(search.toLowerCase())) {
				result.add(u);
			}
		}
		return result;
	}
}
