package app.general;

import es.uam.eps.padsof.telecard.InvalidCardNumberException;
import es.uam.eps.padsof.telecard.OrderRejectedException;
import es.uam.eps.padsof.telecard.TeleChargeAndPaySystem;

/**
 * The bank is used to simulate payments from the users.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 */

public class Bank implements java.io.Serializable {

	private static final long serialVersionUID = -5036897333921611946L;
	private static Bank bank = null;

	public static Bank getBankInstance() {
		if (bank == null)
			bank = new Bank();
		return bank;
	}

	private Bank() {
	}

	/**
	 * Executes a payment with the given arguments, if possible.
	 * 
	 * @param card    card number
	 * @param subject subject of the payment
	 * @param amount  value of the payment
	 * @return boolean indicating success or fail
	 * @throws InvalidCardNumberException
	 * @throws OrderRejectedException
	 */
	public boolean pay(String card, String subject, double amount)
			throws InvalidCardNumberException, OrderRejectedException {

		TeleChargeAndPaySystem.charge(card, subject, amount, true);

		return true;
	}

}
