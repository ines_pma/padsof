package app.general;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import app.App;
import app.media.Album;
import app.media.Music;
import app.media.Playlist;
import app.media.Song;
import app.notification.AdminNotif;
import app.notification.SongRelatedNotif;
import app.notification.UserNotif;
import app.users.Admin;
import app.users.RegUser;

/**
 * 
 * The LoadApp class is used to manage the backup of the application.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 */

public class LoadApp {

	/**
	 * Makes a backup of the songs of the application.
	 * 
	 * @param songsdata  file to store the data
	 * @param songs      songs to be saved
	 * @param totalmusic number of ids used (to be saved)
	 */

	public static void saveSongs(String songsdata, Map<Long, Song> songs, long totalmusic) {
		try {
			PrintWriter pw = new PrintWriter(songsdata);
			pw.close();
			FileOutputStream fileOut = new FileOutputStream(songsdata);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);

			out.writeObject(songs);
			out.writeObject(totalmusic);

			fileOut.close();

		} catch (IOException i) {
			i.printStackTrace();
		}
		return;
	}

	/**
	 * Makes a backup of the users of the application.
	 * 
	 * @param usersdata file to store the data
	 * @param users     users to be saved
	 * @param admin     admin to be saved
	 */

	public static void saveUsers(String usersdata, Map<Long, RegUser> users, Admin admin) {
		try {
			PrintWriter pw = new PrintWriter(usersdata);
			pw.close();
			FileOutputStream fileOut = new FileOutputStream(usersdata);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);

			out.writeObject(users);
			out.writeObject(admin);

			fileOut.close();

		} catch (IOException i) {
			i.printStackTrace();
		}
		return;
	}

	/**
	 * Makes a backup of the songs of the application.
	 * 
	 * @param albumsdata file to store the data
	 * @param albums     albums to be saved
	 */

	public static void saveAlbums(String albumsdata, Map<Long, Album> albums) {
		try {
			PrintWriter pw = new PrintWriter(albumsdata);
			pw.close();
			FileOutputStream fileOut = new FileOutputStream(albumsdata);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);

			out.writeObject(albums);

			fileOut.close();

		} catch (IOException i) {
			i.printStackTrace();
		}
		return;
	}

	/**
	 * Saves the id fields, to retrieve the app weakmaps when loading the app.
	 * 
	 * @param users
	 * @param songs
	 * @param albums
	 */
	public static void saveIdFields(Map<Long, RegUser> users, Map<Long, Song> songs, Map<Long, Album> albums) {
		for (RegUser r : users.values()) {
			r.getAlbumsId();
			r.getSongsId();
		}
		for (Song s : songs.values())
			s.getContainedId();
		for (Album a : albums.values()) {
			a.getContainedId();
			a.getMusicId();
		}
	}

	/**
	 * Loads the songs
	 * 
	 * @param songsdata file
	 */

	@SuppressWarnings("unchecked")
	public static void loadSongs(String songsdata) {
		try {
			FileInputStream fileIn = new FileInputStream(songsdata);
			ObjectInputStream in = new ObjectInputStream(fileIn);

			Map<Long, Song> songs = (Map<Long, Song>) in.readObject();
			long totalmusic = (long) in.readObject();
			App.setTotalMusic(totalmusic);
			App.setSongs(songs);
			in.close();
			fileIn.close();
		} catch (IOException | ClassNotFoundException i) {
			i.printStackTrace();
			return;
		}
	}

	/**
	 * Loads the users
	 * 
	 * @param usersdata file
	 */

	@SuppressWarnings("unchecked")
	public static void loadUsers(String usersdata) {
		try {

			FileInputStream fileIn = new FileInputStream(usersdata);
			ObjectInputStream in = new ObjectInputStream(fileIn);

			Map<Long, RegUser> users = (Map<Long, RegUser>) in.readObject();
			RegUser.updateTotal(users.size());
			Admin admin = (Admin) in.readObject();

			App.setAdmin(admin);
			App.setUsers(users);

			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
			return;
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
			return;
		}
	}

	/**
	 * Loads the albums
	 * 
	 * @param albumsdata file
	 */

	@SuppressWarnings("unchecked")
	public static void loadAlbums(String albumsdata) {
		try {
			FileInputStream fileIn = new FileInputStream(albumsdata);
			ObjectInputStream in = new ObjectInputStream(fileIn);

			Map<Long, Album> albums = (Map<Long, Album>) in.readObject();
			App.setAlbums(albums);

			in.close();
			fileIn.close();
		} catch (IOException i) {
			i.printStackTrace();
			return;
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
			return;
		}
	}

	/**
	 * Retrieves the authors of the songs and playlists after reloading the app.
	 * 
	 * @param songs
	 * @param playlists
	 * @param users
	 */

	public static void loadAuthors(Map<Long, Song> songs, Map<Long, Playlist> playlists, Map<Long, RegUser> users) {
		for (Song s : songs.values()) {
			RegUser aux = users.get(s.getAuthor().getId());
			s.loadAuthor(aux);
		}
		for (Playlist p : playlists.values()) {
			RegUser aux = users.get(p.getAuthor().getId());
			p.loadAuthor(aux);
		}
	}

	/**
	 * Loads the playlists as an auxiliar variable (to help when reloading the
	 * weakmaps).
	 * 
	 * @param users
	 * @param playlists
	 */

	public static void loadPlaylists(Map<Long, RegUser> users, Map<Long, Playlist> playlists) {
		Collection<RegUser> aux = users.values();
		for (RegUser a : aux) {
			Set<Playlist> p = a.getPlaylists();
			for (Playlist paux : p) {
				playlists.put(paux.getId(), paux);
			}
		}
	}

	/**
	 * Loads the notifications of users and admin.
	 * 
	 * @param admin
	 * @param users
	 * @param songs
	 */

	public static void loadNotifications(Admin admin, Map<Long, RegUser> users, Map<Long, Song> songs) {
		for (AdminNotif a : admin.getNotifications()) {
			a.loadSong(songs.get(a.getSongId()));
		}
		for (RegUser u : users.values()) {
			for (UserNotif n : u.getNotifications()) {
				if (n.isSongRelated()) {
					SongRelatedNotif s = (SongRelatedNotif) n;
					s.loadSong(songs.get(s.getSongId()));
				}
			}
		}

	}

	/**
	 * Loads the maximum ids used of music and users.
	 * 
	 * @param totalmusic
	 * @param totalusers
	 */

	public static void loadTotals(long totalmusic, long totalusers) {
		Music.updateTotal(totalmusic);
		RegUser.updateTotal(totalusers);
		return;
	}

	/**
	 * Loads the containedid field (weakmap).
	 * 
	 * @param songs
	 * @param albums
	 * @param playlists
	 */

	public static void loadContainedId(Map<Long, Song> songs, Map<Long, Album> albums, Map<Long, Playlist> playlists) {
		for (Song s : songs.values()) {
			Set<Long> aux = s.getContainedId();
			if (aux == null)
				break;
			for (long l : aux) {
				if (albums.containsKey(l)) {
					s.loadContained(albums.get(l));
				} else if (playlists.containsKey(l)) {
					s.loadContained(playlists.get(l));
				}
			}
		}

		for (Album a : albums.values()) {
			Set<Long> aux = a.getContainedId();
			if (aux == null)
				break;
			for (long l : aux) {
				if (playlists.containsKey(l)) {
					a.loadContained(playlists.get(l));
				}
			}
		}

		for (Playlist p : playlists.values()) {
			Set<Long> aux = p.getContainedId();
			if (aux == null)
				break;
			for (long l : aux) {
				if (playlists.containsKey(l)) {
					p.loadContained(playlists.get(l));
				}
			}
		}

	}

	/**
	 * Loads the musicid field (weakmap).
	 * 
	 * @param albums
	 * @param songs
	 * @param playlists
	 */

	public static void loadMusicId(Map<Long, Album> albums, Map<Long, Song> songs, Map<Long, Playlist> playlists) {
		for (Album a : albums.values()) {
			Set<Long> aux = a.getMusicId();
			if (aux == null)
				break;
			for (long l : aux) {
				if (songs.containsKey(l)) {
					a.loadMusic(songs.get(l));
				}
			}
		}

		for (Playlist p : playlists.values()) {
			Set<Long> aux = p.getMusicId();
			for (long l : aux) {
				if (aux == null)
					break;
				if (songs.containsKey(l)) {
					p.loadMusic(songs.get(l));
				} else if (albums.containsKey(l)) {
					p.loadMusic(albums.get(l));
				} else if (playlists.containsKey(l)) {
					p.loadMusic(playlists.get(l));
				}
			}
		}
	}

	/**
	 * Loads the songsid field (weakmap).
	 * 
	 * @param songs
	 * @param users
	 */

	public static void loadSongsId(Map<Long, Song> songs, Map<Long, RegUser> users) {
		for (RegUser r : users.values()) {
			Set<Long> aux = r.getSongsId();
			if (aux == null)
				break;
			for (long l : aux) {
				if (songs.containsKey(l)) {
					r.loadSong(songs.get(l));
				}
			}
		}
	}

	/**
	 * Loads the albumsid field (weakmap).
	 * 
	 * @param users
	 * @param playlists
	 * @param albums
	 */

	public static void loadAlbumsId(Map<Long, RegUser> users, Map<Long, Playlist> playlists, Map<Long, Album> albums) {
		for (RegUser r : users.values()) {
			Set<Long> aux = r.getAlbumsId();
			if (aux == null)
				break;
			for (long l : aux) {
				if (albums.containsKey(l)) {
					r.loadAlbum(albums.get(l));
				}
			}
		}
	}
}
