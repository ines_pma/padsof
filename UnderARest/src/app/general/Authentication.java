package app.general;

import java.time.LocalDate;
import java.util.Collection;

import app.App;
import app.exceptions.NameAlreadyExistsException;
import app.general.exceptions.BlockedUserException;
import app.general.exceptions.InvalidCredentialsException;
import app.users.RegUser;
import app.users.User;
import app.users.UserState;
import app.users.exceptions.NotBornException;

/**
 * Methods to control the login and registration
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 *
 */
public class Authentication {

	/**
	 * If possible, registers a user with the introduced parameters.
	 * 
	 * @param username username
	 * @param pass     password
	 * @param author   authorname
	 * @param birthday birthday
	 * @throws NameAlreadyExistsException if the username is already taken
	 * @throws NotBornException
	 */
	public static void register(String username, String pass, String author, LocalDate birthday)
			throws NameAlreadyExistsException, NotBornException {
		Collection<RegUser> users = App.getUsers().values();
		for (RegUser u : users) {
			if (u.getUserName().equals(username)) {
				throw new NameAlreadyExistsException();
			}
		}
		RegUser u = new RegUser(username, pass, author, birthday);
		App.addUser(u);
	}

	/**
	 * Enters as an anonymous user to the app.
	 */
	public static void enterAsGuest() {
		User u = new User();
		App.getAppInstance().setActualUser(u);
		return;
	}

	/**
	 * It checks the username and password and logs into the application. It will
	 * update all the user's info
	 * 
	 * @param username
	 * @param psswrd   password
	 * @throws InvalidCredentialsException
	 * @throws BlockedUserException
	 */
	public static void login(String username, String psswrd) throws InvalidCredentialsException, BlockedUserException {
		Collection<RegUser> users = App.getUsers().values();
		if (App.getAdmin().getUserName().equals(username) && App.getAdmin().getPasswd().equals(psswrd)) {
			App.getAppInstance().setActualUser(App.getAdmin());
			return;
		}
		for (RegUser u : users) {
			if (u.getUserName().equals(username) && u.getPasswd().equals(psswrd)) {
				App.getAppInstance().setActualUser(u);
				u.update();
				u.reviewSongs();
				if (u.getUserState() == UserState.BLOCKED || u.getUserState() == UserState.BLOCKED_FOREVER)
					throw new BlockedUserException(u.getExpDate());
				return;
			}
		}
		throw new InvalidCredentialsException();
	}

	/**
	 * Logs out of the app
	 */
	public static void logout() {
		App.getAppInstance().setActualUser(null);
	}

}
