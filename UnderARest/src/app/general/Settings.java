package app.general;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import app.general.exceptions.InvalidValueException;

/**
 * * This class is used to load and change the settings values.
 * 
 * @author Ines Pastor
 * @author Carmen Díez
 * @author Ignacio Echave-Sustaeta
 */

public class Settings implements java.io.Serializable {

	private static final long serialVersionUID = 465694154436305110L;
	private double premiumPrice;
	private int numSongAnon;
	private int numSongReg;
	private int underage;
	private int repUpgrade;
	private int blockedDays;

	private static String archivo = "txt/Settings.txt";

	private static Settings settings = null;

	public static Settings getSettingsInstance() {
		if (settings == null)
			try {
				settings = new Settings(archivo);
			} catch (IOException | InvalidValueException e) {
				e.printStackTrace();
			}
		return settings;
	}

	private Settings(String archivo) throws IOException, InvalidValueException {
		readSettingsFile(archivo);
	}

	private void readSettingsFile(String archivo) throws IOException, InvalidValueException {
		BufferedReader buffer = new BufferedReader(new InputStreamReader(new FileInputStream(archivo)));
		String line = buffer.readLine();
		String words[] = line.split(" ");

		this.premiumPrice = Double.parseDouble(words[0]);

		this.numSongAnon = Integer.parseInt(words[1]);

		this.numSongReg = Integer.parseInt(words[2]);

		this.underage = Integer.parseInt(words[3]);

		this.repUpgrade = Integer.parseInt(words[4]);

		this.blockedDays = Integer.parseInt(words[5]);

		if (premiumPrice < 0 || numSongAnon < 0 || numSongReg < 0 || underage < 0 || repUpgrade < 0
				|| blockedDays < 0) {
			buffer.close();
			throw new InvalidValueException();
		}

		buffer.close();
	}

	/**
	 * Returns the price to pay to get a premium subscription.
	 * 
	 * @return the premiumPrice
	 */
	public double getPremiumPrice() {
		return premiumPrice;
	}

	/**
	 * @param premiumPrice the premiumPrice to set
	 * @throws InvalidValueException
	 */
	public void setPremiumPrice(double premiumPrice) throws InvalidValueException {
		if (premiumPrice < 0)
			throw new InvalidValueException();
		this.premiumPrice = premiumPrice;
		updateFile();
		return;
	}

	/**
	 * Returns the number of songs that an anonymous user can listen in a month.
	 * 
	 * @return the numSongAnon
	 */
	public int getNumSongAnon() {
		return numSongAnon;
	}

	/**
	 * @param numSongAnon the numSongAnon to set
	 * @throws InvalidValueException
	 */
	public void setNumSongAnon(int numSongAnon) throws InvalidValueException {
		if (numSongAnon < 0)
			throw new InvalidValueException();
		this.numSongAnon = numSongAnon;
		updateFile();
		return;
	}

	/**
	 * Returns the number of songs that a registered user can listen in a month, if
	 * he is not premium.
	 * 
	 * @return the numSongReg
	 */
	public int getNumSongReg() {
		return numSongReg;
	}

	/**
	 * @param numSongReg the numSongReg to set
	 * @throws InvalidValueException
	 */
	public void setNumSongReg(int numSongReg) throws InvalidValueException {
		if (numSongReg < 0)
			throw new InvalidValueException();
		this.numSongReg = numSongReg;
		updateFile();
		return;
	}

	/**
	 * Returns the minimum age to stop being considered underage (in years)
	 * 
	 * @return the underage
	 */
	public int getUnderage() {
		return underage;

	}

	/**
	 * @param underage the underage to set
	 * @throws InvalidValueException
	 */
	public void setUnderage(int underage) throws InvalidValueException {
		if (underage < 0)
			throw new InvalidValueException();
		this.underage = underage;
		updateFile();
		return;
	}

	/**
	 * Returns the number of reproductions which grants Premium to a user.
	 * 
	 * @return the repUpgrade
	 */
	public int getRepUpgrade() {
		return repUpgrade;
	}

	/**
	 * @param repUpgrade the repUpgrade to set
	 * @throws InvalidValueException
	 */
	public void setRepUpgrade(int repUpgrade) throws InvalidValueException {
		if (repUpgrade < 0)
			throw new InvalidValueException();
		this.repUpgrade = repUpgrade;
		updateFile();
		return;
	}

	/**
	 * Returns the number of days to correct a blocked song.
	 * 
	 * @return the blockedDays
	 */
	public int getBlockedDays() {
		return blockedDays;
	}

	/**
	 * @param blockedDays the blockedDays to set
	 * @throws InvalidValueException
	 */
	public void setBlockedDays(int blockedDays) throws InvalidValueException {
		if (blockedDays < 0)
			throw new InvalidValueException();
		this.blockedDays = blockedDays;
		updateFile();
		return;
	}

	/**
	 * For testing purposes, in the real app the file will be always the same to
	 * avoid problems
	 * 
	 * @throws InvalidValueException
	 * @throws IOException
	 */

	public void setSettingsDataFile(String archivo) throws IOException, InvalidValueException {
		Settings.archivo = archivo;
		readSettingsFile(archivo);
	}

	/**
	 * Updates the settings file (called after changing a value).
	 */

	private void updateFile() {
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(archivo);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		String aux = Double.toString(premiumPrice) + " " + Integer.toString(this.numSongAnon) + " "
				+ Integer.toString(this.numSongReg) + " " + Integer.toString(this.underage) + " "
				+ Integer.toString(this.repUpgrade) + " " + Integer.toString(this.blockedDays);

		pw.print(aux);

		pw.close();
		return;
	}

}