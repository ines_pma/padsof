package app.notification;

import app.media.Song;

/**
 * Informs about expired premium.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 */


public class PremiumExpiredNotif extends UserNotif {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1700206974008673632L;

	@Override
	public String toString() {
		return "Your premium account has expired";
	}
	
	/*
	 * @see app.notification.UserNotif#getSong()
	 */
	@Override
	public Song getSong() {
		return null;
	}

	/*
	 * @see app.notification.UserNotif#isReportedNotif()
	 */
	@Override
	public boolean isReportedNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isNotValidatedNotif()
	 */
	@Override
	public boolean isNotValidatedNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isSongRelated()
	 */
	@Override
	public boolean isSongRelated() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isUploadNotif()
	 */
	@Override
	public boolean isUploadNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isValidatedNotif()
	 */
	@Override
	public boolean isValidatedNotif() {
		return false;
	}

}