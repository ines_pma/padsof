package app.notification;

import app.media.Song;

/**
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 */

public abstract class UserNotif extends Notification {

	private static final long serialVersionUID = -2814026544940980715L;

	public abstract boolean isReportedNotif();

	public abstract boolean isNotValidatedNotif();

	public abstract boolean isUploadNotif();

	public abstract boolean isValidatedNotif();

	public abstract boolean isSongRelated();

	public abstract Song getSong();
}