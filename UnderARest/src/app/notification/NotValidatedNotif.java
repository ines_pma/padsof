package app.notification;

import app.media.Song;

/**
 * Informs of a not validated song
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 * 
 */

public class NotValidatedNotif extends SongRelatedNotif {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4906310035007944364L;

	public NotValidatedNotif(Song s) {
		super(s);
	}
	
	@Override
	public String toString() {
		return "Your song " + this.getSong().getName() + "hasn't been validated";
	}

	@Override
	public boolean isNotValidatedNotif() {
		return true;
	}

	/*
	 * @see app.notification.UserNotif#isReportedNotif()
	 */
	@Override
	public boolean isReportedNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isUploadNotif()
	 */
	@Override
	public boolean isUploadNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isValidatedNotif()
	 */
	@Override
	public boolean isValidatedNotif() {
		return false;
	}
}