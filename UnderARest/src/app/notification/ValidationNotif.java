package app.notification;

import app.media.Song;

/**
 * Informs about a song that has to be revised.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 * 
 *
 */


public class ValidationNotif extends AdminNotif {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7534822533538825943L;

	public ValidationNotif(Song s) {
		super(s);
	}

	@Override
	public String toString() {
		return "You have to check a song for validation: " + this.getSong() + "\n";
	}

	/*
	 * @see app.notification.AdminNotif#isValidationNotif()
	 */
	@Override
	public boolean isValidationNotif() {
		return true;
	}

	/*
	 * @see app.notification.AdminNotif#isReportNotif()
	 */
	@Override
	public boolean isReportNotif() {
		return false;
	}

}