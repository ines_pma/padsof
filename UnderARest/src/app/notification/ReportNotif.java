package app.notification;

import app.media.Song;
import app.users.RegUser;

/**
 * Informs about a report to some song.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 * 
 */


public class ReportNotif extends AdminNotif {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1032330472524226346L;
	private RegUser reporter;
	private String comment;

	public ReportNotif(Song s, RegUser reporter, String comment) {
		super(s);
		this.reporter = reporter;
		this.comment = comment;
	}

	/**
	 * @return the reporter
	 */
	public RegUser getReporter() {
		return reporter;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/*
	 * @see app.notification.AdminNotif#isReportNotif()
	 */
	@Override
	public boolean isReportNotif() {
		return true;
	}

	/*
	 * @see app.notification.AdminNotif#isValidationNotif()
	 */
	@Override
	public boolean isValidationNotif() {
		return false;
	}

}