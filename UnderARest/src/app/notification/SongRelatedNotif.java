package app.notification;

import java.lang.ref.WeakReference;

import app.media.Song;

/**
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 */


public abstract class SongRelatedNotif extends UserNotif {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4473916313926287507L;
	private transient WeakReference<Song> r;
	private long songid;

	public SongRelatedNotif(Song s) {
		this.r = new WeakReference<Song>(s);
		songid = s.getId();
	}

	/**
	 * @return the song
	 */
	@Override
	public Song getSong() {
		if (r == null)
			return null;
		return r.get();
	}

	@Override
	public boolean isSongRelated() {
		return true;
	}

	/**
	 * 
	 * @return the songid
	 */
	public long getSongId() {
		return songid;
	}

	public void loadSong(Song s) {
		r = new WeakReference<Song>(s);
	}

}