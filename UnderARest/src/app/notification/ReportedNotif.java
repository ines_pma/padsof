package app.notification;

import app.media.Song;

/**
 * Informs about a reported song of yours.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 * 
 *
 */

public class ReportedNotif extends SongRelatedNotif {

	/**
	 * 
	 */
	private static final long serialVersionUID = 955597142384069341L;

	public ReportedNotif(Song s) {
		super(s);
	}
	
	@Override
	public String toString() {
		return "Your song " + this.getSong().getName() + "has been reported";
	}

	@Override
	public boolean isReportedNotif() {
		return true;
	}

	/*
	 * @see app.notification.UserNotif#isNotValidatedNotif()
	 */
	@Override
	public boolean isNotValidatedNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isUploadNotif()
	 */
	@Override
	public boolean isUploadNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isValidatedNotif()
	 */
	@Override
	public boolean isValidatedNotif() {
		return false;
	}
}