package app.notification;

import java.lang.ref.WeakReference;

import app.media.Song;

/**
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 */

public abstract class AdminNotif extends Notification {
	private static final long serialVersionUID = -7120388609839454215L;
	private transient WeakReference<Song> r;
	private long songid;

	public AdminNotif(Song s) {
		this.r = new WeakReference<Song>(s);
		songid = s.getId();
	}

	/**
	 * @return the song
	 */
	public Song getSong() {
		if (r == null)
			return null;
		return r.get();
	}

	/**
	 * 
	 * @return the songid
	 */
	public long getSongId() {
		return songid;
	}

	public void loadSong(Song s) {
		r = new WeakReference<Song>(s);
	}

	public abstract boolean isReportNotif();

	public abstract boolean isValidationNotif();
}