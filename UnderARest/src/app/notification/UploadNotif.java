package app.notification;

import app.media.Song;

/**
 * Informs about an upload of a followed user.
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 *
 *
 */

public class UploadNotif extends SongRelatedNotif {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6200287776801262656L;

	public UploadNotif(Song s) {
		super(s);
	}
	
	@Override
	public String toString() {
		return this.getSong().getAuthor().getAuthorName() + " has uploaded a new song " + this.getSong().getName();
	}

	/*
	 * @see app.notification.UserNotif#isUploadNotif()
	 */
	@Override
	public boolean isUploadNotif() {
		return true;
	}

	/*
	 * @see app.notification.UserNotif#isReportedNotif()
	 */
	@Override
	public boolean isReportedNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isNotValidatedNotif()
	 */
	@Override
	public boolean isNotValidatedNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isValidatedNotif()
	 */
	@Override
	public boolean isValidatedNotif() {
		return false;
	}

}