package app.notification;

import app.media.Song;

/**
 * Informs about a song of yours being validated
 * 
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 * 
 * 
 */

public class ValidatedNotif extends SongRelatedNotif {

	/**
	 * 
	 */
	private static final long serialVersionUID = 228474938038818397L;

	public ValidatedNotif(Song s) {
		super(s);
	}

	@Override
	public String toString() {
		return "Your song " + this.getSong().getName() + " has been validated.\n";
	}

	/*
	 * @see app.notification.UserNotif#isValidatedNotif()
	 */
	@Override
	public boolean isValidatedNotif() {
		return true;
	}

	/*
	 * @see app.notification.UserNotif#isReportedNotif()
	 */
	@Override
	public boolean isReportedNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isNotValidatedNotif()
	 */
	@Override
	public boolean isNotValidatedNotif() {
		return false;
	}

	/*
	 * @see app.notification.UserNotif#isUploadNotif()
	 */
	@Override
	public boolean isUploadNotif() {
		return false;
	}
}