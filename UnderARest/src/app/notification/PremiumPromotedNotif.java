/**
 * 
 */
package app.notification;

import app.media.Song;


public class PremiumPromotedNotif extends UserNotif{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7226335956684976233L;

	@Override
	public String toString() {
		return "Congratulations! You've been upgraded to premium";
	}
	
	@Override
	public boolean isReportedNotif() {
		return false;
	}

	@Override
	public boolean isNotValidatedNotif() {
		return false;
	}

	@Override
	public boolean isUploadNotif() {
		return false;
	}

	@Override
	public boolean isValidatedNotif() {
		return false;
	}

	@Override
	public boolean isSongRelated() {
		return false;
	}

	@Override
	public Song getSong() {
		return null;
	}

}
