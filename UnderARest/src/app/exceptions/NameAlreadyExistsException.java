package app.exceptions;

/**
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 *         This exceptions appears if a name is already being used and someone
 *         tries to use it
 */

@SuppressWarnings("serial")
public class NameAlreadyExistsException extends Exception {

	@Override
	public String toString() {
		return "This name is already taken, use a different one";
	}

}
