package app;

import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import app.general.Bank;
import app.general.LoadApp;
import app.general.Settings;
import app.media.Album;
import app.media.Music;
import app.media.PlayingWait;
import app.media.Playlist;
import app.media.Song;
import app.users.Admin;
import app.users.RegUser;
import app.users.User;
import pads.musicPlayer.Mp3Player;
import pads.musicPlayer.exceptions.Mp3PlayerException;

/**
 * App contains the general aspects and data of the complete application.
 *
 * @author Ines Pastor
 * @author Carmen Diez
 * @author Ignacio Echave-Sustaeta
 *
 *
 */

public class App implements java.io.Serializable {

	private static final long serialVersionUID = -1390093094206850089L;

	private static Mp3Player player;
	private static Thread thread;
	private static PlayingWait runnable;

	private static Bank bank = Bank.getBankInstance();
	private static Settings settings = Settings.getSettingsInstance();
	private static Map<Long, RegUser> users = new HashMap<Long, RegUser>();
	private static Map<Long, Song> songs = new HashMap<Long, Song>();
	private static Map<Long, Album> albums = new HashMap<Long, Album>();
	private static Map<Long, Playlist> playlists = new HashMap<Long, Playlist>();
	private transient User actualUser = null;
	private static Admin admin = Admin.getAdminInstance();

	private static long totalmusic = 0;
	private static long totalusers = 0;
	private static String usersdata = "data/users.ser";
	private static String albumsdata = "data/albums.ser";
	private static String songsdata = "data/songs.ser";

	private Music currentMusic = null;
	private Song currentSong = null;

	private static App app = null;

	public static App getAppInstance() {
		if (app == null)
			app = new App();
		return app;
	}

	private App() {
		loadApp();
		LoadApp.loadPlaylists(users, playlists);
		LoadApp.loadAuthors(songs, playlists, users);
		loadWeakMaps();
		totalusers = users.size();
		LoadApp.loadTotals(totalmusic, totalusers);
		playlists = null;
	}

	public User getActualUser() {
		return actualUser;
	}

	public void setActualUser(User user) {
		actualUser = user;
	}

	static public Mp3Player getPlayer() {
		return player;
	}

	public static void setTotalMusic(long a) {
		totalmusic = a;
	}

	public static void setTotalUsers(long a) {
		totalusers = a;
	}

	public static long getTotalMusic() {
		return totalmusic;
	}

	public static Map<Long, RegUser> getUsers() {
		return Collections.unmodifiableMap(users);
	}

	public static long getTotalUsers() {
		return totalusers;
	}

	/**
	 * @return the songs
	 */
	public static Map<Long, Song> getSongs() {
		return songs;
		//return Collections.unmodifiableMap(songs);
	}

	/**
	 * @return the albums
	 */
	public static Map<Long, Album> getAlbums() {
		return Collections.unmodifiableMap(albums);
	}

	public static void addSong(Song s) {
		songs.put(s.getId(), s);
		return;
	}

	public static void addUser(RegUser u) {
		users.put(u.getId(), u);
		return;
	}

	public static void addAlbum(Album a) {
		albums.put(a.getId(), a);
		return;
	}

	public static void deleteSong(Song s) {
		songs.remove(s.getId(), s);
		System.gc();
		return;
	}

	public static void deleteAlbum(Album a) {
		albums.remove(a.getId(), a);
		System.gc();
		return;
	}

	public static Bank getBank() {
		return bank;
	}

	public static Settings getSettings() {
		return settings;
	}

	public static Admin getAdmin() {
		return admin;
	}

	static public Thread newThread(Music m) throws FileNotFoundException, Mp3PlayerException {
		if (thread != null)
			threadStop();
		runnable = new PlayingWait(m);
		thread = new Thread(runnable);
		thread.start();
		return thread;
	}

	static public void threadStop() {
		if (thread != null)
			thread.interrupt();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
		thread = null;
		player = null;
	}

	static public Thread getThread() {
		return thread;
	}

	public static void turnOffApp() {
		saveApp();
		users = new HashMap<Long, RegUser>();
		albums = new HashMap<Long, Album>();
		songs = new HashMap<Long, Song>();
		playlists = new HashMap<Long, Playlist>();
		return;
	}

	/**
	 * For testing purposes (checking that closing and opening loads the data to the
	 * app)
	 */
	public static void reloadData() {
		loadApp();
		LoadApp.loadPlaylists(users, playlists);
		loadWeakMaps();
		LoadApp.loadAuthors(songs, playlists, users);
		LoadApp.loadTotals(totalmusic, totalusers);
		playlists = null;
	}

	/**
	 * Loads the serialized fields from the files.
	 */

	private static void loadApp() {
		LoadApp.loadUsers(usersdata);
		LoadApp.loadAlbums(albumsdata);
		LoadApp.loadSongs(songsdata);
		return;
	}

	/**
	 * Saves the serializable fields to the files.
	 */

	private static void saveApp() {
		LoadApp.saveIdFields(users, songs, albums);
		LoadApp.saveUsers(usersdata, users, admin);
		LoadApp.saveAlbums(albumsdata, albums);
		LoadApp.saveSongs(songsdata, songs, totalmusic);
	}

	/**
	 * Loads the weak maps using the recovered data.
	 */

	private static void loadWeakMaps() {
		LoadApp.loadContainedId(songs, albums, playlists);
		LoadApp.loadMusicId(albums, songs, playlists);
		LoadApp.loadSongsId(songs, users);
		LoadApp.loadAlbumsId(users, playlists, albums);
		LoadApp.loadNotifications(admin, users, songs);
	}

	static public Mp3Player newPlayer() throws FileNotFoundException, Mp3PlayerException {
		if (player != null)
			player.stop();
		player = new Mp3Player();
		return player;
	}

	/**
	 * Method for testing purposes (for testing starting with an empty app)
	 *
	 * @param usersdata  the new users data file
	 * @param albumsdata the new albums data file
	 * @param songsdata  the new songs data file
	 */

	public static void setEmptyDataFiles(String usersdata, String albumsdata, String songsdata) {
		App.usersdata = usersdata;
		App.albumsdata = albumsdata;
		App.songsdata = songsdata;
		users.clear();
		albums.clear();
		songs.clear();
		saveApp();
		loadApp();
		totalmusic = 0;
		totalusers = 0;
		Music.updateTotal(totalmusic);
		RegUser.updateTotal(totalusers);
	}

	/**
	 * @param users
	 */
	public static void setUsers(Map<Long, RegUser> users) {
		App.users = users;
	}

	/**
	 * @param songs
	 */
	public static void setSongs(Map<Long, Song> songs) {
		App.songs = songs;
	}

	/**
	 * @param albums
	 */
	public static void setAlbums(Map<Long, Album> albums) {
		App.albums = albums;
	}

	/**
	 * @param admin
	 */
	public static void setAdmin(Admin admin) {
		App.admin = admin;
	}

	/**
	 * @param music
	 */
	public void setCurrentMusic(Music music) {
		currentMusic = music;

	}

	/**
	 * @param s
	 */
	public void setCurrentSong(Song s) {
		currentSong = s;

	}

	/**
	 * @return current music playing
	 */
	public Music getCurrentMusic() {
		return currentMusic;
	}

	/**
	 * @return current song playing
	 */
	public Song getCurrentSong() {
		return currentSong;
	}

}
