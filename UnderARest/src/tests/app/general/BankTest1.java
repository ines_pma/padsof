package tests.app.general;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import app.general.Bank;
import es.uam.eps.padsof.telecard.InvalidCardNumberException;
import es.uam.eps.padsof.telecard.OrderRejectedException;

public class BankTest1 {

	@Test
	public void SuccessPay() throws InvalidCardNumberException, OrderRejectedException {
		Bank b = Bank.getBankInstance();

		assertNotNull(b);

		assertTrue(b.pay("1234567890101112", "Test pay.", 10.25));

	}

	@Test(expected = InvalidCardNumberException.class)
	public void FailPay() throws InvalidCardNumberException, OrderRejectedException {
		Bank b = Bank.getBankInstance();

		b.pay("111111111", "Hola", 10.24);
	}

}
