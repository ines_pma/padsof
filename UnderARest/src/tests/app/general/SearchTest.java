package tests.app.general;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import app.App;
import app.general.Search;
import app.media.Album;
import app.media.Song;
import app.media.SongState;
import app.media.exceptions.FileTooLongException;
import app.users.RegUser;
import app.users.exceptions.NotBornException;

public class SearchTest {
	static App app;
	static Song s1, s2;
	static Album a1, a2;
	static RegUser u1, u2, u3;

	@BeforeClass
	public static void SetUp() throws NotBornException, FileNotFoundException, FileTooLongException {
		app = App.getAppInstance();
		App.setEmptyDataFiles("data/userstest.ser", "data/albumstest.ser", "data/songstest.ser");
		u1 = new RegUser("srnm", "pssword", "pitumoni", LocalDate.now().minusYears(80));
		App.addUser(u1);
		u2 = new RegUser("srnam", "pssword", "rrrrr", LocalDate.now().minusDays(10));
		App.addUser(u2);
		u3 = new RegUser("srnamm", "pssword", "ritumoni", LocalDate.now().minusDays(10));
		App.addUser(u2);
		s1 = new Song("SoNg", "mp3/chicle3.mp3", u1);
		App.addSong(s1);
		s1.setSongState(SongState.VALIDATED);
		s2 = new Song("Song 2", "mp3/chicle3.mp3", u1);
		s2.setExplicit(true);
		s2.setSongState(SongState.VALIDATED);
		App.addSong(s2);
		a1 = new Album("hgkf r", 1000);
		App.addAlbum(a1);
		a2 = new Album("ijk", 1000);
		a2.setExplicit(true);
		App.addAlbum(a2);

	}

	@Test
	public void testSearchSong1() {
		App.getAppInstance().setActualUser(u1);
		Set<Song> result = Search.searchSong("song");
		assertFalse(App.getAppInstance().getActualUser().isUnderage());
		assertTrue(result.contains(s1));
		assertTrue(result.contains(s2));
		result = Search.searchSong("g 2");
		assertFalse(result.contains(s1));
		assertTrue(result.contains(s2));
	}

	@Test
	public void testSearchSong2() {
		App.getAppInstance().setActualUser(u2);
		Set<Song> result = Search.searchSong("song");
		assertTrue(result.contains(s1));
		assertFalse(result.contains(s2));
		result = Search.searchSong("g 2");
		assertFalse(result.contains(s1));
		assertFalse(result.contains(s2));
	}

	@Test
	public void testSearchSong3() {
		App.getAppInstance().setActualUser(u1);
		s2.setSongState(SongState.REPORTED);
		Set<Song> result = Search.searchSong("song");
		assertTrue(result.contains(s1));
		assertFalse(result.contains(s2));
		result = Search.searchSong("g 2");
		assertFalse(result.contains(s1));
		assertFalse(result.contains(s2));
	}

	@Test
	public void testSearchAlbum1() {
		App.getAppInstance().setActualUser(u1);
		Set<Album> result = Search.searchAlbum("k");
		assertTrue(result.contains(a1));
		assertTrue(result.contains(a2));
		result = Search.searchAlbum("hgkf r");
		assertFalse(result.contains(a2));
		assertTrue(result.contains(a1));
	}

	@Test
	public void testSearchAlbum2() {
		App.getAppInstance().setActualUser(u2);
		Set<Album> result = Search.searchAlbum("k");
		assertTrue(result.contains(a1));
		assertFalse(result.contains(a2));
		result = Search.searchAlbum("hgkf r");
		assertFalse(result.contains(a2));
		assertTrue(result.contains(a1));
	}

	@Test
	public void testSearchUser() {
		Set<RegUser> result = Search.searchUser("rrr");
		assertFalse(result.contains(u1));
		assertTrue(result.contains(u2));
		result = Search.searchUser("pitumoni");
		assertTrue(result.contains(u1));
		assertFalse(result.contains(u2));
		result = Search.searchUser("r");
		assertTrue(result.contains(u1));
		assertTrue(result.contains(u2));
	}
}
