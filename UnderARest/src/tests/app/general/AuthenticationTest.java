package tests.app.general;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import app.App;
import app.exceptions.NameAlreadyExistsException;
import app.general.Authentication;
import app.general.exceptions.BlockedUserException;
import app.general.exceptions.InvalidCredentialsException;
import app.media.Song;
import app.media.SongState;
import app.users.RegUser;
import app.users.UserState;
import app.users.exceptions.NotBornException;

public class AuthenticationTest {
	static App app;
	static RegUser user1, user3;

	@BeforeClass
	public static void setup() throws Exception {
		app = App.getAppInstance();
		user1 = new RegUser("name", "123", "name", LocalDate.now().minusYears(30));
		user3 = new RegUser("name3", "123", "name", LocalDate.now().minusYears(30));
		user3.setUserState(UserState.BLOCKED_FOREVER);
		user1.uploadSong("asd", "mp3/chicle3.mp3");
		Song s1 = user1.getSongs().iterator().next();
		s1.setSongState(SongState.WITH_ERRORS);
		s1.setTimeValid(LocalDate.now().minusDays(4));
		App.addUser(user1);
		App.addUser(user3);
	}

	@Test(expected = NameAlreadyExistsException.class)
	public void registerTest1() throws NameAlreadyExistsException, NotBornException {
		Authentication.register("name", "123", "name", LocalDate.now().minusMonths(20));
	}

	@Test
	public void registerTest2() throws NameAlreadyExistsException, NotBornException {
		Authentication.register("name2", "123", "name", LocalDate.now().minusMonths(20));
	}

	@Test
	public void enterAsGuest() {
		Authentication.enterAsGuest();
		assertEquals(App.getAppInstance().getActualUser().getUserState(), UserState.ANONYMOUS);
	}

	@Test
	public void loginTest1() throws InvalidCredentialsException, BlockedUserException {
		Authentication.login("name", "123");
		assertEquals(App.getAppInstance().getActualUser(), user1);
		Authentication.logout();
		assertNull(App.getAppInstance().getActualUser());
	}

	@Test(expected = InvalidCredentialsException.class)
	public void loginTest2() throws InvalidCredentialsException, BlockedUserException {
		Authentication.login("name", "1234");
	}

	@Test(expected = BlockedUserException.class)
	public void loginTest3() throws InvalidCredentialsException, BlockedUserException {
		Authentication.login("name3", "123");
	}

	@Test
	public void loginTest4() throws InvalidCredentialsException, BlockedUserException {
		Authentication.login("admin", "admin");
	}

	/*
	 * tests with garbage collector may fail sometimes, as the garbage collector
	 * since they are executed for a very small amount of time and the collector may
	 * not pass right away. When executed multiple times we can see it works
	 **/
	/*
	 * @Test public void loginTest5() throws InvalidCredentialsException,
	 * BlockedUserException { Authentication.login("name", "123"); System.gc();
	 * assertTrue(((RegUser) (App.getActualUser())).getSongs().isEmpty()); }
	 */

}
