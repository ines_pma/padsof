package tests.app.general;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import app.general.Settings;
import app.general.exceptions.InvalidValueException;

public class SettingsSettersTest {

	static Settings s;

	@BeforeClass
	public static void before() throws IOException, InvalidValueException {
		s = Settings.getSettingsInstance();
		s.setSettingsDataFile("txt/settingstest2.txt");
	}

	@Test
	public void notNull() {
		assertNotNull(s);
	}

	@Test
	public void premiumPriceTest() throws IOException, InvalidValueException {
		s.setPremiumPrice(10.2);
		assertEquals(s.getPremiumPrice(), 10.2, 0);
	}

	@Test
	public void numSongAnonTest() throws IOException, InvalidValueException {
		s.setNumSongAnon(5);
		assertEquals(s.getNumSongAnon(), 5);
	}

	@Test
	public void numSongRegTest() throws IOException, InvalidValueException {
		s.setNumSongReg(100);
		assertEquals(s.getNumSongReg(), 100);
	}

	@Test
	public void underageTest() throws IOException, InvalidValueException {
		s.setUnderage(20);
		assertEquals(s.getUnderage(), 20);
	}

	@Test
	public void repUpgradeTest() throws IOException, InvalidValueException {
		s.setRepUpgrade(90);
		assertEquals(s.getRepUpgrade(), 90);
	}

	@Test
	public void blockedDaysTest() throws IOException, InvalidValueException {
		s.setBlockedDays(2);
		assertEquals(s.getBlockedDays(), 2);
	}

	@Test(expected = InvalidValueException.class)
	public void premiumPriceFailTest() throws IOException, InvalidValueException {
		s.setPremiumPrice(-1);
	}

	@Test(expected = InvalidValueException.class)
	public void numSongAnonFailTest() throws IOException, InvalidValueException {
		s.setNumSongAnon(-1);
	}

	@Test(expected = InvalidValueException.class)
	public void numSongRegFailTest() throws IOException, InvalidValueException {
		s.setNumSongReg(-1);
	}

	@Test(expected = InvalidValueException.class)
	public void underageFailTest() throws IOException, InvalidValueException {
		s.setUnderage(-1);
	}

	@Test(expected = InvalidValueException.class)
	public void repUpgradeFailTest() throws IOException, InvalidValueException {
		s.setRepUpgrade(-1);
	}

	@Test(expected = InvalidValueException.class)
	public void blockedDaysFailTest() throws IOException, InvalidValueException {
		s.setBlockedDays(-1);
	}
}
