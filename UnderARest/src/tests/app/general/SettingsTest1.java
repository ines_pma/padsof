package tests.app.general;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import app.general.Settings;
import app.general.exceptions.InvalidValueException;

public class SettingsTest1 {

	static Settings s;

	@BeforeClass
	public static void before() {
		s = Settings.getSettingsInstance();
	}

	@Test
	public void notNull() {
		assertNotNull(s);
	}

	@Test
	public void premiumPriceTest() throws IOException, InvalidValueException {
		assertEquals(s.getPremiumPrice(), 5.5, 0);
	}

	@Test
	public void numSongAnonTest() throws IOException, InvalidValueException {
		assertEquals(s.getNumSongAnon(), 10);
	}

	@Test
	public void numSongRegTest() throws IOException, InvalidValueException {
		assertEquals(s.getNumSongReg(), 10);
	}

	@Test
	public void underageTest() throws IOException, InvalidValueException {
		assertEquals(s.getUnderage(), 18);
	}

	@Test
	public void repUpgradeTest() throws IOException, InvalidValueException {
		assertEquals(s.getRepUpgrade(), 40);
	}

	@Test
	public void blockedDaysTest() throws IOException, InvalidValueException {
		assertEquals(s.getBlockedDays(), 30);
	}

}
