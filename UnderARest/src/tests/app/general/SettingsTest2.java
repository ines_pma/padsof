package tests.app.general;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import app.general.Settings;
import app.general.exceptions.InvalidValueException;

public class SettingsTest2 {

	static Settings s;

	@BeforeClass
	public static void before() throws IOException, InvalidValueException {
		s = Settings.getSettingsInstance();
	}

	@Test(expected = InvalidValueException.class)
	public void invalidValue() throws IOException, InvalidValueException {
		s.setSettingsDataFile("txt/settingstest.txt");
	}

}
