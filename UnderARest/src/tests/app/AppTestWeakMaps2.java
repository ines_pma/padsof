package tests.app;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;

import org.junit.Test;

import app.App;
import app.notification.ValidatedNotif;
import app.users.RegUser;

public class AppTestWeakMaps2 {

	@Test
	public void test() throws Exception {
		App.getAppInstance();
		App.setEmptyDataFiles("data/userstest.ser", "data/albumstest.ser", "data/songstest.ser");
		RegUser u = new RegUser("Test1", "Test1", "Test1", LocalDate.MIN);
		u.uploadSong("Song1", "mp3/1.mp3");
		App.addUser(u);
		App.getUsers().get(0L).addNotification(new ValidatedNotif(App.getSongs().get(0L)));
		assertNotNull(App.getAdmin().getNotifications().iterator().next().getSong());
		App.deleteSong(App.getSongs().get(0L));
		System.gc();
		assertNull(App.getAdmin().getNotifications().iterator().next().getSong());
		App.turnOffApp();
		App.reloadData();

	}

}
