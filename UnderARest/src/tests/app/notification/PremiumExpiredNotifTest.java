package tests.app.notification;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import app.App;
import app.media.Song;
import app.media.exceptions.FileTooLongException;
import app.notification.PremiumExpiredNotif;
import app.users.RegUser;
import app.users.exceptions.NotBornException;

public class PremiumExpiredNotifTest {

	static RegUser u;
	static Song s;

	@BeforeClass
	public static void before() throws FileNotFoundException, FileTooLongException, NotBornException {
		u = new RegUser("Test", "Test", "Test", LocalDate.MIN);
		App.getAppInstance();
	}

	@Test
	public void creationAndAdition() {
		PremiumExpiredNotif n = new PremiumExpiredNotif();
		assertNotNull(n);
		u.addNotification(n);
		assertTrue(u.getNotifications().contains(n));
	}

	@Test
	public void isXTest() {
		PremiumExpiredNotif n = new PremiumExpiredNotif();
		assertFalse(n.isNotValidatedNotif());
		assertFalse(n.isReportedNotif());
		assertFalse(n.isSongRelated());
		assertFalse(n.isUploadNotif());
		assertFalse(n.isValidatedNotif());
	}

}