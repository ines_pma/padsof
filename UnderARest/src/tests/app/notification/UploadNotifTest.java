package tests.app.notification;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import app.App;
import app.media.Song;
import app.media.exceptions.FileTooLongException;
import app.notification.UploadNotif;
import app.users.RegUser;
import app.users.exceptions.NotBornException;

public class UploadNotifTest {

	static RegUser u;
	static RegUser author;
	static Song s;

	@BeforeClass
	public static void before() throws FileNotFoundException, FileTooLongException, NotBornException {
		u = new RegUser("Test", "Test", "Test", LocalDate.MIN);
		author = new RegUser("TestAuthor", "TestAuthor", "TestAuthor", LocalDate.MIN);
		s = new Song("TestSong", "mp3/chicle3.mp3", author);
		App.getAppInstance();
	}

	@Test
	public void creationAndAdition() {
		UploadNotif n = new UploadNotif(s);
		assertNotNull(n);
		u.addNotification(n);
		assertTrue(u.getNotifications().contains(n));
	}

	public void isXTest() {
		UploadNotif n = new UploadNotif(s);
		assertFalse(n.isNotValidatedNotif());
		assertFalse(n.isReportedNotif());
		assertTrue(n.isSongRelated());
		assertTrue(n.isUploadNotif());
		assertFalse(n.isValidatedNotif());
	}

}