package tests.app.notification;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import app.media.Song;
import app.media.exceptions.FileTooLongException;
import app.notification.ValidationNotif;
import app.users.Admin;
import app.users.RegUser;
import app.users.exceptions.ExistingNotificationException;
import app.users.exceptions.NotBornException;

public class ValidationNotifTest {

	static Admin a;
	static RegUser author;
	static Song song;

	@BeforeClass
	public static void before() throws FileNotFoundException, FileTooLongException, NotBornException {
		a = Admin.getAdminInstance();
		author = new RegUser("Author", "Author", "Author", LocalDate.MIN);
		song = new Song("TestSong", "mp3/chicle3.mp3", author);
	}

	@Test
	public void test() throws ExistingNotificationException {
		ValidationNotif n = new ValidationNotif(song);
		assertNotNull(n);
		a.addNotification(n);
		assertTrue(a.getNotifications().contains(n));
	}

	public void isXTest() {
		ValidationNotif n = new ValidationNotif(song);
		assertFalse(n.isReportNotif());
		assertTrue(n.isValidationNotif());
	}

}