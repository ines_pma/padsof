package tests.app.notification;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import app.App;
import app.general.exceptions.InvalidValueException;
import app.media.Song;
import app.media.exceptions.FileTooLongException;
import app.notification.NotValidatedNotif;
import app.users.RegUser;
import app.users.exceptions.NotBornException;

public class NotValidatedNotifTest {

	static RegUser u;
	static Song s;

	@BeforeClass
	public static void before()
			throws FileNotFoundException, FileTooLongException, NotBornException, InvalidValueException {
		u = new RegUser("Test", "Test", "Test", LocalDate.MIN);
		s = new Song("TestSong", "mp3/chicle3.mp3", u);
		App.getAppInstance();
	}

	@Test
	public void creationAndAdition() {
		NotValidatedNotif n = new NotValidatedNotif(s);
		assertNotNull(n);
		u.addNotification(n);
		assertTrue(u.getNotifications().contains(n));
	}

	@Test
	public void isXTest() {
		NotValidatedNotif n = new NotValidatedNotif(s);
		assertTrue(n.isNotValidatedNotif());
		assertFalse(n.isReportedNotif());
		assertTrue(n.isSongRelated());
		assertFalse(n.isUploadNotif());
		assertFalse(n.isValidatedNotif());
	}
}
