package tests.app.notification;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import app.App;
import app.media.Song;
import app.media.exceptions.FileTooLongException;
import app.notification.ReportedNotif;
import app.users.RegUser;
import app.users.exceptions.NotBornException;

public class ReportedNotifTest {

	static RegUser u;
	static Song s;

	@BeforeClass
	public static void before() throws FileNotFoundException, FileTooLongException, NotBornException {
		u = new RegUser("Test", "Test", "Test", LocalDate.MIN);
		s = new Song("TestSong", "mp3/chicle3.mp3", u);
		App.getAppInstance();
	}

	@Test
	public void creationAndAdition() {
		ReportedNotif n = new ReportedNotif(s);
		assertNotNull(n);
		u.addNotification(n);
		assertTrue(u.getNotifications().contains(n));
	}

	@Test
	public void isXTest() {
		ReportedNotif n = new ReportedNotif(s);
		assertFalse(n.isNotValidatedNotif());
		assertTrue(n.isReportedNotif());
		assertTrue(n.isSongRelated());
		assertFalse(n.isUploadNotif());
		assertFalse(n.isValidatedNotif());
	}
}
