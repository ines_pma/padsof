package tests.app.notification;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import app.App;
import app.media.Song;
import app.media.exceptions.FileTooLongException;
import app.notification.NotValidatedNotif;
import app.notification.ValidatedNotif;
import app.users.RegUser;
import app.users.exceptions.NotBornException;

public class ValidatedNotifTest {

	static RegUser u;
	static Song s;

	@BeforeClass
	public static void before() throws FileNotFoundException, FileTooLongException, NotBornException {
		u = new RegUser("Test", "Test", "Test", LocalDate.MIN);
		s = new Song("TestSong", "mp3/chicle3.mp3", u);
		App.getAppInstance();
	}

	@Test
	public void creationAndAdition() {
		NotValidatedNotif n = new NotValidatedNotif(s);
		assertNotNull(n);
		u.addNotification(n);
		assertTrue(u.getNotifications().contains(n));
	}

	public void isXTest() {
		ValidatedNotif n = new ValidatedNotif(s);
		assertFalse(n.isNotValidatedNotif());
		assertFalse(n.isReportedNotif());
		assertTrue(n.isSongRelated());
		assertFalse(n.isUploadNotif());
		assertTrue(n.isValidatedNotif());
	}
}