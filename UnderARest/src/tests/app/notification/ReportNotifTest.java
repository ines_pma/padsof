package tests.app.notification;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import app.media.Song;
import app.media.exceptions.FileTooLongException;
import app.notification.ReportNotif;
import app.users.Admin;
import app.users.RegUser;
import app.users.exceptions.ExistingNotificationException;
import app.users.exceptions.NotBornException;

public class ReportNotifTest {

	static Admin a;
	static RegUser reporter;
	static RegUser author;
	static Song song;

	@BeforeClass
	public static void before() throws FileNotFoundException, FileTooLongException, NotBornException {
		a = Admin.getAdminInstance();
		reporter = new RegUser("Reporter", "Reporter", "Reported", LocalDate.MIN);
		author = new RegUser("Author", "Author", "Author", LocalDate.MIN);
		song = new Song("TestSong", "mp3/chicle3.mp3", author);
	}

	@Test
	public void test() throws ExistingNotificationException {
		ReportNotif n = new ReportNotif(song, reporter, "This song is a plagiarism");
		assertNotNull(n);
		a.addNotification(n);
		assertTrue(a.getNotifications().contains(n));
	}

	public void isXTest() {
		ReportNotif n = new ReportNotif(song, reporter, "This song is a plagiarism");
		assertTrue(n.isReportNotif());
		assertFalse(n.isValidationNotif());
	}

}
