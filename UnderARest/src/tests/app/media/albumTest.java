package tests.app.media;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import app.App;
import app.media.Album;
import app.media.Song;
import app.media.exceptions.DuplicatedSongException;
import app.media.exceptions.EmptyException;
import app.media.exceptions.FileTooLongException;
import app.users.RegUser;

public class albumTest {
	static private Album a;
	static private Song s1;
	static private Song s2;
	static private RegUser user;

	@BeforeClass
	public static void setUp() throws DuplicatedSongException, FileNotFoundException, FileTooLongException {
		App.getAppInstance();
		a = new Album("a", 1000);
		s1 = new Song("asd", "mp3/1.mp3", user);
		s2 = new Song("asd", "mp3/chicle3.mp3", user);
		a.addSong(s1);
		App.addAlbum(a);
		App.addSong(s1);
		App.addSong(s2);
	}

	@Test(expected = DuplicatedSongException.class)
	public void testAddSong1() throws DuplicatedSongException {
		a.addSong(s1);
	}

	@Test
	public void testAddSong2() throws DuplicatedSongException, EmptyException {
		a.addSong(s2);
		assertEquals(a.getDuration(), s1.getDuration() + s2.getDuration(), 0.001);
		a.deleteMusic(s2);
		a.containsMusic(s2);
		assertEquals(a.getDuration(), s1.getDuration(), 0.001);
	}

	@Test
	public void explicitTest() throws FileNotFoundException, FileTooLongException, DuplicatedSongException {
		Song s3 = new Song("asd", "mp3/chicle3.mp3", user);
		Song s4 = new Song("asd", "mp3/chicle3.mp3", user);
		s3.setExplicit(true);
		s4.setExplicit(true);
		a.addSong(s3);
		assertTrue(a.getExplicit());
		a.addSong(s4);
		assertTrue(a.getExplicit());
		a.deleteMusic(s3);
		assertTrue(a.getExplicit());
		a.deleteMusic(s4);
		assertFalse(a.getExplicit());
	}

	@Test(expected = DuplicatedSongException.class)
	public void containsMusic() throws DuplicatedSongException {
		a.containsMusic(a);
	}

	@AfterClass
	public static void delete1() throws DuplicatedSongException, InterruptedException {
		a.delete();
		s1.containedMusic(a);
	}
}
