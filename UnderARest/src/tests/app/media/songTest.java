package tests.app.media;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import app.media.Song;
import app.media.SongState;
import app.media.exceptions.DeleteException;
import app.media.exceptions.DuplicatedSongException;
import app.media.exceptions.FileTooLongException;
import app.users.RegUser;
import app.users.exceptions.NotBornException;

public class songTest {
	private Song s;
	private Song s1;
	private RegUser user;

	@Before
	public void SetUp() throws FileNotFoundException, FileTooLongException, NotBornException {
		user = new RegUser("holi", "hola", "caracola", LocalDate.now().minusDays(30));
		s = new Song("Nombre", "mp3/1.mp3", user);
		s1 = new Song("2", "mp3/elCantarDeLaLunaOscura.mp3", user);
	}

	@Test(expected = FileNotFoundException.class)
	public void testConstructor1() throws FileNotFoundException, FileTooLongException {
		new Song("Nombre", "as.mp3", user);
	}

	@Test(expected = FileTooLongException.class)
	public void testConstructor2() throws FileNotFoundException, FileTooLongException {
		new Song("Nombre", "mp3/long.mp3", user);
	}

	@Test
	public void testChangeRight() throws FileTooLongException, IOException {
		s.change("asd", "mp3/elCantarDeLaLunaOscura.mp3");
	}

	@Test(expected = FileNotFoundException.class)
	public void testChangeWrong() throws FileTooLongException, IOException {
		s.change("asd", "elCantarDeLaLunaOscura.mp3");
		assertEquals(s.getFile(), "mp3/elCantarDeLaLunaOscura.mp3");
	}

	@Test(expected = DeleteException.class)
	public void testCheckDateValidation1() throws DeleteException {
		s.setTimeValid(LocalDate.now().minusDays(4));
		s.setSongState(SongState.WITH_ERRORS);
		// assertEquals(s.getTimeValid(), LocalDate.now().minusDays(4));
		s.checkDateValidation();
	}

	@Test
	public void testCheckDateValidation2() throws DeleteException {
		s.setTimeValid(LocalDate.now().minusDays(4));
		s.setSongState(SongState.VALIDATED);
		// s.setSongState(SongState.WITH_ERRORS);
		s.checkDateValidation();
	}

	@Test
	public void testCheckDateValidation3() throws DeleteException {
		s.setSongState(SongState.WITH_ERRORS);
		s.checkDateValidation();
	}

	@Test
	public void testContainsMusic1() throws DuplicatedSongException {
		s.containsMusic(s1);
	}

	@Test(expected = DuplicatedSongException.class)
	public void testContainsMusic2() throws DuplicatedSongException {
		s.containsMusic(s);
	}

	@Test
	public void testGetSongs() {
		Set<Song> set = new HashSet<Song>();
		set.add(s);
		assertEquals(set, s.getSongs());
	}

}
