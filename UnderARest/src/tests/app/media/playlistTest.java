package tests.app.media;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import app.App;
import app.media.Album;
import app.media.Playlist;
import app.media.Song;
import app.media.exceptions.DuplicatedSongException;
import app.media.exceptions.EmptyException;
import app.media.exceptions.FileTooLongException;
import app.users.RegUser;
import app.users.exceptions.InvalidTitleException;
import app.users.exceptions.NotBornException;

public class playlistTest {
	App app;
	static Playlist p;
	static Song s1;
	static Album a;
	RegUser user;

	@Before
	public void SetUp() throws DuplicatedSongException, FileNotFoundException, FileTooLongException, NotBornException,
			InvalidTitleException {
		app = App.getAppInstance();
		a = new Album("asd", 1398);
		s1 = new Song("2", "mp3/elCantarDeLaLunaOscura.mp3", user);
		user = new RegUser("asd", "asd", "asd", LocalDate.now().minusDays(10));
		user.createPlaylist("asd", s1);
		p = user.getPlaylists().iterator().next();
		a.addSong(s1);
	}

	@Test(expected = DuplicatedSongException.class)
	public void testAdd1() throws DuplicatedSongException {
		p.addMusic(a);
	}

	@Test
	public void testAdd2() throws DuplicatedSongException, EmptyException, FileNotFoundException, FileTooLongException {
		Song s2 = new Song("asd", "mp3/elCantarDeLaLunaOscura.mp3", user);
		p.addMusic(s2);
		p.deleteMusic(s1);
		assertEquals(p.getDuration(), s2.getDuration(), 0.001);
		p.addMusic(a);
		assertEquals(p.getDuration(), s1.getDuration() + s2.getDuration(), 0.001);
	}

	@Test
	public void testExplicit1() throws FileNotFoundException, FileTooLongException, DuplicatedSongException {
		Song s3 = new Song("asd", "mp3/chicle3.mp3", user);
		s3.setExplicit(true);
		p.addMusic(s3);
		assertTrue(p.getExplicit());
		p.deleteMusic(s3);
		assertFalse(p.getExplicit());
	}

	@AfterClass
	public static void delete1() throws DuplicatedSongException, InterruptedException {
		p.deleteMusic(s1);
		p.containedMusic(s1);
	}
}
