package tests.app.media;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import app.App;
import app.media.Album;
import app.media.Playlist;
import app.media.Song;
import app.media.exceptions.DuplicatedSongException;
import app.media.exceptions.FileTooLongException;
import app.users.RegUser;
import app.users.exceptions.NotBornException;
import pads.musicPlayer.exceptions.Mp3PlayerException;

public class musicTest {
	private static Song s1, s2, s3;
	private static RegUser user;
	private static Album a;
	private static Playlist p;

	@Before
	public void SetUp() throws FileNotFoundException, FileTooLongException, DuplicatedSongException, NotBornException {
		user = new RegUser("asd", "asd", "asd", LocalDate.now().minusYears(20));
		s1 = new Song("Nombre", "mp3/1.mp3", user);
		s2 = new Song("2", "mp3/elCantarDeLaLunaOscura.mp3", user);
		s3 = new Song("3", "mp3/hive.mp3", user);
		a = new Album("A", 2010);
		a.addSong(s1);
		p = new Playlist("p", user);
		p.addMusic(s2);
		App.getAppInstance();
		App.addUser(user);
		App.getAppInstance().setActualUser(user);
	}

	@Test
	public void testPlaySong1() throws FileNotFoundException, Mp3PlayerException, InterruptedException {
		s1.play();
		Thread.sleep(5000);
		assertTrue(s1.getPlaying());
		s1.stop();
		assertFalse(s1.getPlaying());
	}

	@Test
	public void testPlaySong2() throws FileNotFoundException, Mp3PlayerException, InterruptedException {
		s3.play();
		Thread.sleep((int) (s3.getDuration() * 1000) + 20);
		assertFalse(s3.getPlaying());
	}

	@Test
	public void testPlaySong3()
			throws FileNotFoundException, Mp3PlayerException, InterruptedException, NotBornException {
		RegUser user2 = new RegUser("fgh", "asd", "asd", LocalDate.now().minusYears(20));
		App.getAppInstance().setActualUser(user2);
		user2.setPlays(3000);
		s1.play();
		assertFalse(s1.getPlaying());
	}

	@Test
	public void testPlaySong4()
			throws FileNotFoundException, Mp3PlayerException, InterruptedException, NotBornException {
		RegUser user2 = new RegUser("fgh", "asd", "asd", LocalDate.now().minusYears(20));
		App.getAppInstance().setActualUser(user2);
		LocalDate now = LocalDate.now();
		user.setExpDate(LocalDate.now().minusMonths(2));
		s1.play();
		Thread.sleep(1000);
		assertFalse(user.getExpDate().isBefore(now));
	}

	@Test
	public void testPlayAlbum() throws FileNotFoundException, Mp3PlayerException, InterruptedException {
		a.play();
		Thread.sleep(5000);
		assertTrue(s1.getPlaying());
		assertTrue(a.getPlaying());
		a.stop();
		Thread.sleep(10);
		assertFalse(s1.getPlaying());
		assertFalse(a.getPlaying());
	}

	@Test
	public void testPlayPlaylist() throws FileNotFoundException, Mp3PlayerException, InterruptedException {
		p.play();
		Thread.sleep(5000);
		assertTrue(s2.getPlaying());
		assertTrue(p.getPlaying());
		p.stop();
		Thread.sleep(100);
		assertFalse(s2.getPlaying());
		assertFalse(p.getPlaying());
	}

	@Test
	public void testPlay2() throws FileNotFoundException, Mp3PlayerException, InterruptedException {
		p.play();
		Thread.sleep(6000);
		a.play();
	}

	@Test
	public void testgetAuthor() {
		assertEquals(s1.getAuthor(), user);
		assertEquals(a.getAuthor(), user);
		assertEquals(p.getAuthor(), user);
	}

	@Test
	public void testContainsMusic1() throws DuplicatedSongException {
		s1.containsMusic(s2);
		a.containsMusic(s2);
		p.containsMusic(a);
		p.containsMusic(s1);
	}

	@Test(expected = DuplicatedSongException.class)
	public void testContainsMusic2() throws DuplicatedSongException {
		Playlist p2 = new Playlist("p2", user);
		p2.addMusic(a);
		p2.containsMusic(s1);
	}

	@Test(expected = DuplicatedSongException.class)
	public void testContainsMusic3() throws DuplicatedSongException {
		a.containsMusic(s1);
	}

	@Test(expected = DuplicatedSongException.class)
	public void testContainsMusic4() throws DuplicatedSongException {
		p.addMusic(a);
		p.containsMusic(s2);
	}

	@Test(expected = DuplicatedSongException.class)
	public void testContainedMusic1() throws DuplicatedSongException {
		Playlist p2 = new Playlist("p2", user);
		p2.addMusic(p);
		p.addMusic(a);
		p2.containedMusic(s2);
	}

	@Test
	public void testContainedMusic2() throws DuplicatedSongException {
		a.containedMusic(p);
		a.containedMusic(s2);
	}

}
