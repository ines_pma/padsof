/**
 * 
 */
package tests.app.users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import app.App;
import app.media.Song;
import app.media.SongState;
import app.media.exceptions.FileTooLongException;
import app.notification.AdminNotif;
import app.notification.ReportNotif;
import app.notification.UserNotif;
import app.notification.ValidationNotif;
import app.users.Admin;
import app.users.RegUser;
import app.users.UserState;
import app.users.exceptions.ExistingNotificationException;
import app.users.exceptions.InvalidTitleException;

public class AdminTest {

	private static RegUser u1, u2;
	private static Song s1, s2;
	private static Admin admin;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUp() throws Exception {
		App.getAppInstance();
		App.setEmptyDataFiles("data/userstest.ser", "data/albumstest.ser", "data/songstest.ser");
		admin = App.getAdmin();
		admin.getNotificationsTest().clear();
		u2 = new RegUser("valira", "pswd", "Valira", LocalDate.of(1980, 1, 11));
		u1 = new RegUser("kesha", "pswd", "Ke$ha", LocalDate.of(1980, 1, 11));
		s1 = u1.uploadSong("Tik Tok", "mp3/1.mp3");
		s2 = new Song("million reasons", "mp3/chicle3.mp3", u1);
	}

	/**
	 * Test method for {@link app.users.Admin#getAdminInstance()}.
	 */
	@Test
	public void testGetAdminInstance() {
		assertNotNull(admin);
	}

	/**
	 * Test method for
	 * {@link app.users.Admin#addNotification(app.notification.AdminNotif)}.
	 * 
	 * @throws ExistingNotificationException
	 */
	@Test
	public void testAddNotification() throws ExistingNotificationException {
		ValidationNotif v = new ValidationNotif(s2);
		admin.addNotification(v);
		assertEquals(admin.getNotifications().size(), 2);
		admin.removeNotification(v);
	}

	/**
	 * Test method for
	 * {@link app.users.Admin#addNotification(app.notification.AdminNotif)}.
	 * 
	 * @throws ExistingNotificationException
	 */
	@Test(expected = ExistingNotificationException.class)
	public void testAddNotificationExistingNotificationException() throws ExistingNotificationException {
		admin.addNotification(new ValidationNotif(s1));
		admin.addNotification(new ValidationNotif(s1));
	}

	/**
	 * Test method for
	 * {@link app.users.Admin#accept(app.notification.ValidationNotif, java.lang.Boolean)}.
	 * 
	 * @throws ExistingNotificationException
	 * @throws InvalidTitleException
	 * @throws FileTooLongException
	 * @throws FileNotFoundException
	 */
	@Test
	public void testAcceptValidationNotifExplicit()
			throws Exception {
		Song s = null;
		RegUser author = null;
		s1 = u1.uploadSong("Eso es queso", "mp3/1.mp3");
		Set<AdminNotif> ns = admin.getNotifications();
		for (AdminNotif n : ns) {
			s = n.getSong();
			assertEquals(s1, s);
			author = s.getAuthor();
			if (n.isValidationNotif()) {
				admin.accept((ValidationNotif) n, true);
				break;
			}
		}
		assertTrue(author.getSongs().contains(s));
		assertEquals(s.getSongState(), SongState.VALIDATED);
		for (UserNotif n : author.getNotifications()) {
			if (n.getSong() == s) {
				assertEquals(n.getSong(), s);
				return;
			}
		}
		assertTrue(false);
	}

	/**
	 * Test method for
	 * {@link app.users.Admin#decline(app.notification.ValidationNotif)}.
	 * 
	 * @throws ExistingNotificationException
	 * @throws InvalidTitleException
	 * @throws FileTooLongException
	 * @throws FileNotFoundException
	 */
	@Test
	public void testDeclineValidationNotif()
			throws Exception {
		RegUser u = null;
		Song s = null;
		admin.getNotificationsTest().clear();
		u1.getNotificationsTest().clear();
		u1.uploadSong("Efe Efe", "mp3/1.mp3");
		for (AdminNotif n : admin.getNotifications()) {
			if (n.isValidationNotif()) {
				s = n.getSong();
				u = s.getAuthor();
				admin.decline((ValidationNotif) n);
				break;
			}
		}
		assertEquals(s.getSongState(), SongState.WITH_ERRORS);
		assertEquals(admin.getNotifications().size(), 0);
		for (UserNotif n : u.getNotifications()) {
			assertEquals(n.getSong(), s);
		}
	}

	/**
	 * Test method for {@link app.users.Admin#accept(app.notification.ReportNotif)}.
	 * 
	 * @throws ExistingNotificationException
	 */
	@Test
	public void testAcceptReportNotif() throws ExistingNotificationException {
		RegUser reporter = null;
		RegUser reported = null;
		Song s = s2;
		admin.getNotificationsTest().clear();
		u2.report(s1, "Horrible copied song");
		for (AdminNotif n : admin.getNotifications()) {
			if (n.isReportNotif()) {
				s = n.getSong();
				reported = s.getAuthor();
				reporter = ((ReportNotif) n).getReporter();
				admin.accept((ReportNotif) n);
				break;
			}
		}
		assertEquals(0, admin.getNotifications().size());
		assertEquals(reporter, u2);
		assertEquals(reported.getUserState(), UserState.BLOCKED_FOREVER);
	}

	/**
	 * Test method for
	 * {@link app.users.Admin#decline(app.notification.ReportNotif)}.
	 * 
	 * @throws ExistingNotificationException
	 */
	@Test
	public void testDeclineReportNotif() throws ExistingNotificationException {
		RegUser reporter = null;
		Song s = s2;
		admin.getNotificationsTest().clear();
		s2.getAuthor().getNotificationsTest().clear();

		u1.report(s2, "Horrible copied song");
		assertEquals(1, s2.getAuthor().getNotifications().size());
		for (AdminNotif n : admin.getNotifications()) {
			if (n.isReportNotif()) {
				s = n.getSong();
				reporter = ((ReportNotif) n).getReporter();
				admin.decline((ReportNotif) n);
				break;
			}
		}
		assertEquals(0, s2.getAuthor().getNotifications().size());
		assertEquals(0, admin.getNotifications().size());
		assertEquals(reporter, u1);
		assertEquals(reporter.getUserState(), UserState.BLOCKED);
		assertEquals(s.getSongState(), SongState.VALIDATED);
		assertEquals(reporter.getExpDate(), LocalDate.now().plusDays(App.getSettings().getBlockedDays()));
	}

}
