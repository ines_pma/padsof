/**
 * 
 */
package tests.app.users;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import app.App;
import app.users.RegUser;
import app.users.User;

public class UserTest {

	/**
	 * Test method for {@link app.users.User#increasePlays()}.
	 */
	@Test
	public void testIncreasePlays() throws Exception {
		RegUser u1 = new RegUser("hi", "hi", "hi", LocalDate.now().minusDays(30));
		User u2 = new User();
		int plays = u2.getPlays();
		u2.increasePlays();
		assertEquals(plays + 1, u2.getPlays());
		u2.setPlays(App.getSettings().getNumSongAnon() + 1);
		plays = u2.getPlays();
		u2.increasePlays();
		assertEquals(plays, App.getSettings().getNumSongAnon() + 1);
		u1.setPlays(App.getSettings().getNumSongReg() + 1);
		plays = u1.getPlays();
		u1.increasePlays();
		assertEquals(plays, App.getSettings().getNumSongReg() + 1);
		plays = App.getAdmin().getPlays();
		App.getAdmin().increasePlays();
		assertEquals(plays + 1, App.getAdmin().getPlays());
	}

}
