package tests.app.users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import app.App;
import app.media.Album;
import app.media.Playlist;
import app.media.Song;
import app.media.SongState;
import app.media.exceptions.DuplicatedSongException;
import app.media.exceptions.FileTooLongException;
import app.notification.AdminNotif;
import app.notification.NotValidatedNotif;
import app.notification.ReportNotif;
import app.notification.ReportedNotif;
import app.notification.UserNotif;
import app.notification.ValidationNotif;
import app.users.RegUser;
import app.users.UserState;
//import org.hamcrest.CoreMatchers.*;
import app.users.exceptions.ExistingNotificationException;
import app.users.exceptions.InvalidTitleException;
import app.users.exceptions.NotBornException;
import app.users.exceptions.NotChangeableSongException;
import app.users.exceptions.NotYourMusicException;

public class RegUserTest {
	private RegUser u1, u2;
	private Album a;
	private Song s1, s2, s3;
	private Set<Song> songsSet = new HashSet<>();
	private Playlist p;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		App.getAppInstance();
		App.getSettings().setUnderage(18);
		App.setEmptyDataFiles("data/userstest.ser", "data/albumstest.ser", "data/songstest.ser");
		App.getAdmin().getNotificationsTest().clear();
		u1 = new RegUser("old", "pswd", "Valira", LocalDate.of(1999, 1, 17));
		u2 = new RegUser("young", "pswd", "XxxTentacion", LocalDate.of(2019, 1, 17));

		s1 = new Song("there", "mp3/1.mp3", u1);
		u1.addToSongs(s1);
		s2 = new Song("2", "mp3/elCantarDeLaLunaOscura.mp3", u1);
		u1.addToSongs(s2);
		songsSet.add(s1);
		songsSet.add(s2);
		s3 = new Song("2", "mp3/1.mp3", u2);

		a = new Album("A", 2010);
		a.addSong(s1);

		p = new Playlist("p", u1);
		p.addMusic(s2);
		p.addMusic(s1);
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#RegUser(java.lang.String, java.lang.String, java.lang.String, java.time.LocalDate)}.
	 *
	 * @throws NotBornException
	 */
	@Test(expected = NotBornException.class)
	public void testRegUserNotBornException() throws NotBornException {
		new RegUser("h", "h", "h", LocalDate.now().plusDays(1));
	}

	/**
	 * Test method for {@link app.users.RegUser#adultifier()}.
	 */
	@Test
	public void testAdultifier() {
		assertFalse(u1.isUnderage());
		assertTrue(u2.isUnderage());
	}

	/**
	 * Test method for {@link app.users.RegUser#update()}.
	 */
	@Test
	public void testUpdate1() {
		u1.setExpDate(LocalDate.now().minusDays(1));
		u1.setUserState(UserState.BLOCKED);
		u1.update();
		assertEquals(u1.getUserState(), UserState.REGISTERED);
		assertEquals(u1.getExpDate(), LocalDate.now().plusDays(30));
	}

	/**
	 * Test method for {@link app.users.RegUser#update()}.
	 */
	@Test
	public void testUpdate2() {
		u1.setExpDate(LocalDate.now().minusDays(1));
		u1.setUserState(UserState.REGISTERED);
		u1.setListened(App.getSettings().getRepUpgrade() + 1);
		u1.update();
		assertEquals(u1.getUserState(), UserState.PREMIUM);
	}

	/**
	 * Test method for {@link app.users.RegUser#update()}.
	 */
	@Test
	public void testUpdate3() {
		u1.getNotificationsTest().clear();
		u1.setExpDate(LocalDate.now().minusDays(1));
		u1.setUserState(UserState.PREMIUM);
		u1.update();
		assertEquals(u1.getUserState(), UserState.REGISTERED);
		assertEquals(u1.getNotifications().size(), 1);
	}

	/**
	 * Test method for {@link app.users.RegUser#update()}.
	 */
	@Test
	public void testUpdate4() {
		u1.setExpDate(LocalDate.now().minusDays(1));
		u1.update();
		assertEquals(u1.getUserState(), UserState.REGISTERED);
	}

	/**
	 * Test method for {@link app.users.RegUser#updateExpDate()}.
	 */

	@Test
	public void testUpdateExpDate() {
		LocalDate expDate1 = u1.getExpDate();
		u1.updateExpDate();
		assertEquals(u1.getListened(), 0);
		assertEquals(u1.getPlays(), 0);
		assertEquals(expDate1.plusDays(30), u1.getExpDate());
		u2.setUserState(UserState.BLOCKED_FOREVER);
		u2.updateExpDate();
		assertEquals(u2.getExpDate(), LocalDate.MAX);
	}

	/**
	 * Test method for {@link app.users.RegUser#follow(app.users.RegUser)}.
	 */
	@Test
	public void testFollow() {
		u1.follow(u2);
		assertTrue(u1.getFollowing().contains(u2));
		assertTrue(u2.getFollowers().contains(u1));
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#report(app.media.Song, java.lang.String)}.
	 *
	 * @throws ExistingNotificationException
	 */
	@Test
	public void testReport() throws ExistingNotificationException {
		u1.report(s2, "Report");
		assertEquals(s2.getSongState(), SongState.REPORTED);

		Set<AdminNotif> ans = App.getAdmin().getNotifications();
		for (AdminNotif n : ans) {
			if (n.isReportNotif()) {
				assertEquals(n.getSong(), s2);
				assertEquals(((ReportNotif) n).getReporter(), u1);
			}
		}
		Set<UserNotif> uns = s2.getAuthor().getNotifications();
		for (UserNotif n : uns) {
			assertEquals(((ReportedNotif) n).getSong(), s2);
		}
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#createPlaylist(java.lang.String, app.media.Music)}.
	 *
	 * @throws InvalidTitleException
	 * @throws DuplicatedSongException
	 */
	@Test
	public void testCreatePlaylist() throws InvalidTitleException, DuplicatedSongException {
		u1.createPlaylist("playlist", this.p);
		u1.createPlaylist("p2", this.a);
		assertEquals(u1.getPlaylists().size(), 2);
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#createPlaylist(java.lang.String, app.media.Music)}.
	 *
	 * @throws InvalidTitleException
	 * @throws DuplicatedSongException
	 */
	@Test(expected = InvalidTitleException.class)
	public void testCreatePlaylistInvalidTitleException() throws InvalidTitleException, DuplicatedSongException {
		u1.createPlaylist("playlist", this.p);
		u1.createPlaylist("playlist", this.a);
	}

	/**
	 * Test method for {@link app.users.RegUser#removePlaylist(app.media.Playlist)}.
	 *
	 * @throws InvalidTitleException
	 * @throws DuplicatedSongException
	 */
	@Test
	public void testRemovePlaylist() throws InvalidTitleException, DuplicatedSongException {
		u1.createPlaylist("playlist", this.p);
		assertEquals(u1.getPlaylists().size(), 1);
		for (Playlist p : u1.getPlaylists()) {
			u1.removePlaylist(p);
		}
		assertEquals(u1.getPlaylists().size(), 0);
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#createAlbum(java.lang.String, int, java.util.Set)}.
	 *
	 * @throws NotYourMusicException
	 * @throws InvalidTitleException
	 * @throws DuplicatedSongException
	 */
	@Test
	public void testCreateAlbum() throws InvalidTitleException, NotYourMusicException, DuplicatedSongException {
		u1.createAlbum("hithere", 2017, this.songsSet);
		assertEquals(u1.getAlbums().size(), 1);
		for (Album a : u1.getAlbums()) {
			assertTrue(a.getSongs().contains(s1));
			assertTrue(a.getSongs().contains(s2));
		}
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#createAlbum(java.lang.String, int, java.util.Set)}.
	 *
	 * @throws InvalidTitleException
	 * @throws NotYourMusicException
	 * @throws DuplicatedSongException
	 */
	@Test(expected = InvalidTitleException.class)
	public void testCreateAlbumInvalidTitleException()
			throws InvalidTitleException, NotYourMusicException, DuplicatedSongException {
		u1.createAlbum("MINT", 2017, this.songsSet);
		u1.createAlbum("MINT", 2017, this.songsSet);
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#createAlbum(java.lang.String, int, java.util.Set)}.
	 *
	 * @throws InvalidTitleException
	 * @throws NotYourMusicException
	 * @throws DuplicatedSongException
	 */
	@Test(expected = NotYourMusicException.class)
	public void testCreateAlbumNotYourMusicException()
			throws InvalidTitleException, NotYourMusicException, DuplicatedSongException {
		this.songsSet.add(s3);
		u1.createAlbum("hi", 2017, this.songsSet);
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#addToAlbum(app.media.Song, app.media.Album)}.
	 *
	 * @throws NotYourMusicException
	 * @throws InvalidTitleException
	 * @throws DuplicatedSongException
	 */
	@Test
	public void testAddToAlbum() throws InvalidTitleException, NotYourMusicException, DuplicatedSongException {
		for (Album a : u1.getAlbums()) {
			u1.addToAlbum(s1, a);
			assertTrue(a.getSongs().contains(s1));
		}
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#uploadSong(java.lang.String, java.lang.String)}.
	 *
	 * @throws InvalidTitleException
	 * @throws FileTooLongException
	 * @throws FileNotFoundException
	 * @throws ExistingNotificationException
	 * @throws NotChangeableSongException
	 */
	@Test
	public void testUploadSong() throws Exception {
		Song s = u1.uploadSong("Guerra Fria", "mp3/1.mp3");
		assertTrue(u1.getSongs().contains(s));
		assertEquals(s.getSongState(), SongState.PENDING_TO_VALIDATE);
		Set<AdminNotif> adminNotifs = App.getAdmin().getNotifications();
		for (AdminNotif a : adminNotifs) {
			assertEquals(a.getSong(), s);
		}
		u1.changeSongData(s, "nombre", "mp3/1.mp3");
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#changeSongData(app.media.Song, java.lang.String, java.lang.String)}.
	 *
	 * @throws NotChangeableSongException
	 * @throws InvalidTitleException
	 * @throws FileNotFoundException
	 * @throws ExistingNotificationException
	 * @throws FileTooLongException
	 */
	@Test
	public void testChangeSongData() throws Exception {
		Song s = null;
		App.getAdmin().getNotificationsTest().clear();
		u1.getNotificationsTest().clear();
		u1.uploadSong("Asereje", "mp3/chicle3.mp3");
		for (AdminNotif an : App.getAdmin().getNotifications()) {
			s = an.getSong();
			App.getAdmin().decline((ValidationNotif) an);
		}
		for (UserNotif un : u1.getNotifications()) {
			assertEquals(s, un.getSong());
		}
		u1.changeSongData(s, "Confident", s1.getFile());
		assertEquals(u1.getNotifications().size(), 0);
		assertEquals(u1.getNotifications().size(), 0);
		assertEquals(App.getAdmin().getNotifications().size(), 1);
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#changeSongData(app.media.Song, java.lang.String, java.lang.String)}.
	 *
	 * @throws NotChangeableSongException
	 * @throws InvalidTitleException
	 * @throws FileNotFoundException
	 * @throws ExistingNotificationException
	 * @throws FileTooLongException
	 */
	@Test(expected = InvalidTitleException.class)
	public void testChangeSongDataInvalidTitle() throws Exception {
		Song s = null;
		App.getAdmin().getNotificationsTest().clear();
		u1.getNotificationsTest().clear();
		u1.uploadSong("Asereje", "mp3/chicle3.mp3");
		for (AdminNotif an : App.getAdmin().getNotifications()) {
			s = an.getSong();
			App.getAdmin().decline((ValidationNotif) an);
		}
		for (UserNotif un : u1.getNotifications()) {
			assertEquals(s, un.getSong());
		}
		u1.changeSongData(s, "there", s1.getFile());
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#changeSongData(app.media.Song, java.lang.String, java.lang.String)}.
	 * @throws Exception 
	 */
	@Test(expected = NotChangeableSongException.class)
	public void testChangeSongDataNotChangeableSongException() throws Exception {
		s1.setSongState(SongState.VALIDATED);
		u1.changeSongData(s1, "Hola", s1.getFile());
	}

	/**
	 * Test method for
	 * {@link app.users.RegUser#validateSongTitle(java.lang.String)}.
	 *
	 * @throws FileTooLongException
	 * @throws FileNotFoundException
	 */
	@Test
	public void testValidateSongTitle() throws FileNotFoundException, FileTooLongException {
		assertFalse(u1.validateSongTitle("there"));
	}

	/**
	 * Test method for {@link app.users.RegUser#reviewSongs()}.
	 * 
	 * @throws FileTooLongException
	 * @throws FileNotFoundException
	 */
	@Test
	public void testReviewSongs() throws FileNotFoundException, FileTooLongException {
		u1.getNotificationsTest().clear();
		assertEquals(2, u1.getSongs().size());
		u1.reviewSongs();
		assertEquals(2, u1.getSongs().size());

		WeakReference<Song> s = new WeakReference<Song>(new Song("Errors", "mp3/1.mp3", u1));

		s.get().setTimeValid(LocalDate.now().minusDays(4));
		s.get().setSongState(SongState.WITH_ERRORS);
		u1.addToSongs(s.get());
		u1.addNotification(new NotValidatedNotif(s.get()));
		assertEquals(1, u1.getNotifications().size());
		u1.reviewSongs();
		assertEquals(0, u1.getNotifications().size());

	}
}
