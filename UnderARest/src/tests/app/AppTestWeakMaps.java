package tests.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import app.App;
import app.users.RegUser;

public class AppTestWeakMaps {

	@Test
	public void test() throws Exception {
		App.getAppInstance();
		App.setEmptyDataFiles("data/userstest.ser", "data/albumstest.ser", "data/songstest.ser");
		RegUser u = new RegUser("User1", "Pass1", "Authorname1", LocalDate.MIN);
		RegUser v = new RegUser("User2", "Pass2", "Authorname2", LocalDate.MIN);
		u.uploadSong("Song1", "mp3/1.mp3");
		v.uploadSong("Song2", "mp3/chicle3.mp3");
		App.addUser(u);
		App.addUser(v);
		assertTrue(App.getUsers().get(0L).getSongs().contains(App.getSongs().get(0L)));
		assertTrue(App.getUsers().get(1L).getSongs().contains(App.getSongs().get(1L)));
		App.turnOffApp();
		App.reloadData();
		assertTrue(App.getUsers().get(0L).getSongs().contains(App.getSongs().get(0L)));
		assertTrue(App.getUsers().get(1L).getSongs().contains(App.getSongs().get(1L)));
		App.deleteSong(App.getUsers().get(0L).getSongs().iterator().next());
		System.gc();
		assertEquals(App.getTotalMusic(), 2);
		assertEquals(App.getSongs().size(), 1);
		assertEquals(App.getSongs().get(1L).getAuthor(), App.getUsers().get(1L));

	}

}
