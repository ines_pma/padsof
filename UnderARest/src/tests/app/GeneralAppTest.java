package tests.app;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

import app.App;
import app.general.Authentication;
import app.media.Playlist;
import app.users.RegUser;

public class GeneralAppTest {

	@Test
	public void generalTest()
			throws Exception {
		App.getAppInstance();
		App.setEmptyDataFiles("data/userstest.ser", "data/albumstest.ser", "data/songstest.ser");
		Authentication.register("User1", "Pass1", "Author1", LocalDate.of(1999, 3, 22));
		Authentication.register("User2", "Pass2", "Author2", LocalDate.of(1999, 5, 12));
		RegUser u1 = App.getUsers().get(0L);
		RegUser u2 = App.getUsers().get(1L);
		u1.uploadSong("million reasons", "mp3/1.mp3");
		u2.uploadSong("despacito", "mp3/chicle3.mp3");
		u1.uploadSong("high hopes", "mp3/highHopes.mp3");
		u2.uploadSong("misery bussiness", "mp3/1.mp3");
		u1.createPlaylist("Playlist1", App.getSongs().get(0L));
		Playlist p1 = (Playlist) u1.getPlaylists().toArray()[0];
		p1.addMusic(App.getSongs().get(1L));
		p1.addMusic(App.getSongs().get(2L));
		u2.createPlaylist("Playlist2", App.getSongs().get(3L));
		Playlist p2 = (Playlist) u2.getPlaylists().toArray()[0];
		p2.addMusic(p1);
		u1.createAlbum("Album1", 2010, u1.getSongs());
		u2.createAlbum("Album2", 2001, u2.getSongs());

		App.turnOffApp();
		App.reloadData();

		assertEquals(App.getSongs().size(), 4);
		assertEquals(App.getUsers().size(), 2);
		assertEquals(App.getAlbums().size(), 2);

		App.deleteAlbum(App.getUsers().get(0L).getAlbums().iterator().next());
		System.gc();
		assertEquals(App.getAlbums().size(), 1);
		// assertEquals(App.getUsers().get(0L).getAlbums().size(),0);
	}

}
