package tests.app;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import app.App;
import app.general.Authentication;
import app.general.Search;
import app.general.exceptions.BlockedUserException;
import app.media.Song;
import app.media.SongState;
import app.notification.AdminNotif;
import app.notification.ReportNotif;
import app.notification.ValidationNotif;
import app.users.RegUser;

public class Simulation {

	public static void main(String args[]) throws Exception {
		App.setEmptyDataFiles("data/users.ser", "data/albums.ser", "data/songs.ser");
		App.getAppInstance();
		App.turnOffApp();
		App.reloadData();
		/*
		 * In this simulation, we will use prints to show the functionality. We just use
		 * them to make it easier to check that it works.
		 */

		Authentication.register("alejandro", "zombor", "bravo", LocalDate.of(2003, 04, 21));
		Authentication.login("alejandro", "zombor");
		RegUser aux = (RegUser) App.getAppInstance().getActualUser();
		aux.uploadSong("take me to church", "mp3/highHopes.mp3");
		aux.uploadSong("sing", "mp3/1.mp3");
		Authentication.logout();

		Authentication.register("carmen", "patata", "dmcarmen", LocalDate.of(1999, 01, 17));
		Authentication.login("carmen", "patata");
		aux = (RegUser) App.getAppInstance().getActualUser();
		aux.createPlaylist("Playlist alejandro", App.getUsers().get(0L).getSongs().iterator().next());
		aux.uploadSong("patata", "mp3/chicle3.mp3");
		Authentication.logout();

		Authentication.login("admin", "admin");
		Set<AdminNotif> notifs = App.getAdmin().getNotificationsTest();
		Set<ValidationNotif> toAccept = new HashSet<>();
		for (AdminNotif a : notifs) {
			if (a.isValidationNotif())
				toAccept.add((ValidationNotif) a);
		}
		for (ValidationNotif v : toAccept)
			App.getAdmin().accept(v, false);
		Authentication.logout();

		Authentication.login("alejandro", "zombor");
		aux = (RegUser) App.getAppInstance().getActualUser();
		Set<Song> s = new HashSet<>();
		s = Search.searchSong("sing");
		App.deleteSong(s.iterator().next());
		Authentication.logout();

		App.turnOffApp();
		App.reloadData();

		Authentication.login("alejandro", "zombor");
		aux = (RegUser) App.getAppInstance().getActualUser();
		aux.uploadSong("king of the clouds", "mp3/1.mp3");
		aux.uploadSong("i miss the misery", "mp3/chicle3.mp3");
		Authentication.logout();

		Authentication.login("admin", "admin");
		notifs = App.getAdmin().getNotificationsTest();
		Set<ValidationNotif> toDecline = new HashSet<>();
		for (AdminNotif a : notifs) {
			if (a.isValidationNotif())
				toDecline.add((ValidationNotif) a);
		}
		for (ValidationNotif v : toDecline)
			App.getAdmin().decline(v);
		Authentication.logout();

		System.out.println("The users that have registered: ");
		for (RegUser u : App.getUsers().values()) {
			System.out.println(u.getAuthorName());
		}

		System.out.println("\nThe songs and its state: ");
		for (Song sng : App.getSongs().values()) {
			System.out.println(sng.getName() + ": " + sng.getSongState());
		}

		Authentication.register("adrian", "lopez", "2lopez", LocalDate.of(1999, 04, 6));
		Authentication.login("adrian", "lopez");
		aux = (RegUser) App.getAppInstance().getActualUser();
		s = Search.searchSong("take");
		System.out.println("\nSearch results: " + s);
		Song playing = s.iterator().next();
		System.out.println(playing.getName() + " is playing.");
		playing.play();
		Thread.sleep(2000);
		playing.stop();
		Set<RegUser> userResults = new HashSet<>();
		userResults = Search.searchUser("dmcar");
		aux.follow(userResults.iterator().next());
		Authentication.logout();

		Authentication.enterAsGuest();
		s = Search.searchSong("pat");
		System.out.println("\nSearch results: " + s);
		playing = s.iterator().next();
		System.out.println(playing.getName() + " is playing.");
		playing.play();
		Thread.sleep(5000);
		playing.stop();
		Authentication.logout();

		Authentication.login("alejandro", "zombor");
		aux = (RegUser) App.getAppInstance().getActualUser();
		Set<Song> forAlbum = new HashSet<>();
		for (Song song : aux.getSongs())
			if (song.getSongState() == SongState.VALIDATED)
				forAlbum.add(song);
		aux.createAlbum("my first album", 2019, forAlbum);
		userResults = Search.searchUser("2lopez");
		aux.follow(userResults.iterator().next());
		Song toChange = null;
		for (Song song : aux.getSongs()) {
			if (song.getSongState() == SongState.WITH_ERRORS) {
				toChange = song;
				break;
			}
		}
		aux.changeSongData(toChange, "not king of the clouds", "mp3/1.mp3");
		s = Search.searchSong("king of the clouds");
		Authentication.logout();

		Authentication.login("carmen", "patata");
		aux = (RegUser) App.getAppInstance().getActualUser();
		s = Search.searchSong("take me");
		aux.report((Song) s.toArray()[0], "Hozier's copy");
		aux.uploadSong("The middle", "mp3/1.mp3");
		Authentication.logout();

		Authentication.login("admin", "admin");
		notifs = App.getAdmin().getNotifications();
		toAccept.clear();
		;
		for (AdminNotif a : notifs) {
			if (a.isValidationNotif())
				toAccept.add((ValidationNotif) a);
		}
		for (ValidationNotif v : toAccept)
			App.getAdmin().accept(v, true);

		Set<ReportNotif> toAcceptRep = new HashSet<>();
		for (AdminNotif a : notifs) {
			if (a.isReportNotif())
				toAcceptRep.add((ReportNotif) a);
		}
		for (ReportNotif v : toAcceptRep)
			App.getAdmin().accept(v);
		Authentication.logout();

		try {
			Authentication.login("alejandro", "zombor");
		} catch (BlockedUserException e) {
			System.out.println("\nIf this is printed, the user zombor has been blocked because of plagiarism.");
		}

		Authentication.login("carmen", "patata");
		aux = (RegUser) App.getAppInstance().getActualUser();
		aux.upgradePaying("0000000000000000");
		Authentication.logout();

		System.out.println("\nThe users that have registered: ");
		for (RegUser u : App.getUsers().values()) {
			System.out.println(u.getAuthorName());
		}

		System.out.println("\nThe songs and its state: ");
		for (Song sng : App.getSongs().values()) {
			System.out.println(sng.getName() + ": " + sng.getSongState());
		}

		App.turnOffApp();
		App.reloadData();

	}
}
