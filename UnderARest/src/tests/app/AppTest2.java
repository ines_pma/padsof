package tests.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import org.junit.Test;

import app.App;
import app.exceptions.NameAlreadyExistsException;
import app.media.Song;
import app.media.exceptions.FileTooLongException;
import app.users.RegUser;
import app.users.exceptions.NotBornException;

public class AppTest2 {

	@Test
	public void testingFollowsAndDeleting()
			throws NameAlreadyExistsException, NotBornException, FileNotFoundException, FileTooLongException {
		App.getAppInstance();
		App.setEmptyDataFiles("data/userstest.ser", "data/albumstest.ser", "data/songstest.ser");
		RegUser u = new RegUser("User1", "Pass1", "Authorname1", LocalDate.MIN);
		RegUser v = new RegUser("User2", "Pass2", "Authorname2", LocalDate.MIN);
		u.follow(v);
		v.follow(u);
		App.addUser(u);
		App.addUser(v);
		assertTrue(App.getUsers().get(0L).getFollowers().contains(App.getUsers().get(1L)));
		assertTrue(App.getUsers().get(1L).getFollowers().contains(App.getUsers().get(0L)));
		App.turnOffApp();
		App.reloadData();
		assertTrue(App.getUsers().get(0L).getFollowers().contains(App.getUsers().get(1L)));
		assertTrue(App.getUsers().get(1L).getFollowers().contains(App.getUsers().get(0L)));
		App.addSong(new Song("Song1", "mp3/1.mp3", App.getUsers().get(0L)));
		assertEquals(App.getTotalMusic(), 1L);
		App.turnOffApp();
		App.reloadData();
		assertEquals(App.getTotalMusic(), 1L);
		assertTrue(App.getSongs().containsKey(0L));
		App.deleteSong(App.getSongs().get(0L));
		assertFalse(App.getSongs().containsKey(0L));
		App.turnOffApp();

	}

}
