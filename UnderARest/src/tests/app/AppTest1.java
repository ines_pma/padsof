package tests.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import app.App;
import app.exceptions.NameAlreadyExistsException;
import app.general.Authentication;
import app.users.exceptions.NotBornException;

public class AppTest1 {

	@Test
	public void addingUsers() throws NameAlreadyExistsException, NotBornException {
		App.getAppInstance();
		App.setEmptyDataFiles("data/userstest.ser", "data/albums.ser", "data/songs.ser");
		Authentication.register("User1", "Pass1", "Authorname1", LocalDate.MIN);
		Authentication.register("User2", "Pass2", "Authorname2", LocalDate.MIN);
		assertEquals(App.getUsers().get(0L).getAuthorName(), "Authorname1");
		assertEquals(App.getUsers().get(1L).getUserName(), "User2");
		App.turnOffApp();
		assertTrue(App.getUsers().isEmpty());
		App.reloadData();
		assertEquals(App.getUsers().get(0L).getAuthorName(), "Authorname1");
		assertEquals(App.getUsers().get(1L).getUserName(), "User2");
		App.turnOffApp();
	}

}
